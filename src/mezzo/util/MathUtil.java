package mezzo.util;

/**
 * <code>MathUtil</code>
 * 
 * @author mezzodrinker
 */
public class MathUtil {
    private MathUtil() {}

    public static double max(double... values) {
        double max = Double.NEGATIVE_INFINITY;
        for (double d : values) {
            max = Math.max(max, d);
        }
        return max;
    }

    public static float max(float... values) {
        float max = Float.NEGATIVE_INFINITY;
        for (float f : values) {
            max = Math.max(max, f);
        }
        return max;
    }

    public static long max(long... values) {
        long max = Long.MIN_VALUE;
        for (long l : values) {
            max = Math.max(max, l);
        }
        return max;
    }

    public static int max(int... values) {
        int max = Integer.MIN_VALUE;
        for (int i : values) {
            max = Math.max(max, i);
        }
        return max;
    }

    public static int quotient(int dividend, int divisor) {
        return (int) quotient((double) dividend, (double) divisor);
    }

    public static float quotient(float dividend, float divisor) {
        return (float) quotient((double) dividend, (double) divisor);
    }

    public static double quotient(double dividend, double divisor) {
        return dividend - (Math.floor(dividend / divisor) * divisor);
    }

    public static <T extends Number, V extends Number> double quotient(T dividend, V divisor) {
        if (dividend == null) throw new IllegalArgumentException("dividend can not be null");
        if (divisor == null) throw new IllegalArgumentException("divisor can not be null");
        return quotient(dividend.doubleValue(), divisor.doubleValue());
    }
}
