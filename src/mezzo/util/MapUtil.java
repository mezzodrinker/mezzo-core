package mezzo.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author lirtistasya
 * @since 0.0.1
 */
public class MapUtil {
    // can't instantiate MapUtil
    private MapUtil() {}

    /**
     * Retrieves the first key which is assigned to a given value.
     * 
     * @param map
     *            the map to search
     * @param value
     *            the value to find
     * @return the key if the map contains the value, <code>null</code> if not.
     */
    public static <K, V> K getFirstKey(Map<K, V> map, V value) {
        if (map instanceof BidirectionalMap) return ((BidirectionalMap<K, V>) map).getKey(value);
        final Iterator<K> it = map.keySet().iterator();

        while (it.hasNext()) {
            final K cur = it.next();
            if (value != null && cur != null && map.get(cur) != null && map.get(cur).equals(value))
                return cur;
            else if (value == null && cur != null && map.get(cur) == null) return cur;
        }

        return null;
    }

    /**
     * Retrieves all applicable keys which are assigned to a given value.
     * 
     * @param map
     *            the map to search
     * @param value
     *            the value to find
     * @return a list containing all keys if the map contains the value, an empty list if not.
     */
    public static <K, V> List<K> getKeys(Map<K, V> map, V value) {
        if (map instanceof BidirectionalMap) return Arrays.asList(((BidirectionalMap<K, V>) map).getKey(value));
        final List<K> ret = new ArrayList<K>();
        final Iterator<K> it = map.keySet().iterator();

        while (it.hasNext()) {
            final K cur = it.next();
            if (value != null && cur != null && map.get(cur) != null && map.get(cur).equals(value)) {
                ret.add(cur);
            } else if (value == null && cur != null && map.get(cur) == null) {
                ret.add(cur);
            }
        }

        return ret;
    }

    public static <K, V> String toString(Map<K, V> map) {
        StringBuilder s = new StringBuilder(map.getClass().getSimpleName());
        s.append("@").append(Integer.toHexString(map.hashCode())).append("{");
        for (Entry<K, V> entry : map.entrySet()) {
            s.append("[");
            if (entry.getKey() != map) {
                s.append(String.valueOf(entry.getKey()));
            } else {
                s.append("(this map)");
            }
            s.append("->");
            if (entry.getValue() != map) {
                s.append(String.valueOf(entry.getValue()));
            } else {
                s.append("(this map)");
            }
            s.append("]");
        }
        s.append("}");
        return s.toString();
    }
}
