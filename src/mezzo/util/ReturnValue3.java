package mezzo.util;

/**
 * <code>ReturnValue3</code>
 * 
 * @author mezzodrinker
 * @param <T1>
 *            type of first value
 * @param <T2>
 *            type of second value
 * @param <T3>
 *            type of third value
 */
public class ReturnValue3<T1, T2, T3> {
    public final T1 value1;
    public final T2 value2;
    public final T3 value3;

    public ReturnValue3(T1 value1, T2 value2, T3 value3) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }
}
