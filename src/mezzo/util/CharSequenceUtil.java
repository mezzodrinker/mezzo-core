package mezzo.util;

import java.util.Comparator;
import java.util.Objects;

/**
 * <code>CharSequenceUtil</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class CharSequenceUtil implements Comparator<CharSequence> {
    // can't instantiate CharSequenceUtil
    private CharSequenceUtil() {}

    public boolean equals(CharSequence one, CharSequence two) {
        return compare(one, two) == 0;
    }

    @Override
    public int compare(CharSequence one, CharSequence two) {
        Objects.requireNonNull(one);
        Objects.requireNonNull(two);
        int limit = Math.min(one.length(), two.length());

        for (int i = 0; i < limit; i++) {
            char c1 = one.charAt(i);
            char c2 = two.charAt(i);

            if (c1 != c2) return c1 - c2;
        }
        return one.length() - two.length();
    }
}
