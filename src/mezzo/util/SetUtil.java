package mezzo.util;

import java.util.Random;
import java.util.Set;

/**
 * 
 * @author lirtistasya
 * @since 0.0.1
 */
public class SetUtil {
    // can't instantiate SetUtil
    private SetUtil() {}

    @SuppressWarnings("unchecked")
    public static <T> T getRandom(Set<T> set) {
        Object[] o = set.toArray();
        return (T) o[new Random().nextInt(o.length)];
    }
}
