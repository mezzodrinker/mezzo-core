package mezzo.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * <code>PriorityQueue</code>
 * 
 * @param <T>
 *            type parameter
 * @author mezzodrinker
 */
public class PriorityQueue<T> implements Iterable<T> {
    transient ArrayList<PriorityEntry<T>> elements     = null;

    private static final int              DEFAULT_SIZE = 10;

    public PriorityQueue() {
        elements = new ArrayList<PriorityEntry<T>>(DEFAULT_SIZE);
    }

    public PriorityQueue(int initialCapacity) {
        elements = new ArrayList<PriorityEntry<T>>(initialCapacity);
    }

    public boolean push(T element, double priority) {
        PriorityEntry<T> entry = new PriorityEntry<T>(element, priority);
        if (elements.contains(entry)) return false;
        elements.add(entry);
        elements.sort(null);
        return true;
    }

    public void set(T element, double priority) {
        PriorityEntry<T> entry = new PriorityEntry<T>(element, priority);
        int index = elements.indexOf(entry);
        if (index >= 0) {
            elements.get(index).setPriority(priority);
        } else {
            elements.add(entry);
        }
        elements.sort(null);
    }

    public T peek() {
        return size() <= 0 ? null : elements.get(0).getValue();
    }

    public T pop() {
        return size() <= 0 ? null : elements.remove(0).getValue();
    }

    public boolean remove(T element) {
        return elements.remove(new PriorityEntry<T>(element, 0));
    }

    public int size() {
        return elements.size();
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }

    public boolean contains(T element) {
        return elements.contains(new PriorityEntry<T>(element, 0));
    }

    @Override
    public Iterator<T> iterator() {
        return new PriorityQueueIterator<T>(elements);
    }

    private class PriorityQueueIterator<E> implements Iterator<E> {
        private final Iterator<PriorityEntry<E>> iterator;

        public PriorityQueueIterator(ArrayList<PriorityEntry<E>> elements) {
            this.iterator = elements.iterator();
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public E next() {
            PriorityEntry<E> ret = iterator.next();
            if (ret == null) return null;
            return ret.getValue();
        }
    }

    private class PriorityEntry<E> implements Comparable<PriorityEntry<? extends E>> {
        private final E value;
        private double  priority = Double.MIN_VALUE;

        public PriorityEntry(E value, double priority) {
            this.value = value;
            this.priority = priority;
        }

        public E getValue() {
            return value;
        }

        public double getPriority() {
            return priority;
        }

        public void setPriority(double priority) {
            this.priority = priority;
        }

        @Override
        @SuppressWarnings("unchecked")
        public boolean equals(Object o) {
            if (!(o instanceof PriorityEntry)) return false;
            PriorityEntry<?> entry = (PriorityEntry<?>) o;
            return value.equals(entry.getValue());
        }

        @Override
        public int compareTo(PriorityEntry<? extends E> entry) {
            return (int) (getPriority() - entry.getPriority());
        }
    }
}
