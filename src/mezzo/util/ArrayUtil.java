package mezzo.util;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <code>ArrayUtil</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public class ArrayUtil {
    private static final short[]     EMPTY_PRIMITIVE_SHORT_ARRAY = new short[0];
    private static final int[]       EMPTY_PRIMITIVE_INT_ARRAY   = new int[0];
    private static final long[]      EMPTY_PRIMITIVE_LONG_ARRAY  = new long[0];
    private static final char[]      EMPTY_PRIMITIVE_CHAR_ARRAY  = new char[0];
    private static final Short[]     EMPTY_SHORT_ARRAY           = new Short[0];
    private static final Integer[]   EMPTY_INTEGER_ARRAY         = new Integer[0];
    private static final Long[]      EMPTY_LONG_ARRAY            = new Long[0];
    private static final Character[] EMPTY_CHARACTER_ARRAY       = new Character[0];

    private ArrayUtil() {}

    /**
     * Creates a new array of a specified component type. The array will contain instances provided by the given supplier.
     * 
     * @param type
     *            component type of the new array
     * @param length
     *            length of the new array
     * @param supplier
     *            supplier that provides the elements for the new array
     * @return new array with length {@code length} and component type {@code type}
     * @throws NullPointerException
     *             if {@code supplier} is null
     * @see #create(Class, int, Object...)
     * @see Array#newInstance(Class, int)
     * @since mezzo-core 1.1.51
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] create(Class<T> type, int length, Supplier<? extends T> supplier) {
        Objects.requireNonNull(supplier, "supplier can not be null");
        T[] array = (T[]) Array.newInstance(type, length);
        for (int i = 0; i < array.length; i++) {
            array[i] = supplier.get();
        }
        return array;
    }

    /**
     * Creates a new array of a specified component type. The array will contain new instances of the component type, created with the given arguments.
     * 
     * <p>
     * The class parameters for finding the correct constructor of the provided class will be determined by the classes of the provided constructor arguments like this:
     * 
     * <pre>
     * Class[] argumentTypes = new Class[arguments == null ? 0 : arguments.length];
     * if (arguments != null) {
     *     for (int i = 0; i &lt; arguments.length; i++) {
     *         argumentTypes[i] = arguments[i] == null ? Object.class : arguments[i].getClass();
     *     }
     * }
     * </pre>
     * 
     * </p>
     * 
     * @param type
     *            component type of the new array
     * @param length
     *            length of the new array
     * @param arguments
     *            arguments for the constructor of the component type
     * @return new array with length {@code length} and component type {@code type}
     * @throws IllegalArgumentException
     *             thrown if one of the following exceptions occurs:
     *             <ul>
     *             <li>{@link NoSuchMethodException}, thrown by {@link Class#getConstructor(Class...)}</li>
     *             <li>{@link SecurityException}, thrown by {@link Class#getConstructor(Class...)}</li>
     *             <li>{@link InstantiationException}, thrown by {@link Constructor#newInstance(Object...)}</li>
     *             <li>{@link IllegalAccessException}, thrown by {@link Constructor#newInstance(Object...)}</li>
     *             <li>{@link IllegalArgumentException}, thrown by {@link Constructor#newInstance(Object...)}</li>
     *             <li>{@link InvocationTargetException}, thrown by {@link Constructor#newInstance(Object...)}</li>
     *             </ul>
     * @see #create(Class, int, Supplier)
     * @see Array#newInstance(Class, int)
     * @see Class#getConstructor(Class...)
     * @see Constructor#newInstance(Object...)
     * @since mezzo-core 1.1.51
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] create(Class<T> type, int length, Object... arguments) {
        T[] array = (T[]) Array.newInstance(type, length);
        try {
            Class<?>[] argumentTypes = new Class<?>[arguments == null ? 0 : arguments.length];
            if (arguments != null) {
                for (int i = 0; i < arguments.length; i++) {
                    argumentTypes[i] = arguments[i] == null ? Object.class : arguments[i].getClass();
                }
            }
            Constructor<T> ctor = type.getConstructor(argumentTypes);
            for (int i = 0; i < array.length; i++) {
                array[i] = ctor.newInstance(arguments);
            }
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new IllegalArgumentException("unsupported type " + type.getName(), e);
        }
        return array;
    }

    /**
     * Removes elements from an array that match the given condition.
     * 
     * @param array
     *            array to remove elements from
     * @param condition
     *            condition to evaluate for every element of {@code array}
     * @return a new array with all elements of {@code array}, excluding the removed elements, resized to the count of remaining elements
     * @throws NullPointerException
     *             if {@code condition} is null
     * @see #remove(Object[], Object...)
     * @see Array#newInstance(Class, int)
     * @see Arrays#copyOf(Object[], int)
     * @since mezzo-core 1.1.51
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] remove(T[] array, Predicate<T> condition) {
        Objects.requireNonNull(condition, "condition can not be null");
        T[] ret = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
        int c = 0;
        int hits = 0;
        for (T element : array) {
            if (condition.test(element)) {
                hits++;
                continue;
            }
            ret[c++] = element;
        }
        return hits > 0 ? Arrays.copyOf(ret, c) : ret;
    }

    /**
     * Removes elements from an array that equal one or more selected elements.
     * 
     * <p>
     * This method actually uses a lambda expression and calls {@link #create(Class, int, Supplier)}:
     * 
     * <pre>
     * return remove(array, (T o) -&gt; {
     *     for (T element : elements) {
     *         boolean equal = (o == null &amp;&amp; element == null) || (o != null &amp;&amp; element != null &amp;&amp; o.equals(element));
     *         if (equal) return true;
     *     }
     *     return false;
     * });
     * </pre>
     * 
     * </p>
     * 
     * @param array
     *            array to remove elements from
     * @param elements
     *            elements that equal at least one from this array will be removed from {@code array}
     * @return a new array with all elements of {@code array}, excluding the removed elements, resized to the count of remaining elements
     * @throws NullPointerException
     *             if {@code elements} is {@code null}
     * @see #remove(Object[], Predicate)
     * @see Object#equals(Object)
     * @since mezzo-core 1.1.51
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] remove(T[] array, T... elements) {
        Objects.requireNonNull(elements, "elements can not be null");
        return remove(array, (T o) -> {
            for (T element : elements) {
                boolean equal = (o == null && element == null) || (o != null && element != null && o.equals(element));
                if (equal) return true;
            }
            return false;
        });
    }

    /**
     * Executes an action for every element of a given array.
     * 
     * <p>
     * This method actually uses a lambda expression and calls {@link #foreach(Object[], Predicate, Consumer)}:
     * 
     * <pre>
     * foreach(array, (T element) -&gt; (true), action);
     * </pre>
     * 
     * </p>
     * 
     * @param array
     *            array to use
     * @param action
     *            action to execute
     * @see #foreach(Object[], Predicate, Consumer)
     * @since mezzo-core 1.1.54
     */
    public static <T> void foreach(T[] array, Consumer<? super T> action) {
        foreach(array, (T element) -> (true), action);
    }

    /**
     * Executes an action for every element of a given array that matches the given condition.
     * 
     * @param array
     *            array to use
     * @param condition
     *            condition to evaluate for every element of {@code array}
     * @param action
     *            action to execute
     * @see #foreach(Object[], Consumer)
     * @since mezzo-core 1.1.54
     */
    public static <T> void foreach(T[] array, Predicate<? super T> condition, Consumer<? super T> action) {
        Objects.requireNonNull(array, "array can not be null");
        for (T element : array) {
            if (condition.test(element)) {
                action.accept(element);
            }
        }
    }

    /**
     * Concatenates two arrays.
     * 
     * @param a
     *            first part of the concatenated array
     * @param b
     *            second part of the concatenated array
     * @return concatenated array of a and b
     * @see #concatenate(Class, Object[], Object[])
     * @see Array#newInstance(Class, int)
     * @see System#arraycopy(Object, int, Object, int, int)
     * @since mezzo-core 1.1.55
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] concatenate(T[] a, T[] b) {
        T[] array = (T[]) Array.newInstance(a.getClass().getComponentType(), a.length + b.length);
        System.arraycopy(a, 0, array, 0, a.length);
        System.arraycopy(b, 0, array, a.length, b.length);
        return array;
    }

    /**
     * Concatenates two arrays.
     * 
     * @param type
     *            a common supertype of {@code a} and {@code b}
     * @param a
     *            first part of the concatenated array
     * @param b
     *            second part of the concatenated array
     * @return concatenated array of a and b
     * @see #concatenate(Object[], Object[])
     * @see System#arraycopy(Object, int, Object, int, int)
     * @since mezzo-core 1.1.54
     */
    public static <T, A extends T, B extends T> T[] concatenate(Class<T> type, A[] a, B[] b) {
        T[] array = create(type, a.length + b.length, () -> (null));
        System.arraycopy(a, 0, array, 0, a.length);
        System.arraycopy(b, 0, array, a.length, b.length);
        return array;
    }

    /**
     * Returns <code>true</code> if the contents of the two arguments are equal and <code>false</code> otherwise.
     * 
     * @param a
     *            array
     * @param b
     *            array to be compared to {@code a} for equality
     * @return {@code true} if the contents of {@code a} and {@code b} are equal, {@code false} otherwise.
     * @see Objects#equals(Object, Object)
     * @since mezzo-core 1.1.54
     */
    public static <A, B> boolean equals(A[] a, B[] b) {
        if (a == b) return true;
        if (a == null || b == null) return false;
        if (a.length != b.length) return false;

        for (A _a : a) {
            boolean success = false;
            for (B _b : b) {
                if (Objects.equals(_a, _b)) {
                    success = true;
                    break;
                }
                success = false;
            }
            if (!success) return false;
        }
        return true;
    }

    /**
     * Creates a new array from the given values.
     * 
     * @param values
     *            values to be included in the new array
     * @return a new array containing all values in {@code values}
     */
    @SafeVarargs
    public static <T> T[] array(T... values) {
        return values;
    }

    /**
     * Reverses an array recursively.
     * 
     * @param array
     *            array to reverse
     * @return a new array containing all elements of {@code array} in reversed order
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] reverse(T[] array) {
        T[] ret = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
        for (int i = 0; i < array.length; i++) {
            if (array[i].getClass().getComponentType() != null) {
                ret[array.length - 1 - i] = (T) reverse((T[]) array[i]);
            } else {
                ret[array.length - 1 - i] = array[i];
            }
        }
        return ret;
    }

    /**
     * Prints the contents of an array to the console ({@link System#out}) recursively.
     * 
     * @param array
     *            array to print to the console
     */
    public static <T> void print(T[] array) {
        System.out.println(array + " -> " + Arrays.toString(array));
        for (T element : array) {
            if (element.getClass().getComponentType() != null) {
                print((Object[]) element);
            }
        }
    }

    /**
     * Converts an array of component type {@code short} to an array of component type {@link Short}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, converted to instances of {@code Short}.
     * @see #toObject(int[])
     * @see #toObject(long[])
     * @see #EMPTY_SHORT_ARRAY
     * @see Short#valueOf(short)
     */
    public static Short[] toObject(final short[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_SHORT_ARRAY;
        final Short[] result = new Short[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Short.valueOf(array[i]);
        }
        return result;
    }

    /**
     * Converts an array of component type {@code int} to an array of component type {@link Integer}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, converted to instances of {@code Integer}.
     * @see #toObject(short[])
     * @see #toObject(long[])
     * @see #EMPTY_INTEGER_ARRAY
     * @see Integer#valueOf(int)
     */
    public static Integer[] toObject(final int[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_INTEGER_ARRAY;
        final Integer[] result = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Integer.valueOf(array[i]);
        }
        return result;
    }

    /**
     * Converts an array of component type {@code long} to an array of component type {@link Long}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, converted to instances of {@code Long}.
     * @see #toObject(short[])
     * @see #toObject(int[])
     * @see #EMPTY_LONG_ARRAY
     * @see Long#valueOf(long)
     */
    public static Long[] toObject(final long[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_LONG_ARRAY;
        final Long[] result = new Long[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Long.valueOf(array[i]);
        }
        return result;
    }

    /**
     * Converts an array of component type {@code char} to an array of component type {@link Character}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, converted into instances of {@code Character}.
     * @see #EMPTY_CHARACTER_ARRAY
     * @see Character#valueOf(char)
     * @since mezzo-core 1.1.67
     */
    public static Character[] toObject(final char[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_CHARACTER_ARRAY;
        final Character[] result = new Character[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Character.valueOf(array[i]);
        }
        return result;
    }

    /**
     * Converts an array of component type {@link Short} to an array of component type {@code short}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, excluding {@code null}, converted to {@code short}.
     * @see #toPrimitive(Integer[])
     * @see #toPrimitive(Long[])
     * @see #EMPTY_PRIMITIVE_SHORT_ARRAY
     * @see Short#shortValue()
     */
    public static short[] toPrimitive(final Short[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_PRIMITIVE_SHORT_ARRAY;
        final short[] result = new short[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                continue;
            }
            result[i] = array[i].shortValue();
        }
        return result;
    }

    /**
     * Converts an array of component type {@link Integer} to an array of component type {@code int}
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, excluding {@code null}, converted to {@code int}.
     * @see #toPrimitive(Short[])
     * @see #toPrimitive(Long[])
     * @see #EMPTY_PRIMITIVE_INT_ARRAY
     * @see Integer#intValue()
     */
    public static int[] toPrimitive(final Integer[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_PRIMITIVE_INT_ARRAY;
        final int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                continue;
            }
            result[i] = array[i].intValue();
        }
        return result;
    }

    /**
     * Converts an array of component type {@link Long} to an array of component type {@code long}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, excluding {@code null}, converted to {@code long}.
     * @see #toPrimitive(Short[])
     * @see #toPrimitive(Integer[])
     * @see #EMPTY_PRIMITIVE_LONG_ARRAY
     * @see Long#longValue()
     */
    public static long[] toPrimitive(final Long[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_PRIMITIVE_LONG_ARRAY;
        final long[] result = new long[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                continue;
            }
            result[i] = array[i].longValue();
        }
        return result;
    }

    /**
     * Converts an array of component type {@link Character} to an array of component type {@code char}.
     * 
     * @param array
     *            array to convert
     * @return new array containing all elements of {@code array}, excluding {@code null}, converted into {@code char}.
     * @see #EMPTY_PRIMITIVE_CHAR_ARRAY
     * @see Character#charValue()
     * @since mezzo-core 1.1.67
     */
    public static char[] toPrimitive(final Character[] array) {
        if (array == null) return null;
        if (array.length <= 0) return EMPTY_PRIMITIVE_CHAR_ARRAY;
        final char[] result = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                continue;
            }
            result[i] = array[i].charValue();
        }
        return result;
    }

    /**
     * Shifts the elements of an array to the right, starting from a given index.
     * 
     * <p>
     * Result of <code>shiftRight(new String[] {"a", "b" "c"}, 1)</code>:
     * 
     * <blockquote>
     * <table border="1">
     * <tr>
     * <th>index</th>
     * <td align="center">0</td>
     * <td align="center">1</td>
     * <td align="center">2</td>
     * <td align="center">3</td>
     * </tr>
     * <tr>
     * <th>value</th>
     * <td align="center">{@code "a"}</td>
     * <td align="center">{@code "b"}</td>
     * <td align="center">{@code "b"}</td>
     * <td align="center">{@code "c"}</td>
     * </tr>
     * </table>
     * </blockquote>
     * </p>
     * 
     * @param array
     *            array to shift the elements of
     * @param startPos
     *            position to start shifting elements at
     * @return {@code array} with all elements from {@code startPos} onwards shifted right by 1
     * @throws NullPointerException
     *             if {@code array} is {@code null}
     * @see #shiftLeft(Object[], int)
     * @see System#arraycopy(Object, int, Object, int, int)
     */
    public static <T> T[] shiftRight(T[] array, int startPos) {
        if (array == null) throw new NullPointerException("array can not be null");
        System.arraycopy(array, startPos, array, startPos + 1, array.length - 1 - startPos);
        return array;
    }

    /**
     * Shifts the elements of an array to the left, starting from a given index.
     * 
     * <p>
     * Result of <code>shiftLeft(new String[] {"a", "b" "c"}, 1)</code>:
     * 
     * <blockquote>
     * <table border="1">
     * <tr>
     * <th>index</th>
     * <td align="center">0</td>
     * <td align="center">1</td>
     * <td align="center">2</td>
     * </tr>
     * <tr>
     * <th>value</th>
     * <td align="center">{@code "b"}</td>
     * <td align="center">{@code "c"}</td>
     * <td align="center">{@code "c"}</td>
     * </tr>
     * </table>
     * </blockquote>
     * </p>
     * 
     * @param array
     *            array to shift the elements of
     * @param startPos
     *            position to start shifting elements at
     * @return {@code array} with all elements from {@code startPos} onwards shifted left by 1
     * @throws NullPointerException
     *             if {@code array} is {@code null}
     * @see #shiftRight(Object[], int)
     * @see System#arraycopy(Object, int, Object, int, int)
     */
    public static <T> T[] shiftLeft(T[] array, int startPos) {
        if (array == null) throw new NullPointerException("array can not be null");
        System.arraycopy(array, startPos, array, startPos - 1, array.length - startPos);
        return array;
    }

    /**
     * Appends values to an existing array.
     * 
     * @param array
     *            array to append values to
     * @param values
     *            values to append
     * @return new array which contains both the values from {@code array} and {@code values}
     * @since mezzo-core 1.1.60
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] join(T[] array, T... values) {
        Objects.requireNonNull(array, "array can not be null");
        Objects.requireNonNull(values, "values can not be null");
        return concatenate(array, values);
    }

    /**
     * Checks if a value is contained in an array.
     * 
     * @param array
     *            array to check
     * @param value
     *            value to find
     * @return {@code true} if {@code array} contains {@code value}, {@code false} if not
     * @since mezzo-core 1.1.60
     */
    public static <T> boolean contains(T[] array, T value) {
        if (array == null) return false;
        for (T element : array) {
            if (Objects.equals(element, value)) return true;
        }
        return false;
    }

    /**
     * Determines how often a value is contained in an array.
     * 
     * @param array
     *            array to use
     * @param value
     *            value to count
     * @return number of occurrences of {@code value} in {@code array} or 0 if it is not contained
     * @see #count(Object[], Predicate)
     * @since mezzo-core 1.1.60
     */
    public static <T> int count(T[] array, T value) {
        return count(array, (T e) -> (Objects.equals(e, value)));
    }

    /**
     * Determines how many elements of an array fit a given condition.
     * 
     * @param array
     *            array to use
     * @param condition
     *            condition to use
     * @return number of elements that were tested positive on {@code condition}
     * @see #count(Object[], Object)
     * @since mezzo-core 1.1.65
     */
    public static <T> int count(T[] array, Predicate<? super T> condition) {
        if (array == null || condition == null) return 0;
        int c = 0;
        for (T element : array) {
            if (condition.test(element)) {
                c++;
            }
        }
        return c;
    }

    /**
     * Splits an array at a specified element. These elements will not be contained in the result matrix.
     * 
     * @param array
     *            array to split
     * @param element
     *            element at whose position the array should be split
     * @return a 2D-matrix containing the split array, excluding all occurrences of {@code element}
     * @see #split(Object[], Predicate)
     * @see #splitBefore(Object[], Object)
     * @see #splitAfter(Object[], Object)
     * @since mezzo-core 1.1.61
     */
    public static <T> T[][] split(T[] array, T element) {
        return split(array, (T e) -> (Objects.equals(e, element)));
    }

    /**
     * Splits an array at elements that meet a given condition. These elements will not be contained in the result matrix.
     * 
     * @param array
     *            array to split
     * @param condition
     *            condition for elements at whose position the array should be split
     * @return a 2D-matrix containing the split array, excluding all elements that were tested positive on {@code condition}
     * @see #splitAfter(Object[], Predicate)
     * @see #splitBefore(Object[], Predicate)
     * @since mezzo-core 1.1.65
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] split(T[] array, Predicate<? super T> condition) {
        Objects.requireNonNull(array, "array can not be null");
        Objects.requireNonNull(condition, "condition can not be null");
        T[][] split = (T[][]) Array.newInstance(array.getClass().getComponentType(), count(array, condition) + 1, array.length);
        int c = 0;
        int i = 0;
        for (T element : array) {
            if (condition.test(element)) {
                split[c] = resize(split[c], i);
                c++;
                i = 0;
                continue;
            }
            split[c][i++] = element;
        }
        split[c] = resize(split[c], i);
        return split;
    }

    /**
     * Splits an array at a specified element. Elements that equal the split element will be included in the split part that's closer to index 0.
     * 
     * @param array
     *            array to split
     * @param element
     *            element at whose position the array should be split
     * @return a 2D-matrix containing the split array, where the split element is always included in the split part that's closer to index 0
     * @see #splitAfter(Object[], Predicate)
     * @see #split(Object[], Object)
     * @see #splitBefore(Object[], Object)
     * @since mezzo-core 1.1.65
     */
    public static <T> T[][] splitAfter(T[] array, T element) {
        return split(array, (T e) -> (Objects.equals(e, element)));
    }

    /**
     * Splits an array at elements that meet a given condition. These elements will be included in the split part that's closer to index 0.
     * 
     * @param array
     *            array to split
     * @param condition
     *            condition for elements at whose position the array should be split
     * @return a 2D-matrix containing the split array, all elements that were tested positive on {@code condition} being included in the split part that's closer to index 0
     * @see #split(Object[], Predicate)
     * @see #splitBefore(Object[], Predicate)
     * @since mezzo-core 1.1.65
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] splitAfter(T[] array, Predicate<? super T> condition) {
        Objects.requireNonNull(array, "array can not be null");
        Objects.requireNonNull(condition, "condition can not be null");
        T[][] split = (T[][]) Array.newInstance(array.getClass().getComponentType(), count(array, condition) + 1, array.length);
        int c = 0;
        int i = 0;
        for (T e : array) {
            split[c][i++] = e;
            if (condition.test(e)) {
                split[c] = resize(split[c], i);
                c++;
                i = 0;
            }
        }
        split[c] = resize(split[c], i);
        return split;
    }

    /**
     * Splits an array at a specified element. Elements that equal the split element will be included in the split part that's closer to the end of the array.
     * 
     * @param array
     *            array to split
     * @param element
     *            element at whose position the array should be split
     * @return a 2D-matrix containing the split array, where the split element is always included in the split part that's closer to the array's end
     * @see #splitBefore(Object[], Predicate)
     * @see #split(Object[], Object)
     * @see #splitAfter(Object[], Object)
     * @since mezzo-core 1.1.65
     */
    public static <T> T[][] splitBefore(T[] array, T element) {
        return splitBefore(array, (T e) -> (Objects.equals(e, element)));
    }

    /**
     * Splits an array at elements that meet a given condition. These elements will be included in the split part that's closer to the end of the array.
     * 
     * @param array
     *            array to split
     * @param condition
     *            condition for elements at whose position the array should be split
     * @return a 2D-matrix containing the split array, all elements that were tested positive on {@code condition} being included in the split part that's closer to the end of the array
     * @see #split(Object[], Predicate)
     * @see #splitAfter(Object[], Predicate)
     * @since mezzo-core 1.1.65
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] splitBefore(T[] array, Predicate<? super T> condition) {
        Objects.requireNonNull(array, "array can not be null");
        Objects.requireNonNull(condition, "condition can not be null");
        T[][] split = (T[][]) Array.newInstance(array.getClass().getComponentType(), count(array, condition) + 1, array.length);
        int c = 0;
        int i = 0;
        for (T e : array) {
            if (condition.test(e)) {
                split[c] = resize(split[c], i);
                c++;
                i = 0;
            }
            split[c][i++] = e;
        }
        split[c] = resize(split[c], i);
        return split;
    }

    /**
     * Resizes an array.
     * 
     * @param array
     *            array to resize
     * @param size
     *            new size of the array
     * @return if {@code size} is greater than or equal to the length of {@code array}, returns a new array containing all elements of {@code array}, otherwise a new array containing all elements of
     *         {@code array} from index 0 to {@code size - 1}
     * @see System#arraycopy(Object, int, Object, int, int)
     * @since mezzo-core 1.1.63
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] resize(T[] array, int size) {
        Objects.requireNonNull(array, "array can not be null");
        T[] ret = (T[]) Array.newInstance(array.getClass().getComponentType(), size);
        System.arraycopy(array, 0, ret, 0, Math.min(array.length, size));
        return ret;
    }

    /**
     * Replaces all occurrences of an element in an array.
     * 
     * @param array
     *            array to use
     * @param replace
     *            element that should be replaced
     * @param replacement
     *            object that should be used as replacement
     * @return new array containing all elements of {@code array} except for all occurrences of {@code replace}, which were replaced by {@code replacement}
     * @see #replace(Object[], Object, Supplier)
     * @since mezzo-core 1.1.66
     */
    public static <T> T[] replace(T[] array, T replace, T replacement) {
        return replace(array, replace, () -> (replacement));
    }

    /**
     * Replaces all occurrences of an element in an array.
     * 
     * @param array
     *            array to use
     * @param replace
     *            element that should be replaced
     * @param replacement
     *            provider for objects that should be used as replacement
     * @return new array containing all elements of {@code array} except for all occurrences of {@code replace}, which were replaced by objects provided by {@code replacement}
     * @see #replace(Object[], Object, Object)
     * @since mezzo-core 1.1.66
     */
    public static <T> T[] replace(T[] array, T replace, Supplier<? extends T> replacement) {
        return replace(array, (T e) -> (Objects.equals(e, replace)), replacement);
    }

    /**
     * Replaces all elements of an array that meet a given condition.
     * 
     * @param array
     *            array to use
     * @param condition
     *            condition for elements that should be replaced
     * @param replacement
     *            provider for objects that should be used as replacement
     * @return new array containing all elements of {@code array} except for all elements that were tested positive on {@code condition}, which were replaced by objects provided by {@code replacement}
     * @see #replace(Object[], Object, Object)
     * @see #replace(Object[], Object, Supplier)
     * @since mezzo-core 1.1.66
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] replace(T[] array, Predicate<? super T> condition, Supplier<? extends T> replacement) {
        T[] ret = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
        for (int i = 0; i < array.length; i++) {
            if (condition.test(array[i])) {
                ret[i] = replacement.get();
            } else {
                ret[i] = array[i];
            }
        }
        return ret;
    }

    /**
     * Replaces all elements of an array that match a given regular expression.
     * 
     * @param array
     *            array to use
     * @param regex
     *            regular expression to check the elements against
     * @param replacement
     *            object that should be used as replacement
     * @return new array containing all elements of {@code array} except for all elements that were tested positive on {@code regex}, which were replaced by {@code replacement}
     * @see #replaceAll(CharSequence[], String, Supplier)
     * @see #replaceAll(CharSequence[], Pattern, Supplier)
     * @since mezzo-core 1.1.66
     */
    public static <T extends CharSequence> T[] replaceAll(T[] array, String regex, T replacement) {
        return replaceAll(array, regex, () -> (replacement));
    }

    /**
     * Replaces all elements of an array that match a given regular expression.
     * 
     * @param array
     *            array to use
     * @param regex
     *            regular expression to check the elements against
     * @param replacement
     *            provider for objects that should be used as replacement
     * @return new array containing all elements of {@code array} except for all elements that were tested positive on {@code regex}, which were replaced by objects provided by {@code replacement}
     * @see #replaceAll(CharSequence[], String, CharSequence)
     * @see #replaceAll(CharSequence[], Pattern, Supplier)
     * @since mezzo-core 1.1.66
     */
    public static <T extends CharSequence> T[] replaceAll(T[] array, String regex, Supplier<T> replacement) {
        return replaceAll(array, Pattern.compile(regex), replacement);
    }

    /**
     * Replaces all elements of an array that match a given regular expression.
     * 
     * @param array
     *            array to use
     * @param pattern
     *            pattern to use for checking elements if they should be replaced or not
     * @param replacement
     *            provider for objects that should be used as replacement
     * @return new array containing all elements of {@code array} except for all elements that were tested positive on {@code pattern}, which were replaced by objects provided by {@code replacement}
     * @see #replaceAll(CharSequence[], String, CharSequence)
     * @see #replaceAll(CharSequence[], String, Supplier)
     * @since mezzo-core 1.1.66
     */
    @SuppressWarnings("unchecked")
    public static <T extends CharSequence> T[] replaceAll(T[] array, Pattern pattern, Supplier<T> replacement) {
        T[] ret = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
        for (int i = 0; i < array.length; i++) {
            Matcher matcher = pattern.matcher(array[i]);
            if (matcher.matches()) {
                ret[i] = replacement.get();
            }
        }
        return ret;
    }

    /**
     * Converts an array to a string, recursively.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static <T> String toString(T[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder();
        String className = array.getClass().getComponentType().getCanonicalName();
        if (className == null) {
            className = array.getClass().getComponentType().getName();
        }
        s.append(className).append('[');
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            T element = array[i];
            if (element == null) {
                s.append("null");
                continue;
            }
            Class<?> component = element.getClass().getComponentType();
            if (component != null) {
                // element is an array -> T is an array class
                if (component == boolean.class) {
                    s.append(toString((boolean[]) element));
                    continue;
                }
                if (component == byte.class) {
                    s.append(toString((byte[]) element));
                    continue;
                }
                if (component == short.class) {
                    s.append(toString((short[]) element));
                    continue;
                }
                if (component == int.class) {
                    s.append(toString((int[]) element));
                    continue;
                }
                if (component == char.class) {
                    s.append(toString((char[]) element));
                    continue;
                }
                if (component == float.class) {
                    s.append(toString((float[]) element));
                    continue;
                }
                if (component == long.class) {
                    s.append(toString((long[]) element));
                    continue;
                }
                if (component == double.class) {
                    s.append(toString((double[]) element));
                    continue;
                }
                s.append(toString((Object[]) element));
                continue;
            }
            s.append(element);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code boolean} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(boolean[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("boolean[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code byte} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(byte[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("byte[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code short} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(short[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("short[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts an {@code int} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     */
    public static String toString(int[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("int[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code char} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(char[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("char[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code float} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(float[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("float[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code long} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(long[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("long[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Converts a {@code double} array to a string.
     * 
     * @param array
     *            array to convert
     * @return string representation of {@code array}
     * @since mezzo-core 1.1.80
     */
    public static String toString(double[] array) {
        if (array == null) return "null";

        StringBuilder s = new StringBuilder("double[");
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                s.append(',');
            }
            s.append(array[i]);
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Simple iterator for arrays.
     * 
     * @param <T>
     *            component type of arrays this iterator will accept
     * @author mezzodrinker
     * @since mezzo-core 1.1.54
     */
    public static class ArrayIterator<T> implements Iterator<T> {
        private int      index = 0;

        /**
         * Array this iterator iterates over
         */
        public final T[] array;

        /**
         * Creates a new <code>ArrayIterator</code>.
         * 
         * @param array
         *            array to iterate over
         */
        public ArrayIterator(T[] array) {
            this.array = array;
        }

        @Override
        public void reset() {
            index = 0;
        }

        @Override
        public boolean hasNext() {
            return index < array.length;
        }

        @Override
        public T next() {
            return array[index++];
        }

        @Override
        public T get() {
            return array[index];
        }
    }

    /**
     * Simple iterable based on arrays.
     * 
     * @param <T>
     *            component type of arrays this {@code Iterable} will accept
     * @author mezzodrinker
     * @since mezzo-core 1.1.54
     */
    public static class ArrayIterable<T> implements Iterable<T> {
        /**
         * Array containing all values of this iterable
         */
        public final T[] array;

        /**
         * Creates a new <code>ArrayIterable</code>.
         * 
         * @param array
         *            array that should be set as content array.
         */
        public ArrayIterable(T[] array) {
            this.array = array;
        }

        @Override
        public Iterator<T> iterator() {
            return new ArrayIterator<T>(array);
        }
    }
}