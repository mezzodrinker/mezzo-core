package mezzo.util.sorting;

import static mezzo.util.ArrayUtil.shiftRight;
import static mezzo.util.ArrayUtil.toObject;
import static mezzo.util.ArrayUtil.toPrimitive;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <code>MergeSort</code>
 * 
 * @author mezzodrinker
 */
public class InsertionSort {
    private static final Random random = new Random();

    private InsertionSort() {};

    public static short[] sort(short[] array) {
        return toPrimitive(sort(toObject(array)));
    }

    public static int[] sort(int[] array) {
        return toPrimitive(sort(toObject(array)));
    }

    public static long[] sort(long[] array) {
        return toPrimitive(sort(toObject(array)));
    }

    /**
     * Sorts an array via InsertionSort.
     * 
     * @param array
     *            the array to be sorted
     * @return the sorted array
     * @throws ClassCastException
     *             in case at least one element of the input array don't implement {@link Comparable}.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T>> T[] sort(T[] array) {
        List<Integer> checked = new ArrayList<Integer>();
        T[] sorted = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
        int firstPos = random.nextInt(array.length);
        int curPos = 0;

        sorted[curPos++] = array[firstPos];
        checked.add(firstPos);

        for (int i = 0; i < array.length; i++) {
            if (checked.contains(i)) {
                continue;
            }
            T element = array[i];
            for (int j = 0; j < sorted.length && sorted[j] != null; j++) {
                T sortedElement = sorted[j];
                if (element.compareTo(sortedElement) < 0) {
                    shiftRight(sorted, j);
                    curPos++;
                    sorted[j] = element;
                    checked.add(i);
                    break;
                }
            }
            if (!checked.contains(i)) {
                sorted[curPos++] = element;
                checked.add(i);
            }
        }

        return sorted;
    }
}
