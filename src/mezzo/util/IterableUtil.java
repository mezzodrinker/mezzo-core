package mezzo.util;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

import mezzo.util.ArrayUtil.ArrayIterable;

/**
 * <code>IterableUtil</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class IterableUtil {
    private IterableUtil() {}

    /**
     * 
     * @param iterable
     * @param action
     * @since mezzo-core 1.1.54
     */
    public static <T> void foreach(Iterable<T> iterable, Consumer<? super T> action) {
        foreach(iterable, (T element) -> (true), action);
    }

    /**
     * 
     * @param iterable
     * @param condition
     * @param action
     * @since mezzo-core 1.1.54
     */
    public static <T> void foreach(Iterable<T> iterable, Predicate<? super T> condition, Consumer<? super T> action) {
        for (T element : iterable) {
            if (condition.test(element)) {
                action.accept(element);
            }
        }
    }

    /**
     * 
     * @param a
     * @param b
     * @return
     * @since mezzo-core 1.1.54
     */
    public static <A, B> boolean equals(Iterable<A> a, Iterable<B> b) {
        if (a == b) return true;
        if (a == null || b == null) return false;

        for (A _a : a) {
            boolean success = false;
            for (B _b : b) {
                if (Objects.equals(_a, _b)) {
                    success = true;
                    break;
                }
                success = false;
            }
            if (!success) return false;
        }
        return true;
    }

    /**
     * 
     * @param elements
     * @return
     * @since mezzo-core 1.1.54
     */
    @SafeVarargs
    public static <T> Iterable<T> iterable(T... elements) {
        return new ArrayIterable<>(elements);
    }

    /**
     * Determines the size of an {@link Iterable}.
     * 
     * @param iterable
     *            {@code Iterable} to determine the size of
     * @return size of {@code iterable}
     */
    @SuppressWarnings("unused")
    public static <T> int sizeof(Iterable<T> iterable) {
        int c = 0;
        for (T __ignore : iterable) {
            c++;
        }
        return c;
    }
}
