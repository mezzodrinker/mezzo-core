package mezzo.util.configuration;

/**
 * <code>ConfigKey</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class ConfigKey {
    public final char       separator;
    public final String     path;
    public final ConfigBase parent;

    public ConfigKey(String key, char separator) {
        this(key, null, separator);
    }

    public ConfigKey(String key, ConfigBase parent) {
        this(key, parent, parent.separator);
    }

    public ConfigKey(String key, ConfigBase parent, char separator) {
        this.parent = parent;
        this.separator = separator;
        path = (parent == null ? "" : parent.path + separator) + key;
    }
}
