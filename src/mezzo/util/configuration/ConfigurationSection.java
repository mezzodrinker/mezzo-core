package mezzo.util.configuration;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <code>ConfigurationSection</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public interface ConfigurationSection {
    /**
     * Returns the {@link Configuration} that contains this section. All {@link Configuration}s will return themselves.
     * 
     * @return this section's root
     */
    public Configuration getRoot();

    /**
     * Returns the parent {@link ConfigurationSection} that directly contains this section. All {@link Configuration}s will return null.
     * 
     * @return this section's parent
     */
    public ConfigurationSection getParent();

    /**
     * Returns the key of this section relative to its root.
     * 
     * @return the full key
     */
    public String getFullKey();

    /**
     * Returns the key of this section relative to its parent.
     * 
     * @return the key
     */
    public String getKey();

    /**
     * Returns a set containing all keys of this section. If <code>recursive</code> is set to <code>true</code>, this will also contain all keys of any child sections. If <code>recursive</code> is set
     * to <code>false</code>, this will only contain this section's keys instead, including all child sections' keys.
     * 
     * @param recursive
     *            determines whether any child section's keys should be included or not
     * @return set of keys contained in this section
     */
    public Set<String> getKeys(boolean recursive);

    /**
     * <p>
     * Returns a set containing all values of this section mapped by their respective keys. If <code>recursive</code> is set to <code>true</code>, this will also contain all values of any child
     * sections. If <code>recursive</code> is set to <code>false</code>, this will only contain this section's values instead, including all child sections.
     * </p>
     * <p>
     * If <code>recursive</code> is <code>true</code>, all child sections will be transformed into maps containing the section's values.
     * </p>
     * 
     * @param recursive
     *            determines whether any child section's values should be included or not
     * @return map of values contained in this section mapped by their respective keys
     */
    public Map<String, Object> getValues(boolean recursive);

    /**
     * Sets the given key to the specified value, replacing any existing entry. If <code>value</code> is <code>null</code>, the key will be removed from the section.
     * 
     * @param key
     *            the key of the value to set
     * @param value
     *            the new value to set the key to
     * @see #set(ConfigKey, Object)
     * @see #set(ConfigBase, ConfigurationSection)
     */
    public void set(String key, Object value);

    /**
     * Sets the given key to the specified value, replacing any existing entry. If <code>value</code> is <code>null</code>, the key will be removed from the section.
     * 
     * @param key
     *            the key of the value to set
     * @param value
     *            the new value to set the key to
     * @see #set(String, Object)
     */
    public default void set(ConfigKey key, Object value) {
        set(key.path, value);
    }

    /**
     * Sets the given key to the specified value, replacing any existing entry. If <code>value</code> is <code>null</code>, the key will be removed from the section.
     * 
     * @param key
     *            the key of the value to set
     * @param value
     *            the new value to set the key to
     * @see #set(String, Object)
     */
    public default void set(ConfigBase key, ConfigurationSection value) {
        set(key.path, value);
    }

    /**
     * Removes a key from this section.
     * 
     * @param key
     *            the key to remove
     * @return <code>true</code> if the key was contained in this section, <code>false</code> if not
     * @see #remove(ConfigKey)
     * @see #remove(ConfigBase)
     */
    public boolean remove(String key);

    /**
     * Removes a key from this section.
     * 
     * @param key
     *            the key to remove
     * @return <code>true</code> if the key was contained in this section, <code>false</code> if not
     * @see #remove(String)
     */
    public default boolean remove(ConfigKey key) {
        return remove(key.path);
    }

    /**
     * Removes a key from this section.
     * 
     * @param key
     *            the key to remove
     * @return <code>true</code> if the key was contained in this section, <code>false</code> if not
     * @see #remove(String)
     */
    public default boolean remove(ConfigBase key) {
        return remove(key.path);
    }

    /**
     * Checks if this section contains the given key.
     * 
     * @param key
     *            the key to check
     * @return <code>true</code> if the key is contained in this section, <code>false</code> if not
     * @see #contains(ConfigKey)
     * @see #contains(ConfigBase)
     */
    public boolean contains(String key);

    /**
     * Checks if this section contains the given key.
     * 
     * @param key
     *            the key to check
     * @return <code>true</code> if the key is contained in this section, <code>false</code> if not
     * @see #contains(String)
     */
    public default boolean contains(ConfigKey key) {
        return contains(key.path);
    }

    /**
     * Checks if this section contains the given key.
     * 
     * @param key
     *            the key to check
     * @return <code>true</code> if the key is contained in this section, <code>false</code> if not
     * @see #contains(String)
     */
    public default boolean contains(ConfigBase key) {
        return contains(key.path);
    }

    /**
     * Checks if the value of the given key is a {@link ConfigurationSection}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link ConfigurationSection}, <code>false</code> if not
     * @see #isConfigurationSection(ConfigBase)
     */
    public boolean isConfigurationSection(String key);

    /**
     * Checks if the value of the given key is a {@link ConfigurationSection}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link ConfigurationSection}, <code>false</code> if not
     * @see #isConfigurationSection(String)
     */
    public default boolean isConfigurationSection(ConfigBase key) {
        return isConfigurationSection(key.path);
    }

    /**
     * Checks if the value of the given key is a {@link Boolean}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link Boolean}, <code>false</code> if not
     * @see #isBoolean(ConfigKey)
     */
    public boolean isBoolean(String key);

    /**
     * Checks if the value of the given key is a {@link Boolean}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link Boolean}, <code>false</code> if not
     * @see #isBoolean(String)
     */
    public default boolean isBoolean(ConfigKey key) {
        return isBoolean(key.path);
    }

    /**
     * Checks if the value of the given key is a {@link String}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link String}, <code>false</code> if not
     * @see #isString(ConfigKey)
     */
    public boolean isString(String key);

    /**
     * Checks if the value of the given key is a {@link String}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link String}, <code>false</code> if not
     * @see #isString(String)
     */
    public default boolean isString(ConfigKey key) {
        return isString(key.path);
    }

    /**
     * Checks if the value of the given key is a {@link Number}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link Number}, <code>false</code> if not
     * @see #isNumber(ConfigKey)
     */
    public boolean isNumber(String key);

    /**
     * Checks if the value of the given key is a {@link Number}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link Number}, <code>false</code> if not
     * @see #isNumber(String)
     */
    public default boolean isNumber(ConfigKey key) {
        return isNumber(key.path);
    }

    /**
     * Checks if the value of the given key is an instance of the given type.
     * 
     * @param key
     *            the key of the value to check
     * @param type
     *            the type
     * @return <code>true</code> if the value is an instance of <code>type</code>, <code>false</code> if not
     * @see #is(ConfigKey, Class)
     * @see #is(ConfigBase, Class)
     */
    public <T> boolean is(String key, Class<T> type);

    /**
     * Checks if the value of the given key is an instance of the given type.
     * 
     * @param key
     *            the key of the value to check
     * @param type
     *            the type
     * @return <code>true</code> if the value is an instance of <code>type</code>, <code>false</code> if not
     * @see #is(String, Class)
     */
    public default <T> boolean is(ConfigKey key, Class<T> type) {
        return is(key.path, type);
    }

    /**
     * Checks if the value of the given key is an instance of the given type.
     * 
     * @param key
     *            the key of the value to check
     * @param type
     *            the type
     * @return <code>true</code> if the value is an instance of <code>type</code>, <code>false</code> if not
     * @see #is(String, Class)
     */
    public default <T> boolean is(ConfigBase key, Class<T> type) {
        return is(key.path, type);
    }

    /**
     * Checks if the value of the given key is a {@link List}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link List}, <code>false</code> if not
     * @see #isList(String)
     */
    public boolean isList(String key);

    /**
     * Checks if the value of the given key is a {@link List}.
     * 
     * @param key
     *            the key of the value to check
     * @return <code>true</code> if the value is a {@link List}, <code>false</code> if not
     * @see #isList(String)
     */
    public default boolean isList(ConfigKey key) {
        return isList(key.path);
    }

    /**
     * Gets the requested value by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the value to get
     * @return requested value
     * @see #get(ConfigKey)
     * @see #get(ConfigBase)
     */
    public Object get(String key);

    /**
     * Gets the requested value by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the value to get
     * @return requested value
     * @see #get(String)
     */
    public default Object get(ConfigKey key) {
        return get(key.path);
    }

    /**
     * Gets the requested value by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the value to get
     * @return requested value
     * @see #get(String)
     */
    public default Object get(ConfigBase key) {
        return get(key.path);
    }

    /**
     * Gets the requested value by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the value to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested value
     * @see #get(ConfigKey, Object)
     * @see #get(ConfigBase, Object)
     */
    public Object get(String key, Object defaultValue);

    /**
     * Gets the requested value by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the value to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested value
     * @see #get(String, Object)
     */
    public default Object get(ConfigKey key, Object defaultValue) {
        return get(key.path, defaultValue);
    }

    /**
     * Gets the requested value by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the value to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested value
     * @see #get(String, Object)
     */
    public default Object get(ConfigBase key, Object defaultValue) {
        return get(key.path, defaultValue);
    }

    public Object getDefault(String key);

    public default Object getDefault(ConfigKey key) {
        return getDefault(key.path);
    }

    public default Object getDefault(ConfigBase key) {
        return getDefault(key.path);
    }

    /**
     * Gets the requested {@link ConfigurationSection} by its key. In case the key doesn't exist or isn't set to a {@link ConfigurationSection}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the ConfigurationSection to get
     * @return requested ConfigurationSection
     * @see #getConfigurationSection(ConfigBase)
     */
    public ConfigurationSection getConfigurationSection(String key);

    /**
     * Gets the requested {@link ConfigurationSection} by its key. In case the key doesn't exist or isn't set to a {@link ConfigurationSection}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the ConfigurationSection to get
     * @return requested ConfigurationSection
     * @see #getConfigurationSection(String)
     */
    public default ConfigurationSection getConfigurationSection(ConfigBase key) {
        return getConfigurationSection(key.path);
    }

    /**
     * Gets the requested {@link ConfigurationSection} by its key. In case the key doesn't exist or isn't set to a {@link ConfigurationSection}, this will return the specified default value.
     * 
     * @param key
     *            the key of the ConfigurationSection to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested ConfigurationSection
     * @see #getConfigurationSection(ConfigBase, ConfigurationSection)
     */
    public ConfigurationSection getConfigurationSection(String key, ConfigurationSection defaultValue);

    /**
     * Gets the requested {@link ConfigurationSection} by its key. In case the key doesn't exist or isn't set to a {@link ConfigurationSection}, this will return the specified default value.
     * 
     * @param key
     *            the key of the ConfigurationSection to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested ConfigurationSection
     * @see #getConfigurationSection(String, ConfigurationSection)
     */
    public default ConfigurationSection getConfigurationSection(ConfigBase key, ConfigurationSection defaultValue) {
        return getConfigurationSection(key.path, defaultValue);
    }

    /**
     * Creates a new section with the specified key. This will override any previously set value.
     * 
     * @param key
     *            the key to create the section at
     * @return newly created section
     * @see #createSection(ConfigBase)
     */
    public ConfigurationSection createSection(String key);

    /**
     * Creates a new section with the specified key. This will override any previously set value.
     * 
     * @param key
     *            the key to create the section at
     * @return newly created section
     * @see #createSection(String)
     */
    public default ConfigurationSection createSection(ConfigBase key) {
        return createSection(key.path);
    }

    /**
     * Creates a new section with the specified key and values. This will override any previously set value.
     * 
     * @param key
     *            the key to create the section at
     * @param values
     *            the initial values that the new section will contain
     * @return newly created section
     * @see #createSection(ConfigBase, Map)
     */
    public ConfigurationSection createSection(String key, Map<?, ?> values);

    /**
     * Creates a new section with the specified key and values. This will override any previously set value.
     * 
     * @param key
     *            the key to create the section at
     * @param values
     *            the initial values that the new section will contain
     * @return newly created section
     * @see #createSection(String, Map)
     */
    public default ConfigurationSection createSection(ConfigBase key, Map<?, ?> values) {
        return createSection(key.path, values);
    }

    /**
     * Gets the requested {@link Boolean} by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the Boolean to get
     * @return requested Boolean
     * @see #getBoolean(ConfigKey)
     */
    public Boolean getBoolean(String key);

    /**
     * Gets the requested {@link Boolean} by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the Boolean to get
     * @return requested Boolean
     * @see #getBoolean(String)
     */
    public default Boolean getBoolean(ConfigKey key) {
        return getBoolean(key.path);
    }

    /**
     * Gets the requested {@link Boolean} by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the Boolean to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested Boolean
     * @see #getBoolean(ConfigKey, Boolean)
     */
    public Boolean getBoolean(String key, Boolean defaultValue);

    /**
     * Gets the requested {@link Boolean} by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the Boolean to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested Boolean
     * @see #getBoolean(String)
     */
    public default Boolean getBoolean(ConfigKey key, Boolean defaultValue) {
        return getBoolean(key.path, defaultValue);
    }

    /**
     * Gets the requested {@link String} by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the String to get
     * @return requested String
     * @see #getString(ConfigKey)
     */
    public String getString(String key);

    /**
     * Gets the requested {@link String} by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the String to get
     * @return requested String
     * @see #getString(String)
     */
    public default String getString(ConfigKey key) {
        return getString(key.path);
    }

    /**
     * Gets the requested {@link String} by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the String to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested String
     * @see #getString(ConfigKey, String)
     */
    public String getString(String key, String defaultValue);

    /**
     * Gets the requested {@link String} by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the String to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested String
     * @see #getString(String, String)
     */
    public default String getString(ConfigKey key, String defaultValue) {
        return getString(key.path, defaultValue);
    }

    /**
     * Gets the requested {@link Number} by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the Number to get
     * @return requested Number
     * @see #getNumber(ConfigKey)
     */
    public Number getNumber(String key);

    /**
     * Gets the requested {@link Number} by its key. In case the key doesn't exist, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the Number to get
     * @return requested Number
     * @see #getNumber(String)
     */
    public default Number getNumber(ConfigKey key) {
        return getNumber(key.path);
    }

    /**
     * Gets the requested {@link Number} by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the Number to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested Number
     * @see #getNumber(ConfigKey, Number)
     */
    public Number getNumber(String key, Number defaultValue);

    /**
     * Gets the requested {@link Number} by its key. In case the key doesn't exist, this will return the specified default value.
     * 
     * @param key
     *            the key of the Number to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @return requested Number
     * @see #getNumber(String, Number)
     */
    public default Number getNumber(ConfigKey key, Number defaultValue) {
        return getNumber(key.path, defaultValue);
    }

    /**
     * Gets the requested value by its key, cast to the specified type. In case the key doesn't exist or the value could not be cast, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the value to get
     * @param type
     *            the type to cast to
     * @return requested value
     * @see #getGeneric(ConfigKey, Class)
     * @see #getGeneric(ConfigBase, Class)
     */
    public <T> T getGeneric(String key, Class<T> type);

    /**
     * Gets the requested value by its key, cast to the specified type. In case the key doesn't exist or the value could not be cast, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the value to get
     * @param type
     *            the type to cast to
     * @return requested value
     * @see #getGeneric(String, Class)
     */
    public default <T> T getGeneric(ConfigKey key, Class<T> type) {
        return getGeneric(key.path, type);
    }

    /**
     * Gets the requested value by its key, cast to the specified type. In case the key doesn't exist or the value could not be cast, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the value to get
     * @param type
     *            the type to cast to
     * @return requested value
     * @see #getGeneric(String, Class)
     */
    public default <T> T getGeneric(ConfigBase key, Class<T> type) {
        return getGeneric(key.path, type);
    }

    /**
     * Gets the requested value by its key, cast to the specified type. In case the key doesn't exist or the value could not be cast, this will return the specified default value.
     * 
     * @param key
     *            the key of the value to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @param type
     *            the type to cast to
     * @return requested value
     * @see #getGeneric(ConfigKey, Object, Class)
     * @see #getGeneric(ConfigBase, Object, Class)
     */
    public <T> T getGeneric(String key, T defaultValue, Class<T> type);

    /**
     * Gets the requested value by its key, cast to the specified type. In case the key doesn't exist or the value could not be cast, this will return the specified default value.
     * 
     * @param key
     *            the key of the value to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @param type
     *            the type to cast to
     * @return requested value
     * @see #getGeneric(String, Object, Class)
     */
    public default <T> T getGeneric(ConfigKey key, T defaultValue, Class<T> type) {
        return getGeneric(key.path, defaultValue, type);
    }

    /**
     * Gets the requested value by its key, cast to the specified type. In case the key doesn't exist or the value could not be cast, this will return the specified default value.
     * 
     * @param key
     *            the key of the value to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist
     * @param type
     *            the type to cast to
     * @return requested value
     * @see #getGeneric(String, Object, Class)
     */
    public default <T> T getGeneric(ConfigBase key, T defaultValue, Class<T> type) {
        return getGeneric(key.path, defaultValue, type);
    }

    /**
     * Gets the requested {@link List} by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List
     * @see #getList(ConfigKey)
     */
    public List<?> getList(String key);

    /**
     * Gets the requested {@link List} by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List
     * @see #getList(String)
     */
    public default List<?> getList(ConfigKey key) {
        return getList(key.path);
    }

    /**
     * Gets the requested {@link List} by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List
     * @see #getList(ConfigKey, List)
     */
    public List<?> getList(String key, List<?> defaultValue);

    /**
     * Gets the requested {@link List} by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List
     * @see #getList(String, List)
     */
    public default List<?> getList(ConfigKey key, List<?> defaultValue) {
        return getList(key.path, defaultValue);
    }

    /**
     * Gets the requested {@link List} of {@link Boolean}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List of Booleans
     * @see #getBooleanList(ConfigKey)
     */
    public List<Boolean> getBooleanList(String key);

    /**
     * Gets the requested {@link List} of {@link Boolean}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List of Booleans
     * @see #getBooleanList(String)
     */
    public default List<Boolean> getBooleanList(ConfigKey key) {
        return getBooleanList(key.path);
    }

    /**
     * Gets the requested {@link List} of {@link Boolean}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List of Booleans
     * @see #getBooleanList(ConfigKey, List)
     */
    public List<Boolean> getBooleanList(String key, List<Boolean> defaultValue);

    /**
     * Gets the requested {@link List} of {@link Boolean}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List of Booleans
     * @see #getBooleanList(String, List)
     */
    public default List<Boolean> getBooleanList(ConfigKey key, List<Boolean> defaultValue) {
        return getBooleanList(key.path, defaultValue);
    }

    /**
     * Gets the requested {@link List} of {@link String}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List of Strings
     * @see #getStringList(ConfigKey)
     */
    public List<String> getStringList(String key);

    /**
     * Gets the requested {@link List} of {@link String}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List of Strings
     * @see #getStringList(String)
     */
    public default List<String> getStringList(ConfigKey key) {
        return getStringList(key.path);
    }

    /**
     * Gets the requested {@link List} of {@link String}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List of Strings
     * @see #getStringList(ConfigKey, List)
     */
    public List<String> getStringList(String key, List<String> defaultValue);

    /**
     * Gets the requested {@link List} of {@link String}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List of Strings
     * @see #getStringList(String, List)
     */
    public default List<String> getStringList(ConfigKey key, List<String> defaultValue) {
        return getStringList(key.path, defaultValue);
    }

    /**
     * Gets the requested {@link List} of {@link Number}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List of Numbers
     * @see #getNumberList(ConfigKey)
     */
    public List<Number> getNumberList(String key);

    /**
     * Gets the requested {@link List} of {@link Number}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>.
     * 
     * @param key
     *            the key of the List to get
     * @return requested List of Numbers
     * @see #getNumberList(String)
     */
    public default List<Number> getNumberList(ConfigKey key) {
        return getNumberList(key.path);
    }

    /**
     * Gets the requested {@link List} of {@link Number}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List of Numbers
     * @see #getNumberList(ConfigKey, List)
     */
    public List<Number> getNumberList(String key, List<Number> defaultValue);

    /**
     * Gets the requested {@link List} of {@link Number}s by its key. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List of Numbers
     * @see #getNumberList(String, List)
     */
    public default List<Number> getNumberList(ConfigKey key, List<Number> defaultValue) {
        return getNumberList(key.path, defaultValue);
    }

    /**
     * Gets the requested {@link List} with all values that can be cast to the specified type. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>
     * 
     * @param key
     *            the key of the List to get
     * @param type
     *            the type to cast the list's values to
     * @return requested List
     * @see #getGenericList(ConfigKey, Class)
     */
    public <T> List<T> getGenericList(String key, Class<T> type);

    /**
     * Gets the requested {@link List} with all values that can be cast to the specified type. In case the key doesn't exist or isn't set to a {@link List}, this will return <code>null</code>
     * 
     * @param key
     *            the key of the List to get
     * @param type
     *            the type to cast the list's values to
     * @return requested List
     * @see #getGenericList(String, Class)
     */
    public default <T> List<T> getGenericList(ConfigKey key, Class<T> type) {
        return getGenericList(key.path, type);
    }

    /**
     * Gets the requested {@link List} with all values that can be cast to the specified type. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default
     * value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the type to cast the list's values to
     * @param type
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List
     * @see #getGenericList(ConfigKey, List, Class)
     */
    public <T> List<T> getGenericList(String key, List<T> defaultValue, Class<T> type);

    /**
     * Gets the requested {@link List} with all values that can be cast to the specified type. In case the key doesn't exist or isn't set to a {@link List}, this will return the specified default
     * value.
     * 
     * @param key
     *            the key of the List to get
     * @param defaultValue
     *            the type to cast the list's values to
     * @param type
     *            the default value to return in case the key doesn't exist or isn't set to a {@link List}
     * @return requested List
     * @see #getGenericList(String, List, Class)
     */
    public default <T> List<T> getGenericList(ConfigKey key, List<T> defaultValue, Class<T> type) {
        return getGenericList(key.path, defaultValue, type);
    }
}
