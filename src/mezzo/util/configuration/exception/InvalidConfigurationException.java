package mezzo.util.configuration.exception;

/**
 * <code>InvalidConfigurationException</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public class InvalidConfigurationException extends Exception {
    private static final long serialVersionUID = 4241246616412685748L;

    public InvalidConfigurationException() {}

    public InvalidConfigurationException(String message) {
        super(message);
    }

    public InvalidConfigurationException(Throwable cause) {
        super(cause);
    }

    public InvalidConfigurationException(String message, Throwable cause) {
        super(cause);
    }
}
