package mezzo.util.configuration.yaml;

import mezzo.util.configuration.ConfigurationOptions;

/**
 * <code>YamlConfigurationOptions</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public class YamlConfigurationOptions implements ConfigurationOptions {
    private Character separator    = '.';
    private int       indentSize   = 2;
    private String    header       = "";
    private String    headerPrefix = "# ";

    @Override
    public Character separator() {
        return separator;
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>separator</code> is <code>null</code>
     */
    @Override
    public YamlConfigurationOptions separator(Character separator) {
        if (separator == null) throw new IllegalArgumentException("separator can not be null");
        this.separator = separator;
        return this;
    }

    @Override
    public int indentSize() {
        return indentSize;
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>indentSize</code> is less than <code>1</code>
     */
    @Override
    public YamlConfigurationOptions indentSize(int indentSize) {
        if (indentSize < 1) throw new IllegalArgumentException("indentSize can not be less than 1");
        this.indentSize = indentSize;
        return this;
    }

    @Override
    public String header() {
        return header;
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>header</code> is <code>null</code>
     */
    @Override
    public YamlConfigurationOptions header(String header) {
        if (header == null) throw new IllegalArgumentException("header can not be null");
        this.header = header;
        return this;
    }

    @Override
    public String headerPrefix() {
        return headerPrefix;
    }
}
