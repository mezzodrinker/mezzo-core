package mezzo.util.configuration.yaml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;

import mezzo.util.configuration.Configuration;
import mezzo.util.configuration.MemorySection;
import mezzo.util.configuration.exception.InvalidConfigurationException;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.wrappers.SerializableColor;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

/**
 * <code>YamlConfiguration</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public class YamlConfiguration extends MemorySection implements Configuration {
    protected Configuration            defaults      = null;
    protected YamlConfigurationOptions options       = new YamlConfigurationOptions();
    protected final Representer        representer   = new Representer();
    protected final DumperOptions      dumperOptions = new DumperOptions();
    protected final Yaml               yaml          = new Yaml(representer, dumperOptions);
    protected static final String      EMPTY_CONFIG  = "{}\n";

    static {
        ConfigurationSerializer.registerClass(SerializableColor.class);
    }

    protected String parseHeader(String data) {
        StringBuilder header = new StringBuilder();
        String[] lines = data.split("\r?\n", -1);
        boolean foundHeader = false;
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (line.startsWith(options().headerPrefix())) {
                if (i > 0) {
                    header.append("\n");
                }
                if (line.length() > options().headerPrefix().length()) {
                    header.append(line.substring(options().headerPrefix().length()));
                }
                foundHeader = true;
            } else if (foundHeader && line.length() == 0) {
                header.append("\n");
            } else if (foundHeader) {
                break;
            }
        }
        return header.toString();
    }

    protected String buildHeader() {
        String h = options().header();
        StringBuilder header = new StringBuilder();
        String[] lines = h.split("\r?\n", -1);
        boolean startedHeader = false;
        for (int i = lines.length - 1; i >= 0; i--) {
            String line = lines[i];
            header.insert(0, "\n");
            if (startedHeader || line.length() > 0) {
                header.insert(0, line);
                header.insert(0, options().headerPrefix());
                startedHeader = true;
            }
        }
        return header.toString();
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>file</code> is <code>null</code>
     * @see #load(InputStream)
     */
    @Override
    public void load(File file) throws FileNotFoundException, IOException, InvalidConfigurationException {
        if (file == null) throw new IllegalArgumentException("file can not be null");
        FileInputStream fileInputStream = new FileInputStream(file);
        load(fileInputStream);
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>inputStream</code> is <code>null</code>
     * @see #load(String)
     */
    @Override
    public void load(InputStream inputStream) throws IOException, InvalidConfigurationException {
        if (inputStream == null) throw new IllegalArgumentException("input stream can not be null");
        StringBuilder content = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream); BufferedReader reader = new BufferedReader(inputStreamReader);) {
            String line;
            for (long lineCount = 0; (line = reader.readLine()) != null; lineCount++) {
                if (lineCount > 0) {
                    content.append("\n");
                }
                content.append(line);
            }
        }
        load(content.toString());
    }

    /**
     * @see Yaml#load(String)
     */
    @Override
    public void load(String data) throws InvalidConfigurationException {
        if (data.replaceAll("\n|(\n\r)|\\s", "").isEmpty() || data.equalsIgnoreCase(EMPTY_CONFIG)) return;
        Object o = yaml.load(data);
        if (o != null && !(o instanceof Map)) throw new InvalidConfigurationException("top level value is not a map");
        options().header(parseHeader(data));
        if (o == null) return;
        Map<?, ?> content = (Map<?, ?>) o;
        for (Entry<?, ?> entry : content.entrySet()) {
            String key = entry.getKey().toString();
            Object value = entry.getValue();
            if (value instanceof Map) {
                createSection(key, (Map<?, ?>) value);
            } else {
                set(key, value);
            }
        }
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>file</code> is <code>null</code>
     * @see #saveToString()
     * @see FileWriter#write(String)
     */
    @Override
    public void save(File file) throws IOException {
        if (file == null) throw new IllegalArgumentException("file can not be null");
        dumperOptions.setIndent(options().indentSize());
        dumperOptions.setDefaultFlowStyle(FlowStyle.BLOCK);
        representer.setDefaultFlowStyle(FlowStyle.BLOCK);
        String content = saveToString();
        File parent = file.getParentFile();
        if (parent != null && !parent.exists()) {
            parent.mkdirs();
        }
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(content);
        }
    }

    /**
     * @see Yaml#dump(Object)
     */
    @Override
    public String saveToString() {
        dumperOptions.setIndent(options().indentSize());
        dumperOptions.setDefaultFlowStyle(FlowStyle.BLOCK);
        representer.setDefaultFlowStyle(FlowStyle.BLOCK);

        Map<String, Object> values = getValues(true);
        if (defaults != null) {
            for (Entry<String, Object> entry : defaults.getValues(true).entrySet()) {
                if (!values.containsKey(entry.getKey())) {
                    values.put(entry.getKey(), entry.getValue());
                }
            }
        }

        String config = yaml.dump(values);
        if (config.equals(EMPTY_CONFIG)) {
            config = "";
        }

        String header = buildHeader();

        return header + header == null || header.isEmpty() ? "" : "\n" + config;
    }

    @Override
    public YamlConfigurationOptions options() {
        if (options == null) {
            options = new YamlConfigurationOptions();
        }
        return options;
    }

    @Override
    public void addDefault(String key, Object value) {
        if (key == null) throw new IllegalArgumentException("path can not be null");

        if (defaults == null) {
            defaults = new YamlConfiguration();
        }

        defaults.set(key, value);
    }

    @Override
    public void addDefaults(Map<String, Object> defaults) {
        if (defaults == null) throw new IllegalArgumentException("defaults can not be null");

        for (Entry<String, Object> entry : defaults.entrySet()) {
            addDefault(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void addDefaults(Configuration defaults) {
        if (defaults == null) throw new IllegalArgumentException("defaults can not be null");

        addDefaults(defaults.getValues(true));
    }

    @Override
    public Configuration getDefaults() {
        return defaults;
    }

    /**
     * Loads a {@link YamlConfiguration} from the given {@link InputStream}. This will return an empty configuration if...
     * <ul>
     * <li>the stream does not contain any data <b>or</b></li>
     * <li>an {@link IOException} occurred <b>or</b></li>
     * <li>an {@link InvalidConfigurationException} occurred</li>
     * </ul>
     * Each {@link Exception} will be ignored and logged to {@link System#err}.
     * 
     * @param stream
     *            the stream to load from
     * @return configuration that was loaded
     * @throws IllegalArgumentException
     *             thrown if <code>stream</code> is <code>null</code>
     * @see #load(InputStream)
     */
    public static YamlConfiguration loadConfiguration(InputStream stream) {
        if (stream == null) throw new IllegalArgumentException("stream can not be null");
        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(stream);
        } catch (Throwable e) {
            System.err.println("could not load configuration from stream: " + e.getMessage());
            e.printStackTrace();
        }
        return config;
    }

    /**
     * Loads a {@link YamlConfiguration} from the given {@link File}. This will return an empty configuration if...
     * <ul>
     * <li>the file does not contain any data <b>or</b></li>
     * <li>an {@link IOException} occurred <b>or</b></li>
     * <li>an {@link InvalidConfigurationException} occurred</li>
     * </ul>
     * Each {@link Exception} will be ignored and logged to {@link System#err}.
     * 
     * @param file
     *            the file to load from
     * @return configuration that was loaded
     * @throws IllegalArgumentException
     *             thrown if <code>file</code> is <code>null</code>
     * @see #load(File)
     */
    public static YamlConfiguration loadConfiguration(File file) {
        if (file == null) throw new IllegalArgumentException("file can not be null");
        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(file);
        } catch (Throwable e) {
            System.err.println("could not load configuration from file: " + e.getMessage());
            e.printStackTrace();
        }
        return config;
    }

    public static void dump(File file) {
        YamlConfiguration config = loadConfiguration(file);
        System.out.println(config.saveToString());
    }

    // public static void main(String... args) {
    // try {
    // File file = new File("test.yml");
    // dump(file)
    // } catch (Exception | Error e) {
    // e.printStackTrace();
    // }
    // }
}
