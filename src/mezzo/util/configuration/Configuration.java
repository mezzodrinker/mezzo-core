package mezzo.util.configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import mezzo.util.configuration.exception.InvalidConfigurationException;

/**
 * <code>Configuration</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public interface Configuration extends ConfigurationSection {
    /**
     * Gets the {@link ConfigurationOptions} for this {@link Configuration}.
     * 
     * @return options for this Configuration
     */
    public ConfigurationOptions options();

    /**
     * Loads the values of this {@link Configuration} from the given {@link InputStream}. Any values (except options) will be overwritten by this method.
     * 
     * @param stream
     *            the stream to load from
     * @throws IOException
     *             thrown if an I/O exception occurs
     * @throws InvalidConfigurationException
     *             thrown if the stream's contents could not be converted into a Configuration
     */
    public void load(InputStream stream) throws IOException, InvalidConfigurationException;

    /**
     * Loads the values of this {@link Configuration} from the given {@link File}. Any values (except options) will be overwritten by this method.
     * 
     * @param file
     *            the file to load from
     * @throws FileNotFoundException
     *             thrown if the given file was not found
     * @throws IOException
     *             thrown if an I/O exception occurs
     * @throws InvalidConfigurationException
     *             thrown if the file's contents could not be converted into a Configuration
     */
    public void load(File file) throws FileNotFoundException, IOException, InvalidConfigurationException;

    /**
     * Loads the values of this {@link Configuration} from the given {@link String}. Any values (except options) will be overwritten by this method.
     * 
     * @param data
     *            the string to load from
     * @throws InvalidConfigurationException
     *             thrown if the string could not be converted into a Configuration
     */
    public void load(String data) throws InvalidConfigurationException;

    /**
     * Saves the values of this {@link Configuration} to the given {@link File}. Any options won't be saved.
     * 
     * @param file
     *            the file to save this Configuration to
     * @throws IOException
     *             thrown if an I/O exception occurs
     */
    public void save(File file) throws IOException;

    /**
     * Saves the values of this {@link Configuration} to a {@link String}. Any options won't be saved.
     * 
     * @return string containing this Configuration
     */
    public String saveToString();

    public void addDefault(String key, Object value);

    public default void addDefault(ConfigKey key, Object value) {
        if (key == null) throw new IllegalArgumentException("key can not be null");
        addDefault(key.path, value);
    }

    public void addDefaults(Map<String, Object> defaults);

    public void addDefaults(Configuration defaults);

    public Configuration getDefaults();
}
