package mezzo.util.configuration;

/**
 * <code>ConfigBase</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class ConfigBase {
    public final char       separator;
    public final String     path;
    public final ConfigBase parent;

    public ConfigBase(String key, char separator) {
        this(key, null, separator);
    }

    public ConfigBase(String key, ConfigBase parent) {
        this(key, parent, parent.separator);
    }

    public ConfigBase(String key, ConfigBase parent, char separator) {
        this.parent = parent;
        this.separator = separator;
        path = (parent == null ? "" : parent.path + separator) + key;
    }
}
