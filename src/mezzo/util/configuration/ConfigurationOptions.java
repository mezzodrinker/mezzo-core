package mezzo.util.configuration;

/**
 * <code>ConfigurationOptions</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public interface ConfigurationOptions {
    /**
     * Gets the {@link Character} that will be used to separate {@link ConfigurationSection}s.
     * 
     * @return key separator
     */
    public Character separator();

    /**
     * Sets the {@link Character} that will be used to separate {@link ConfigurationSection}s.
     * 
     * @param separator
     *            the new separator
     * @return this instance
     */
    public ConfigurationOptions separator(Character separator);

    /**
     * Gets how many space characters should be used to indent each line.
     * 
     * @return count of space characters for indentation
     */
    public int indentSize();

    /**
     * Sets how many space characters should be used to indent each line.
     * 
     * @param indentSize
     *            the new count of space characters for indentation
     * @return this instance
     */
    public ConfigurationOptions indentSize(int indentSize);

    /**
     * <p>
     * Gets the header that will be applied to the top of the saved output.
     * </p>
     * <p>
     * This header will be commented out and applied directly at the top of the generated output of the {@link Configuration}. It is not required to include a newline at the end of the header as it
     * will automatically be applied, but you may include one if you wish for extra spacing.
     * </p>
     * 
     * @return a Configuration's header
     */
    public String header();

    /**
     * <p>
     * Gets the header that will be applied to the top of the saved output.
     * </p>
     * <p>
     * This header will be commented out and applied directly at the top of the generated output of the {@link Configuration}. It is not required to include a newline at the end of the header as it
     * will automatically be applied, but you may include one if you wish for extra spacing.
     * </p>
     * 
     * @param header
     *            the new header
     * @return this instance
     */
    public ConfigurationOptions header(String header);

    /**
     * Gets the header prefix that will be applied to every line of a {@link Configuration}'s header.
     * 
     * @return the header prefix
     */
    public String headerPrefix();
}
