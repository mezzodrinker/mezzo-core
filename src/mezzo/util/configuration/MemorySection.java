package mezzo.util.configuration;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.ConfigurationSerializer;

/**
 * <code>ConfigurationSection</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public class MemorySection implements ConfigurationSection {
    protected final Configuration        root;
    protected final ConfigurationSection parent;
    protected final String               fullKey;
    protected final String               key;
    protected final Map<String, Object>  values = new LinkedHashMap<String, Object>();

    /**
     * Creates a new instance of <code>MemorySection</code>.
     * 
     * @throws UnsupportedOperationException
     *             thrown if this class isn't an instance of {@link Configuration}.
     */
    protected MemorySection() {
        if (!(this instanceof Configuration)) throw new UnsupportedOperationException("can not instanciate a configuration section with ConfigurationSection()");
        root = (Configuration) this;
        parent = null;
        fullKey = "";
        key = "";
    }

    /**
     * Creates a new instance of <code>MemorySection</code> with the given parent and key.
     * 
     * @param parent
     *            the new section's parent section
     * @param key
     *            the key of the new section in the parent section
     * @throws IllegalArgumentException
     *             throw if <code>parent</code> is <code>null</code> or <code>key</code> is either <code>null</code> or empty.
     */
    protected MemorySection(ConfigurationSection parent, String key) {
        if (parent == null) throw new IllegalArgumentException("parent can not be null");
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("key can not be null or empty");
        this.parent = parent;
        this.key = key;
        root = parent.getRoot();
        fullKey = parent.getFullKey().isEmpty() ? key : parent.getFullKey() + root.options().separator() + key;
    }

    @Override
    public Configuration getRoot() {
        return root;
    }

    @Override
    public ConfigurationSection getParent() {
        return parent;
    }

    @Override
    public String getFullKey() {
        return fullKey;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Set<String> getKeys(boolean recursive) {
        if (!recursive) return new LinkedHashSet<String>(values.keySet());
        Set<String> keys = new LinkedHashSet<String>();
        for (Entry<String, Object> entry : values.entrySet()) {
            keys.add(entry.getKey());
            if (entry.getValue() instanceof MemorySection) {
                MemorySection section = (MemorySection) entry.getValue();
                Set<String> otherKeys = section.getKeys(recursive);
                Character separator = getRoot().options().separator();
                for (String key : otherKeys) {
                    keys.add(section.getKey() + separator + key);
                }
            }
        }
        return keys;
    }

    @Override
    public Map<String, Object> getValues(boolean recursive) {
        if (!recursive) return new LinkedHashMap<String, Object>(values);
        Map<String, Object> values = new LinkedHashMap<String, Object>();
        for (Entry<String, Object> entry : this.values.entrySet()) {
            if (entry.getValue() instanceof ConfigurationSection) {
                ConfigurationSection section = (ConfigurationSection) entry.getValue();
                values.put(entry.getKey(), section.getValues(recursive));
            } else {
                values.put(entry.getKey(), entry.getValue());
            }
        }
        return values;
    }

    /**
     * {@inheritDoc}
     * <p>
     * If the value is an instance of {@link ConfigurationSerializable}, it will be serialized.
     * </p>
     * 
     * @throws IllegalArgumentException
     *             thrown if <code>key</code> is either <code>null</code> or empty
     * @see ConfigurationSerializer#serializeObject(ConfigurationSerializable)
     * @see #createSection(String)
     */
    @Override
    public void set(String key, Object value) {
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("key can not be null or empty");
        char separator = getRoot().options().separator();
        int leadingIndex = -1;
        int trailingIndex;
        ConfigurationSection section = this;
        while ((leadingIndex = key.indexOf(separator, trailingIndex = leadingIndex + 1)) >= 0) {
            String node = key.substring(trailingIndex, leadingIndex);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null) {
                section = section.createSection(node);
            } else {
                section = subSection;
            }
        }
        String node = key.substring(trailingIndex);
        if (section == this) {
            if (value == null) {
                values.remove(node);
            } else {
                if (value instanceof ConfigurationSerializable) {
                    values.put(node, ConfigurationSerializer.serializeObject((ConfigurationSerializable) value));
                } else if (value instanceof Iterable) {
                    Iterable<?> list = (Iterable<?>) value;
                    List<Object> toSet = new ArrayList<Object>();
                    for (Object o : list) {
                        if (o instanceof ConfigurationSerializable) {
                            toSet.add(ConfigurationSerializer.serializeObject((ConfigurationSerializable) o));
                            continue;
                        }
                        toSet.add(o);
                    }
                    values.put(node, toSet);
                } else if (value.getClass().getComponentType() != null) {
                    Object[] o = (Object[]) value;
                    List<Object> list = new ArrayList<>();
                    for (Object element : o) {
                        if (element instanceof ConfigurationSerializable) {
                            list.add(ConfigurationSerializer.serializeObject((ConfigurationSerializable) element));
                            continue;
                        }
                        list.add(element);
                    }
                    values.put(node, list);
                } else {
                    values.put(node, value);
                }
            }
        } else {
            section.set(node, value);
        }
    }

    /**
     * @see #set(String, Object)
     */
    @Override
    public boolean remove(String key) {
        boolean contained = contains(key);
        if (contained) {
            set(key, null);
        }
        return contained;
    }

    /**
     * @see #get(String)
     */
    @Override
    public boolean contains(String key) {
        return get(key) != null;
    }

    /**
     * @see #getConfigurationSection(String)
     */
    @Override
    public boolean isConfigurationSection(String key) {
        return getConfigurationSection(key) != null;
    }

    /**
     * @see #getBoolean(String)
     */
    @Override
    public boolean isBoolean(String key) {
        return getBoolean(key) != null;
    }

    /**
     * @see #getString(String)
     */
    @Override
    public boolean isString(String key) {
        return getString(key) != null;
    }

    /**
     * @see #getNumber(String)
     */
    @Override
    public boolean isNumber(String key) {
        return getNumber(key) != null;
    }

    /**
     * @see #getGeneric(String, Class)
     */
    @Override
    public <T> boolean is(String key, Class<T> type) {
        return getGeneric(key, type) != null;
    }

    /**
     * @see #getList(String)
     */
    @Override
    public boolean isList(String key) {
        return getList(key) != null;
    }

    /**
     * @see #get(String, Object)
     */
    @Override
    public Object get(String key) {
        return get(key, getDefault(key));
    }

    /**
     * {@inheritDoc}
     * <p>
     * If the value is an instance of {@link Map} and contains the key {@link ConfigurationSerializer#KEY_SERIALIZED_TYPE}, the map will be deserialized.
     * </p>
     * 
     * @throws IllegalArgumentException
     *             thrown if <code>key</code> is either <code>null</code> or empty
     * @see ConfigurationSerializer#deserializeObject(Map)
     * @see #createSection(String)
     */
    @Override
    public Object get(String key, Object defaultValue) {
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("key can not be null or empty");
        char separator = getRoot().options().separator();
        int leadingIndex = -1;
        int trailingIndex;
        ConfigurationSection section = this;
        while ((leadingIndex = key.indexOf(separator, trailingIndex = leadingIndex + 1)) >= 0) {
            String node = key.substring(trailingIndex, leadingIndex);
            // /* DEBUG */System.out.println("\t@" + Integer.toHexString(hashCode()) + " section node == " + node);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null) {
                section = section.createSection(node);
            } else {
                section = subSection;
            }
        }
        String node = key.substring(trailingIndex);
        if (section == this) {
            Object value = values.get(node);
            if (value instanceof Map) {
                Map<?, ?> m = (Map<?, ?>) value;
                if (m.containsKey(ConfigurationSerializer.KEY_SERIALIZED_TYPE)) {
                    Map<String, Object> deserialize = new LinkedHashMap<String, Object>(m.size());
                    for (Entry<?, ?> entry : m.entrySet()) {
                        deserialize.put(entry.getKey().toString(), entry.getValue());
                    }
                    value = ConfigurationSerializer.deserializeObject(deserialize);
                }
            } else if (value instanceof List) {
                List<?> old = (List<?>) value;
                List<Object> list = new ArrayList<>();
                for (Object o : old) {
                    if (o instanceof Map) {
                        Map<?, ?> m = (Map<?, ?>) o;
                        if (m.containsKey(ConfigurationSerializer.KEY_SERIALIZED_TYPE)) {
                            Map<String, Object> deserialize = new LinkedHashMap<String, Object>(m.size());
                            for (Entry<?, ?> entry : m.entrySet()) {
                                deserialize.put(entry.getKey().toString(), entry.getValue());
                            }
                            list.add(ConfigurationSerializer.deserializeObject(deserialize));
                            continue;
                        }
                    }
                    list.add(o);
                }
                value = list;
            }
            // /* DEBUG */System.out.println("\t@" + Integer.toHexString(hashCode()) + " value(" + node + ") == " + value);
            return value == null ? defaultValue : value;
        }
        // /* DEBUG */System.out.println("\t@" + Integer.toHexString(hashCode()) + " node == " + node);
        return section.get(node, defaultValue);
    }

    @Override
    public Object getDefault(String key) {
        if (key == null) throw new IllegalArgumentException("key can not be null");

        Configuration root = getRoot();
        Configuration defaults = root == null ? null : root.getDefaults();

        return defaults == null ? null : defaults.get(createKey(this, key));
    }

    /**
     * @see #getConfigurationSection(String, ConfigurationSection)
     */
    @Override
    public ConfigurationSection getConfigurationSection(String key) {
        return getConfigurationSection(key, null);
    }

    /**
     * @see #get(String)
     */
    @Override
    public ConfigurationSection getConfigurationSection(String key, ConfigurationSection defaultValue) {
        Object value = get(key);
        return value instanceof ConfigurationSection ? (ConfigurationSection) value : defaultValue;
    }

    /**
     * @see #getBoolean(String, Boolean)
     */
    @Override
    public Boolean getBoolean(String key) {
        return getBoolean(key, null);
    }

    /**
     * @see #get(String)
     */
    @Override
    public Boolean getBoolean(String key, Boolean defaultValue) {
        Object value = get(key);
        if (value instanceof Boolean) return (Boolean) value;
        if (Boolean.TRUE.toString().equals(value)) return Boolean.TRUE;
        if (Boolean.FALSE.toString().equals(value)) return Boolean.FALSE;
        return defaultValue;
    }

    /**
     * @see #getString(String, String)
     */
    @Override
    public String getString(String key) {
        return getString(key, null);
    }

    /**
     * @see #get(String)
     */
    @Override
    public String getString(String key, String defaultValue) {
        Object value = get(key);
        return value == null ? defaultValue : String.valueOf(value);
    }

    /**
     * @see #getNumber(String, Number)
     */
    @Override
    public Number getNumber(String key) {
        return getNumber(key, null);
    }

    /**
     * {@inheritDoc}
     * <p>
     * If the value of the key is a {@link String}, this will try to parse the {@link String} to a {@link Number}. In case the conversion fails, the default value will be returned.
     * </p>
     * 
     * @see #get(String)
     * @see Double#valueOf(String)
     */
    @Override
    public Number getNumber(String key, Number defaultValue) {
        Object value = get(key);
        if (value instanceof Number) return (Number) value;
        if (value instanceof String) {
            try {
                Double.valueOf((String) value);
            } catch (NumberFormatException e) {
            }
        }
        return defaultValue;
    }

    /**
     * @see #getGeneric(String, Object, Class)
     */
    @Override
    public <T> T getGeneric(String key, Class<T> type) {
        return getGeneric(key, null, type);
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>type</code> is <code>null</code>
     * @see #get(String)
     * @see Class#cast(Object)
     */
    @Override
    public <T> T getGeneric(String key, T defaultValue, Class<T> type) {
        if (type == null) throw new IllegalArgumentException("type can not be null");
        Object value = get(key);
        if (value == null) return defaultValue;
        try {
            if (ConfigurationSerializable.class.isAssignableFrom(type) && !type.isInstance(value)) {
                ConfigurationSerializable ret = (ConfigurationSerializable) defaultValue;
                if (value instanceof Map) {
                    Map<?, ?> map = (Map<?, ?>) value;
                    if (map.containsKey(ConfigurationSerializer.KEY_SERIALIZED_TYPE)) {
                        Map<String, Object> deserialize = new LinkedHashMap<String, Object>(map.size());
                        for (Entry<?, ?> entry : map.entrySet()) {
                            deserialize.put(entry.getKey().toString(), entry.getValue());
                        }
                        ret = ConfigurationSerializer.deserializeObject(deserialize);
                    }
                } else if (value instanceof ConfigurationSection) {
                    Map<String, Object> values = ((ConfigurationSection) value).getValues(true);
                    if (values.containsKey(ConfigurationSerializer.KEY_SERIALIZED_TYPE)) {
                        ret = ConfigurationSerializer.deserializeObject(values);
                    }
                }
                return type.cast(ret);
            }
            return type.cast(value);
        } catch (ClassCastException e) {
            return defaultValue;
        }
    }

    /**
     * @see #getList(String, List)
     */
    @Override
    public List<?> getList(String key) {
        return getList(key, null);
    }

    /**
     * @see #get(String)
     */
    @Override
    public List<?> getList(String key, List<?> defaultValue) {
        Object value = get(key);
        return value instanceof List ? (List<?>) value : defaultValue;
    }

    /**
     * @see #getBooleanList(String, List)
     */
    @Override
    public List<Boolean> getBooleanList(String key) {
        return getBooleanList(key, null);
    }

    /**
     * {@inheritDoc}
     * <p>
     * The returned list will only contain values that could be parsed to a {@link Boolean}.
     * </p>
     * 
     * @see #getList(String)
     */
    @Override
    public List<Boolean> getBooleanList(String key, List<Boolean> defaultValue) {
        List<?> list = getList(key);
        if (list == null) return defaultValue;
        List<Boolean> value = new ArrayList<Boolean>();
        for (Object o : list) {
            if (o instanceof Boolean) {
                value.add((Boolean) o);
            } else if (o instanceof String) {
                if (Boolean.TRUE.toString().equals(o)) {
                    value.add(Boolean.TRUE);
                } else if (Boolean.FALSE.toString().equals(o)) {
                    value.add(Boolean.FALSE);
                }
            }
        }
        return value;
    }

    /**
     * @see #getStringList(String)
     */
    @Override
    public List<String> getStringList(String key) {
        return getStringList(key, null);
    }

    /**
     * {@inheritDoc}
     * <p>
     * The returned list will only contain values that could be parsed to a {@link String}.
     * </p>
     * 
     * @see #getList(String)
     */
    @Override
    public List<String> getStringList(String key, List<String> defaultValue) {
        List<?> list = getList(key);
        if (list == null) return defaultValue;
        List<String> value = new ArrayList<String>();
        for (Object o : list) {
            if (o instanceof String || o instanceof Boolean || o instanceof Character || o instanceof Number) {
                value.add(String.valueOf(o));
            }
        }
        return value;
    }

    /**
     * @see #getNumberList(String, List)
     */
    @Override
    public List<Number> getNumberList(String key) {
        return getNumberList(key, null);
    }

    /**
     * {@inheritDoc}
     * <p>
     * The returned list will only contain values that could be parsed to a {@link Boolean}.
     * </p>
     * 
     * @see #getList(String)
     */
    @Override
    public List<Number> getNumberList(String key, List<Number> defaultValue) {
        List<?> list = getList(key);
        if (list == null) return defaultValue;
        List<Number> value = new ArrayList<Number>();
        for (Object o : list) {
            if (o instanceof Number) {
                value.add((Number) o);
            } else if (o instanceof String) {
                try {
                    value.add(Double.valueOf(o.toString()));
                } catch (NumberFormatException e) {
                }
            }
        }
        return value;
    }

    /**
     * @see #getGenericList(String, List, Class)
     */
    @Override
    public <T> List<T> getGenericList(String key, Class<T> type) {
        return getGenericList(key, null, type);
    }

    /**
     * @throws IllegalArgumentException
     *             thrown if <code>type</code> is <code>null</code>
     * @see #getList(String)
     * @see Class#cast(Object)
     */
    @Override
    public <T> List<T> getGenericList(String key, List<T> defaultValue, Class<T> type) {
        if (type == null) throw new IllegalArgumentException("type can not be null");
        List<?> list = getList(key);
        if (list == null) return defaultValue;
        List<T> value = new ArrayList<T>();
        for (Object o : list) {
            try {
                value.add(type.cast(o));
            } catch (ClassCastException e) {
            }
        }
        return value;
    }

    /**
     * {@inheritDoc}
     * <p>
     * This will also create any non-existent parent sections. E.g. the key <code>"foo.bar.baz"</code> will create sections at <code>"foo"</code> and <code>"foo.baz"</code> if they don't exist yet.
     * </p>
     * 
     * @throws IllegalArgumentException
     *             thrown if <code>key</code> is <code>null</code> or empty
     */
    @Override
    public ConfigurationSection createSection(String key) {
        if (key == null || key.isEmpty()) throw new IllegalArgumentException("key can not be null or empty");
        char separator = getRoot().options().separator();
        int leadingIndex = -1;
        int trailingIndex;
        ConfigurationSection section = this;
        while ((leadingIndex = key.indexOf(separator, trailingIndex = leadingIndex + 1)) >= 0) {
            String node = key.substring(trailingIndex, leadingIndex);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null) {
                section = section.createSection(node);
            } else {
                section = subSection;
            }
        }
        String node = key.substring(trailingIndex);
        if (section == this) {
            ConfigurationSection newSection = new MemorySection(this, node);
            values.put(node, newSection);
            // System.out.println("! created new section " + newSection.getFullKey());
            return newSection;
        }
        return section.createSection(key);
    }

    /**
     * @see #createSection(String)
     * @see #set(String, Object)
     */
    @Override
    public ConfigurationSection createSection(String key, Map<?, ?> values) {
        ConfigurationSection newSection = createSection(key);
        for (Entry<?, ?> entry : values.entrySet()) {
            String entryKey = entry.getKey().toString();
            Object value = entry.getValue();
            if (value instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) value;
                if (map.containsKey(ConfigurationSerializer.KEY_SERIALIZED_TYPE)) {
                    Map<String, Object> deserialize = new LinkedHashMap<String, Object>(map.size());
                    for (Entry<?, ?> entry2 : map.entrySet()) {
                        deserialize.put(entry2.getKey().toString(), entry2.getValue());
                    }
                    newSection.set(entryKey, ConfigurationSerializer.deserializeObject(deserialize));
                } else {
                    newSection.createSection(entryKey, map);
                }
            } else {
                newSection.set(entryKey, value);
            }
        }
        // System.out.println("+ created new section and assigned values:\n\tsection: " + newSection.getFullKey() + "\n\tvalues: ");
        return newSection;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getSimpleName());
        builder.append("@").append(Integer.toHexString(hashCode()));
        builder.append("[");
        builder.append("fullKey=").append(fullKey).append(";");
        builder.append("key=").append(key).append(";");
        builder.append("values=").append(values);
        builder.append("]");
        return builder.toString();
    }

    public Map<String, Object> toMap() {
        return new HashMap<String, Object>(values);
    }

    private static void print(ConfigurationSection c, String indent, PrintStream stream) {
        Map<String, Object> values = c.getValues(false);
        if (values.isEmpty()) {
            stream.println(indent + "[]");
            return;
        }
        for (Entry<String, Object> entry : values.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof ConfigurationSection) {
                stream.println(indent + key + " -> " + value.getClass().getSimpleName());
                print((ConfigurationSection) value, indent + "  ");
                continue;
            }
            stream.println(indent + key + " -> " + value + "(" + (value == null ? "null" : value.getClass().getName()) + ")");
        }
    }

    private static void print(ConfigurationSection c, String indent) {
        print(c, indent, System.out);
    }

    /**
     * Prints a {@link ConfigurationSection} and its contents to the default output stream.
     * 
     * @param c
     *            the ConfigurationSection to print
     */
    public static void print(ConfigurationSection c) {
        print(c, "");
    }

    public static String createKey(ConfigurationSection c, String key) {
        return createKey(c, key, (c == null) ? null : c.getRoot());
    }

    public static String createKey(ConfigurationSection c, String key, ConfigurationSection relativeTo) {
        if (c == null) throw new IllegalArgumentException("c can not be null");

        Configuration root = c.getRoot();
        if (root == null) throw new IllegalArgumentException("root can not be null");

        Character separator = root.options().separator();
        StringBuilder builder = new StringBuilder();

        for (ConfigurationSection parent = c; (parent != null) && (parent != relativeTo); parent = parent.getParent()) {
            if (builder.length() > 0) {
                builder.insert(0, separator);
            }
            builder.insert(0, parent.getKey());
        }

        if (key != null && key.length() > 0) {
            if (builder.length() > 0) {
                builder.append(separator);
            }
            builder.append(key);
        }

        return builder.toString();
    }
}
