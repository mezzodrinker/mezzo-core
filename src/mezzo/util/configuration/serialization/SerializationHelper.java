package mezzo.util.configuration.serialization;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <code>SerializationHelper</code>
 * 
 * @author mezzodrinker
 */
public class SerializationHelper {
    protected SerializationHelper() {};

    public static String asString(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        return String.valueOf(o);
    }

    public static List<String> asStringList(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (!(value instanceof List)) throw new IllegalArgumentException(key + "(" + value + ") is not a string list");
        List<?> l = (List<?>) value;
        List<String> ret = new ArrayList<String>();
        for (Object o : l) {
            ret.add(String.valueOf(o));
        }
        return ret;
    }

    public static Number asNumber(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (value instanceof String) {
            NumberFormat format = NumberFormat.getInstance();
            try {
                return format.parse((String) value);
            } catch (ParseException e) {
            }
        }
        if (!(value instanceof Number)) throw new IllegalArgumentException(key + "(" + value + ") is not a number");
        return (Number) value;
    }

    public static int asInteger(Map<String, Object> map, String key) {
        return asNumber(map, key).intValue();
    }

    public static double asDouble(Map<String, Object> map, String key) {
        return asNumber(map, key).doubleValue();
    }
}
