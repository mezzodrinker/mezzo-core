package mezzo.util.configuration.serialization;

import java.util.Map;

/**
 * <code>ConfigurationSerializable</code>
 * <p>
 * Represents a class that may be serialized.
 * </p>
 * <p>
 * In addition to the methods defined by this interface, the class has to implement <b>at least one</b> of the following other methods.
 * <ul>
 * <li><code>public static {@link ConfigurationSerializable} deserialize({@link Map}&lt;{@link String}, {@link Object}&gt;)</code></li>
 * <li>constructor that accepts {@link Map}&lt;{@link String}, {@link Object}&gt;
 * </ul>
 * </p>
 * <p>
 * Note that before this class can be serialized/deserialized by the {@link ConfigurationSerializer} you have to register it by using {@link ConfigurationSerializer#registerClass(Class)} or
 * {@link ConfigurationSerializer#registerClass(Class, String)}. Instead of using the latter, you can instead use the annotation {@link SerializeAs @SerializeAs} to set this class' alias.
 * </p>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 * @see SerializeAs
 */
public interface ConfigurationSerializable {
    /**
     * <p>
     * Creates a map representation of this instance.
     * </p>
     * <p>
     * This class must provide a way to restore this object from a map.
     * </p>
     * 
     * @return Map containing the current state of this instance
     */
    public Map<String, Object> serialize();
}
