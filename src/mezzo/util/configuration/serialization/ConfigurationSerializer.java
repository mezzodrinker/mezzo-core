package mezzo.util.configuration.serialization;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mezzo.util.time.TimeUnit;
import mezzo.util.version.Version;

/**
 * <code>ConfigurationSerializer</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public class ConfigurationSerializer {
    private final Class<? extends ConfigurationSerializable>                       serializableClass;
    protected static final Map<String, Class<? extends ConfigurationSerializable>> aliases                     = new HashMap<String, Class<? extends ConfigurationSerializable>>();
    public static final String                                                     DESERIALIZATION_METHOD_NAME = "deserialize";
    public static final String                                                     KEY_SERIALIZED_TYPE         = "->";

    static {
        registerClass(Version.class);
        registerClass(TimeUnit.class);
    }

    /**
     * <p>
     * Creates a new instance of <code>ConfigurationSerializer</code> that can serialize and deserialize instances of a class.
     * </p>
     * <p>
     * The class must fulfill the requirements as specified in {@link ConfigurationSerializable}.
     * </p>
     * 
     * @param serializableClass
     *            the class that this instance can serialize and deserialize
     */
    protected ConfigurationSerializer(Class<? extends ConfigurationSerializable> serializableClass) {
        this.serializableClass = serializableClass;
    }

    /**
     * Returns a method with given identifier and {@link Map} as the only argument.
     * 
     * @param identifier
     *            the method's identifier
     * @param staticMethod
     *            determines if searching for a static or non-static method
     * @return <code>null</code> if an error occurred or no suiting method was found, else the method
     */
    protected Method getDeserializationMethod(String identifier, boolean staticMethod) {
        try {
            Method method = serializableClass.getDeclaredMethod(identifier, Map.class);
            if (!ConfigurationSerializable.class.isAssignableFrom(method.getReturnType())) return null;
            if (Modifier.isStatic(method.getModifiers()) != staticMethod) return null;
            return method;
        } catch (NullPointerException | NoSuchMethodException | SecurityException e) {
            return null;
        }
    }

    /**
     * Returns a constructor that accepts {@link Map} as it's only argument.
     * 
     * @return <code>null</code> if an error occurred or no suiting constructor was found, else the constructor
     */
    protected Constructor<? extends ConfigurationSerializable> getDeserializationConstructor() {
        try {
            return serializableClass.getConstructor(Map.class);
        } catch (NoSuchMethodException | SecurityException e) {
            return null;
        }
    }

    /**
     * Deserializes a map using the specified method.
     * 
     * @param method
     *            the method to use to deserialize the map, needs to accept <code>{@link Map}&lt;{@link String}, {@link Object}&gt;</code> as its argument
     * @param args
     *            the map to deserialize
     * @return <code>null</code> if an error occurred, else a <code>{@link ConfigurationSerializable}</code> instance
     * @see #deserialize(Constructor, Map)
     */
    protected ConfigurationSerializable deserialize(Method method, Map<String, Object> args) {
        try {
            ConfigurationSerializable ret = (ConfigurationSerializable) method.invoke(this, args);
            if (ret == null) {
                System.err.println("could not call method '" + method.getName() + "' of " + serializableClass.getName() + " for deserialization: " + ret);
            } else
                return ret;
        } catch (NullPointerException | IllegalAccessException | InvocationTargetException e) {
            System.err.println("could not call method '" + method.getName() + "' of " + serializableClass.getName() + " for deserialization");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Deserializes a map using the specified constructor.
     * 
     * @param constructor
     *            the constructor to use to deserialize the map, needs to accept <code>{@link Map}&lt;{@link String}, {@link Object}&gt;</code> as its argument
     * @param args
     *            the map to deserialize
     * @return <code>null</code> if an error occurred, else a <code>{@link ConfigurationSerializable}</code> instance
     * @see #deserialize(Method, Map)
     */
    protected ConfigurationSerializable deserialize(Constructor<? extends ConfigurationSerializable> constructor, Map<String, Object> args) {
        try {
            return constructor.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            return null;
        }
    }

    /**
     * Deserializes a map.
     * 
     * @param args
     *            the map to deserialize
     * @return <code>null</code> if the map couldn't be deserialized, else a <code>{@link ConfigurationSerializable}</code> instance
     * @throws IllegalArgumentException
     *             if <code>args</code> is <code>null</code>
     * @see #deserialize(Constructor, Map)
     * @see #deserialize(Method, Map)
     */
    public ConfigurationSerializable deserialize(Map<String, Object> args) {
        if (args == null) throw new IllegalArgumentException("args can not be null");
        ConfigurationSerializable ret = null;
        Method method = getDeserializationMethod(DESERIALIZATION_METHOD_NAME, true);
        if (method != null) {
            ret = deserialize(method, args);
            if (ret != null) return ret;
        }
        Constructor<? extends ConfigurationSerializable> constructor = getDeserializationConstructor();
        if (constructor != null) {
            ret = deserialize(constructor, args);
            if (ret != null) return ret;
        }
        return ret;
    }

    /**
     * Deserializes a map.
     * 
     * @param args
     *            the map to deserialize
     * @param deserializeClass
     *            the deserialized instance's class
     * @return a {@link ConfigurationSerializable} instance that is represented by the map
     * @see #deserialize(Map)
     */
    public static ConfigurationSerializable deserializeObject(Map<String, Object> args, Class<? extends ConfigurationSerializable> deserializeClass) {
        return new ConfigurationSerializer(deserializeClass).deserialize(args);
    }

    /**
     * Deserializes a map. The return value's class is determined by the value of the key {@value #KEY_SERIALIZED_TYPE} which has to equal either...
     * <ul>
     * <li>the class' alias that was determined by {@link #registerClass(Class, String)} <b>or</b></li>
     * <li>the class' alias that was determined by the annotation {@link SerializeAs @SerializeAs} <b>or</b></li>
     * <li>the class' {@link Class#getName() fully qualified name}</li>
     * </ul>
     * ..., depending on which one was used when the class was registered.
     * 
     * @param args
     *            the map to deserialize
     * @return a {@link ConfigurationSerializable} instance that is represented by the map
     * @throws IllegalArgumentException
     *             thrown if...
     *             <ul>
     *             <li><code>args</code> does not contain the key {@value #KEY_SERIALIZED_TYPE}</li>
     *             <li>the alias of the specified class equals <code>null</code></li>
     *             <li>the specified class has not been registered or does not exist</li>
     *             </ul>
     * @see #deserializeObject(Map, Class)
     * @see #serializeObject(ConfigurationSerializable)
     * @see #registerClass(Class)
     * @see #registerClass(Class, String)
     */
    public static ConfigurationSerializable deserializeObject(Map<String, Object> args) {
        if (!args.containsKey(KEY_SERIALIZED_TYPE)) throw new IllegalArgumentException("the deserialization map does not contain a key '" + KEY_SERIALIZED_TYPE + "'");
        Class<? extends ConfigurationSerializable> deserializeClass = null;
        try {
            String alias = args.get(KEY_SERIALIZED_TYPE).toString();
            if (alias == null) throw new IllegalArgumentException("class alias must not be null");
            deserializeClass = aliases.get(alias);
            if (deserializeClass == null) throw new IllegalArgumentException("specified class '" + alias + "' is not registered or does not exist");
        } catch (NullPointerException | ClassCastException e) {
            e.fillInStackTrace();
            throw e;
        }
        return deserializeObject(args, deserializeClass);
    }

    /**
     * Serializes a {@link ConfigurationSerializable} instance.
     * 
     * @param o
     *            the instance to serialize
     * @return a map containing the values of <code>{@link ConfigurationSerializable#serialize() o.serialize()}</code> and an additional key {@value #KEY_SERIALIZED_TYPE} set to the instance's class'
     *         alias.
     * @throws IllegalArgumentException
     *             if <code>o</code> is <code>null</code> or the instance's class has not been registered
     */
    public static Map<String, Object> serializeObject(ConfigurationSerializable o) {
        if (o == null) throw new IllegalArgumentException("can not serialize null");
        Class<?> theClass = o.getClass();
        String alias = null;
        for (Entry<String, Class<? extends ConfigurationSerializable>> entry : aliases.entrySet()) {
            if (entry.getValue().equals(theClass)) {
                alias = entry.getKey();
            }
        }
        if (alias == null) throw new IllegalArgumentException("specified class " + theClass.getName() + " is not registered");
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(KEY_SERIALIZED_TYPE, alias);
        map.putAll(o.serialize());
        for (Entry<String, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof ConfigurationSerializable && value != o) {
                map.put(entry.getKey(), serializeObject((ConfigurationSerializable) value));
            } else if (value.getClass().getComponentType() != null) {
                Object[] array = (Object[]) value;
                List<Object> list = new ArrayList<Object>();
                for (Object element : array) {
                    if (element instanceof ConfigurationSerializable && element != o) {
                        list.add(serializeObject((ConfigurationSerializable) element));
                        continue;
                    }
                    list.add(element);
                }
                map.put(entry.getKey(), list);
            } else if (value instanceof Iterable) {
                Iterable<?> iterable = (Iterable<?>) value;
                List<Object> list = new ArrayList<Object>();
                for (Object element : iterable) {
                    if (element instanceof ConfigurationSerializable && element != o) {
                        list.add(serializeObject((ConfigurationSerializable) element));
                        continue;
                    }
                    list.add(element);
                }
                map.put(entry.getKey(), list);
            }
        }
        return map;
    }

    /**
     * Registers a class to the {@link ConfigurationSerializer} so that it can be deserialized. The alias by which this class will be deserialized is...
     * <ul>
     * <li><b>if the class has an {@link SerializeAs alias annotation}:</b> the annotation's value</li>
     * <li><b>if the class does not have an alias annotation:</b> the class' {@link Class#getName() fully qualified name}</li>
     * </ul>
     * 
     * @param deserializeClass
     *            the class to register
     * @throws IllegalArgumentException
     *             if <code>deserializeClass</code> is <code>null</code>
     * @see #registerClass(Class, String)
     */
    public static void registerClass(Class<? extends ConfigurationSerializable> deserializeClass) {
        if (deserializeClass == null) throw new IllegalArgumentException("can not register null");
        SerializeAs annot = deserializeClass.getAnnotation(SerializeAs.class);
        if (annot == null || annot.value() == null || annot.value().isEmpty()) {
            registerClass(deserializeClass, deserializeClass.getName());
        } else {
            registerClass(deserializeClass, annot.value());
        }
    }

    /**
     * <p>
     * Registers a class to the {@link ConfigurationSerializer} so that it can be deserialized. The specified alias has to be unique!
     * </p>
     * <p>
     * Note that this method won't overwrite any aliases of other classes. If the class has already been registered, this method will also remove all other aliases of the specified class.
     * </p>
     * 
     * @param deserializeClass
     *            the class to register
     * @param alias
     *            the alias to register the class by
     * @throws IllegalArgumentException
     *             thrown if...
     *             <ul>
     *             <li><code>deserializeClass</code> is <code>null</code></li>
     *             <li><code>alias</code> is <code>null</code></li>
     *             <li>the specified alias is already given to another class
     *             </ul>
     */
    public static void registerClass(Class<? extends ConfigurationSerializable> deserializeClass, String alias) {
        if (deserializeClass == null) throw new IllegalArgumentException("can not register null");
        if (alias == null || alias.isEmpty()) throw new IllegalArgumentException("can not use '" + alias + "' as an alias");
        if (aliases.get(alias) != null && aliases.get(alias) != deserializeClass)
            throw new IllegalArgumentException("the alias '" + alias + "' is already given to the class " + aliases.get(alias).getName());
        if (aliases.containsValue(deserializeClass)) {
            for (Entry<String, Class<? extends ConfigurationSerializable>> entry : aliases.entrySet()) {
                if (entry.getValue().equals(deserializeClass)) {
                    aliases.remove(entry.getKey());
                }
            }
        }
        aliases.put(alias, deserializeClass);
    }

    /**
     * Unregisters an alias from the {@link ConfigurationSerializer}.
     * 
     * @param alias
     *            the alias to unregister
     * @return <code>true</code> if the alias was unregistered successfully, <code>false</code> if the alias did not exist or could not be unregistered
     * @throws IllegalArgumentException
     *             if <code>alias</code> is <code>null</code>
     * @see #unregisterClass(Class)
     */
    public static boolean unregisterClass(String alias) {
        if (alias == null) throw new IllegalArgumentException("can not unregister null");
        if (!aliases.containsKey(alias)) return false;
        aliases.remove(alias);
        return true;
    }

    /**
     * Unregisters a class from the {@link ConfigurationSerializer}, removing all aliases for it.
     * 
     * @param deserializeClass
     *            the class to unregister
     * @return <code>true</code> if the class was unregistered successfully, <code>false</code> if the class has not been registered or could not be unregistered
     * @throws IllegalArgumentException
     *             if <code>deserializeClass</code> is <code>null</code>
     * @see #unregisterClass(String)
     */
    public static boolean unregisterClass(Class<? extends ConfigurationSerializable> deserializeClass) {
        if (deserializeClass == null) throw new IllegalArgumentException("can not unregister null");
        if (!aliases.containsValue(deserializeClass)) return false;
        boolean success = false;
        for (Entry<String, Class<? extends ConfigurationSerializable>> entry : aliases.entrySet()) {
            if (entry.getValue().equals(deserializeClass)) {
                aliases.remove(entry.getKey());
                success = true;
            }
        }
        return success;
    }
}
