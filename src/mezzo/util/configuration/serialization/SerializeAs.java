package mezzo.util.configuration.serialization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <code>SerializeAs</code>
 * <p>
 * This annotation represents an <i>alias</i> a {@link ConfigurationSerializable} may be (re)stored as. If this is not present on a {@link ConfigurationSerializable} class, the
 * {@link ConfigurationSerializer} will use the fully qualified name of that class instead.
 * </p>
 * <p>
 * The value of this annotation will be stored in the configuration alongside the serialized instance so that the {@link ConfigurationSerializer} can determine the type of that instance.
 * </p>
 * <p>
 * Using this annotation on any class that does not implement {@link ConfigurationSerializable} will have no effect.
 * </p>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SerializeAs {
    /**
     * This is the name your class will be recognized as by the {@link ConfigurationSerializer}. Thus, it <b>must be</b> unique!
     * 
     * @return name to serialize the class as
     */
    public String value();
}
