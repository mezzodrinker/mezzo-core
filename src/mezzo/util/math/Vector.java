package mezzo.util.math;

/**
 * <code>Vector</code>
 * 
 * @author mezzodrinker
 */
public abstract class Vector implements Cloneable {
    protected static final double epsilon = 0.000001d;

    public abstract double getX();

    public abstract double getY();

    public abstract double getZ();

    public abstract Vector add(Vector v);

    public abstract Vector subtract(Vector v);

    public abstract Vector multiply(Vector v);

    public abstract Vector divide(Vector v);

    public abstract Vector multiply(double d);

    public abstract Vector divide(double d);

    public abstract Vector cross(Vector v);

    public abstract double scalar(Vector v);

    public abstract double abs();

    public abstract Vector normalize();

    public abstract Vector rotate(double degrees);

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract Vector clone();
}
