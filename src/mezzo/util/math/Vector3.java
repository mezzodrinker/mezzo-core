package mezzo.util.math;

import static mezzo.util.configuration.serialization.SerializationHelper.asDouble;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.configuration.serialization.SerializeAs;

/**
 * <code>Vector3</code> - three-dimensional vectors
 * 
 * @author mezzodrinker
 */
@SerializeAs("Vector3")
public class Vector3 extends Vector implements ConfigurationSerializable {
    public final double           x;
    public final double           y;
    public final double           z;

    protected static final String SERIALIZE_X = "x";
    protected static final String SERIALIZE_Y = "y";
    protected static final String SERIALIZE_Z = "z";

    static {
        ConfigurationSerializer.registerClass(Vector3.class);
    }

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public Vector3 add(Vector v) {
        double x = this.x + v.getX();
        double y = this.y + v.getY();
        double z = this.z + v.getZ();
        return new Vector3(x, y, z);
    }

    @Override
    public Vector3 subtract(Vector v) {
        double x = this.x - v.getX();
        double y = this.y - v.getY();
        double z = this.z - v.getZ();
        return new Vector3(x, y, z);
    }

    @Override
    public Vector3 multiply(Vector v) {
        double x = this.x * v.getX();
        double y = this.y * v.getY();
        double z = this.z * v.getZ();
        return new Vector3(x, y, z);
    }

    @Override
    public Vector3 divide(Vector v) {
        double x = this.x / v.getX();
        double y = this.y / v.getY();
        double z = this.z / v.getZ();
        return new Vector3(x, y, z);
    }

    @Override
    public Vector3 multiply(double d) {
        double x = this.x * d;
        double y = this.y * d;
        double z = this.z * d;
        return new Vector3(x, y, z);
    }

    @Override
    public Vector3 divide(double d) {
        double x = this.x / d;
        double y = this.y / d;
        double z = this.z / d;
        return new Vector3(x, y, z);
    }

    @Override
    public Vector3 cross(Vector v) {
        double x = y * v.getZ() - v.getY() * z;
        double y = z * v.getX() - v.getZ() * this.x;
        double z = this.x * v.getY() - v.getX() * this.y;
        return new Vector3(x, y, z);
    }

    @Override
    public double scalar(Vector v) {
        return x * v.getX() + y * v.getY() + z * v.getZ();
    }

    @Override
    public double abs() {
        return Math.sqrt(scalar(this));
    }

    @Override
    public Vector3 normalize() {
        return divide(abs());
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(SERIALIZE_X, x);
        map.put(SERIALIZE_Y, y);
        map.put(SERIALIZE_Z, z);
        return Collections.unmodifiableMap(map);
    }

    @Override
    public Vector3 rotate(double degrees) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vector3)) return false;
        Vector3 v = (Vector3) o;
        return Math.abs(x - v.x) < epsilon && Math.abs(y - v.y) < epsilon && Math.abs(z - v.z) < epsilon;
    }

    @Override
    public Vector3 clone() {
        return new Vector3(x, y, z);
    }

    public static Vector3 deserialize(Map<String, Object> map) {
        double x = asDouble(map, SERIALIZE_X);
        double y = asDouble(map, SERIALIZE_Y);
        double z = asDouble(map, SERIALIZE_Z);
        return new Vector3(x, y, z);
    }
}
