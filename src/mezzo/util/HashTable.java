package mezzo.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * <code>Table</code>
 * 
 * @author mezzodrinker
 * @param <K>
 *            first key type
 * @param <T>
 *            second key type
 * @param <V>
 *            value type
 */
public class HashTable<K, T, V> {
    private Map<Key2<K, T>, V> table = new HashMap<Key2<K, T>, V>();

    public void foreach(BiConsumer<Key2<K, T>, V> action) {
        for (Entry<Key2<K, T>, V> entry : entrySet()) {
            action.accept(entry.getKey(), entry.getValue());
        }
    }

    public V set(K key1, T key2, V value) {
        return table.put(new Key2<>(key1, key2), value);
    }

    public V get(K key1, T key2) {
        return table.get(new Key2<>(key1, key2));
    }

    public Set<V> getForFirstKey(K key1) {
        Set<V> set = new HashSet<V>();
        for (Entry<Key2<K, T>, V> entry : entrySet()) {
            if (entry.getKey().part1.equals(key1)) {
                set.add(entry.getValue());
            }
        }
        return set.isEmpty() ? null : set;
    }

    public Set<V> getForSecondKey(T key2) {
        Set<V> set = new HashSet<V>();
        for (Entry<Key2<K, T>, V> entry : entrySet()) {
            if (entry.getKey().part2.equals(key2)) {
                set.add(entry.getValue());
            }
        }
        return set.isEmpty() ? null : set;
    }

    public boolean remove(K key1, T key2, V value) {
        return table.remove(new Key2<>(key1, key2), value);
    }

    public V remove(K key1, T key2) {
        return table.remove(new Key2<>(key1, key2));
    }

    public int size() {
        return table.size();
    }

    public boolean contains(K key1, T key2) {
        return table.containsKey(new Key2<>(key1, key2));
    }

    public boolean contains(V value) {
        return table.containsValue(value);
    }

    public Collection<? extends V> values() {
        return table.values();
    }

    public Set<Entry<Key2<K, T>, V>> entrySet() {
        return table.entrySet();
    }

    @Override
    public String toString() {
        return table.toString();
    }

    /**
     * <code>Key2</code>
     * 
     * @param <K1>
     *            first key component
     * @param <K2>
     *            second key component
     * @author mezzodrinker
     * @author <a href="http://stackoverflow.com/users/22656/jon-skeet">John Skeet</a>, in an answer on <a
     *         href="http://stackoverflow.com/questions/10787860/map-with-two-key-for-a-value">StackOverflow</a>
     */
    public static final class Key2<K1, K2> {
        public final K1 part1;
        public final K2 part2;

        public Key2(K1 part1, K2 part2) {
            if (part1 == null || part2 == null) throw new NullPointerException("key components can not be null");
            this.part1 = part1;
            this.part2 = part2;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Key2)) return false;
            Key2<?, ?> key = (Key2<?, ?>) o;
            return part1.equals(key.part1) && part2.equals(key.part2);
        }

        @Override
        public int hashCode() {
            int hash = 23;
            hash = hash * 17 + part1.hashCode();
            hash = hash * 31 + part2.hashCode();
            return hash;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "{" + part1 + "," + part2 + "}";
        }
    }
}
