package mezzo.util.validation;

import java.util.Objects;

/**
 * <code>Validate</code>
 * 
 * @author mezzodrinker
 * @since mezzo-core 1.1.77
 */
public class Validate {
    private Validate() {}

    /**
     * 
     * @param value
     * @param allowedValues
     * @return
     * @since mezzo-core 1.1.77
     */
    @SafeVarargs
    public static <T> T validate(T value, T... allowedValues) {
        return validate(value, String.valueOf(value), allowedValues);
    }

    /**
     * 
     * @param value
     * @param message
     * @param allowedValues
     * @return
     * @since mezzo-core 1.1.77
     */
    @SafeVarargs
    public static <T> T validate(T value, String message, T... allowedValues) {
        if (allowedValues == value) return value;
        if (allowedValues == null) throw new IllegalArgumentException(message);
        for (T t : allowedValues) {
            if (Objects.equals(value, t)) return value;
        }
        throw new IllegalArgumentException(message);
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static <T> T[] validate(T[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static <T> T[] validate(T[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static boolean[] validate(boolean[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static boolean[] validate(boolean[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static byte[] validate(byte[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static byte[] validate(byte[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static short[] validate(short[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static short[] validate(short[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static char[] validate(char[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static char[] validate(char[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static int[] validate(int[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static int[] validate(int[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static float[] validate(float[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static float[] validate(float[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static long[] validate(long[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static long[] validate(long[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }

    /**
     * 
     * @param array
     * @param length
     * @return
     * @since mezzo-core 1.1.78
     */
    public static double[] validate(double[] array, int length) {
        return validate(array, length, "" + (array == null ? "null" : "invalid array length " + array.length));
    }

    /**
     * 
     * @param array
     * @param length
     * @param message
     * @return
     * @since mezzo-core 1.1.78
     */
    public static double[] validate(double[] array, int length, String message) {
        if (array == null) throw new NullPointerException(message);
        if (array.length != length) throw new IllegalArgumentException(message);
        return array;
    }
}
