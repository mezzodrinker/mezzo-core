package mezzo.util;

import java.util.NoSuchElementException;

/**
 * <code>Iterator</code>
 * 
 * @param <T>
 *            the type of elements returned by this iterator
 * @author mezzodrinker
 * @since
 */
public interface Iterator<T> extends java.util.Iterator<T> {
    /**
     * @return current element
     * @throws NoSuchElementException
     *             if the iterator reached the end
     */
    public T get();

    /**
     * @throws UnsupportedOperationException
     *             if the {@code reset} operation is not supported by this iterator
     */
    default public void reset() {
        throw new UnsupportedOperationException("reset");
    }
}
