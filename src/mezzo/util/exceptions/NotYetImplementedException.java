package mezzo.util.exceptions;

/**
 * <code>NotYetImplementedException</code>
 * 
 * @author mezzodrinker
 */
public class NotYetImplementedException extends UnsupportedOperationException {
    private static final long serialVersionUID = 1L;
}
