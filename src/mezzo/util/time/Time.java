package mezzo.util.time;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * <code>Time</code>
 * 
 * @author mezzodrinker
 */
public final class Time {
    private static final Calendar c = Calendar.getInstance();

    private Time() {}

    public static TimeZone getTimezone() {
        return Calendar.getInstance().getTimeZone();
    }

    public static String getTimezoneAsString() {
        long offset = getTimezone().getOffset(System.currentTimeMillis());
        if (offset == 0) return "GMT+00.00";
        int sign = (int) Math.signum(offset);
        long hours = offset / 3600000;
        offset -= hours * 3600000;
        long minutes = offset / 60000;
        String h = String.valueOf(Math.abs(hours));
        String min = String.valueOf(Math.abs(minutes));
        while (h.length() < 2) {
            h = "0" + h;
        }
        while (min.length() < 2) {
            min = "0" + min;
        }
        return "GMT" + (sign > 0 ? "+" : "-") + h + "." + min;
    }

    public static String getCurrentDateTimeAsString() {
        return getCurrentDateAsString() + " " + getCurrentTimeAsString();
    }

    public static String getCurrentDateAsString() {
        c.setTimeInMillis(System.currentTimeMillis());
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH)) + 1;
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

        while (year.length() < 4) {
            year = "0" + year;
        }

        while (month.length() < 2) {
            month = "0" + month;
        }

        while (day.length() < 2) {
            day = "0" + day;
        }

        return day + "-" + month + "-" + year;
    }

    public static String getCurrentTimeAsString() {
        c.setTimeInMillis(System.currentTimeMillis());
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(c.get(Calendar.MINUTE));
        String second = String.valueOf(c.get(Calendar.SECOND));

        while (hour.length() < 2) {
            hour = "0" + hour;
        }

        while (minute.length() < 2) {
            minute = "0" + minute;
        }

        while (second.length() < 2) {
            second = "0" + second;
        }

        return hour + "." + minute + "." + second;
    }

    public static String getCurrentDayAsString() {
        c.setTimeInMillis(System.currentTimeMillis());
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

        if (day.endsWith("1") && !day.equals("11")) {
            day += "st";
        } else if (day.endsWith("2") && !day.equals("12")) {
            day += "nd";
        } else if (day.endsWith("3") && !day.equals("13")) {
            day += "rd";
        } else {
            day += "th";
        }

        return day;
    }

    public static String getCurrentMonthName() {
        c.setTimeInMillis(System.currentTimeMillis());
        int month = c.get(Calendar.MONTH);
        switch (month) {
            case 0:
                return "January";
            case 1:
                return "Febuary";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
        }
        return null;
    }
}
