package mezzo.util.time;

import java.util.HashMap;
import java.util.Map;

import mezzo.util.configuration.serialization.ConfigurationSerializable;

/**
 * <code>TimeUnits</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
public enum TimeUnit implements ConfigurationSerializable {
    /**
     * Equals 1ns (nano second).
     */
    NANO_SECOND(1L, "ns"),
    /**
     * Equals 1�s (micro second) or 1000ns.
     * 
     * @see #NANO_SECOND
     */
    MICRO_SECOND(NANO_SECOND.getTimeNano() * 1000L, "�s"),
    /**
     * Equals 1ms (milli second) or 1000�s.
     * 
     * @see #MICRO_SECOND
     */
    MILLI_SECOND(MICRO_SECOND.getTimeNano() * 1000L, "ms"),
    /**
     * Equals 1s (second) or 1000ms.
     * 
     * @see #MILLI_SECOND
     */
    SECOND(MILLI_SECOND.getTimeNano() * 1000L, "s"),
    /**
     * Equals 1min (minute) or 60s.
     * 
     * @see #SECOND
     */
    MINUTE(SECOND.getTimeNano() * 60L, "min"),
    /**
     * Equals 1h (hour) or 60min.
     * 
     * @see #MINUTE
     */
    HOUR(MINUTE.getTimeNano() * 60L, "h"),
    /**
     * Equals 1d (day) or 24h.
     * 
     * @see #HOUR
     */
    DAY(HOUR.getTimeNano() * 24L, "d"),
    /**
     * Equals 1w (week) or 7d.
     * 
     * @see #DAY
     */
    WEEK(DAY.getTimeNano() * 7L, "w"),
    /**
     * Equals 1m (month) or 30d.
     * 
     * @see #DAY
     */
    MONTH(DAY.getTimeNano() * 30L, "m"),
    /**
     * Equals 1y (year) or 365d.
     * 
     * @see #DAY
     */
    YEAR(DAY.getTimeNano() * 365L, "y");

    private long                timeInNanoSec;
    private String              symbol;
    private static final String KEY_SYMBOL = "symbol";

    private TimeUnit(long timeInNanoSec, String symbol) {
        this.timeInNanoSec = timeInNanoSec;
        this.symbol = symbol;
    }

    public long getTimeNano() {
        return timeInNanoSec;
    }

    /**
     * Converts this <code>TimeUnit</code>'s value into another <code>TimeUnit</code>.
     * 
     * @param unit
     *            the other <code>TimeUnit</code>
     * @return the time in the other <code>TimeUnit</code>
     */
    public long getTimeIn(TimeUnit unit) {
        return timeInNanoSec / unit.getTimeNano();
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(KEY_SYMBOL, symbol);
        return map;
    }

    public static TimeUnit deserialize(Map<String, Object> args) {
        Object value = args.get(KEY_SYMBOL);
        if (value == null) return null;
        return get(value.toString());
    }

    public static TimeUnit get(String symbol) {
        if (symbol == null) return null;
        for (TimeUnit unit : values()) {
            if (unit.getSymbol().equals(symbol)) return unit;
        }
        return null;
    }
}
