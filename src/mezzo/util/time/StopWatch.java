package mezzo.util.time;

/**
 * Copyright (c) 2005, Corey Goldberg<br/>
 * <br/>
 * StopWatch.java is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * @author Corey Goldberg
 */
public class StopWatch {
    private long    startTime = 0;
    private long    stopTime  = 0;
    private boolean running   = false;

    public void start() {
        startTime = System.currentTimeMillis();
        running = true;
    }

    public void stop() {
        stopTime = System.currentTimeMillis();
        running = false;
    }

    // elapsed time in milliseconds
    public long getElapsedTime() {
        long elapsed;
        if (running) {
            elapsed = (System.currentTimeMillis() - startTime);
        } else {
            elapsed = (stopTime - startTime);
        }
        return elapsed;
    }

    // elapsed time in seconds
    public double getElapsedTimeSecs() {
        double elapsed;
        if (running) {
            elapsed = ((System.currentTimeMillis() - startTime) / 1000);
        } else {
            elapsed = ((stopTime - startTime) / 1000);
        }
        return elapsed;
    }

    /**
     * sample usage
     * 
     * 
     * public static void main(String[] args) { StopWatch s = new StopWatch(); s.start(); //code you want to time goes here s.stop(); System.out.println("elapsed time in milliseconds: " +
     * s.getElapsedTime()); }
     */
}
