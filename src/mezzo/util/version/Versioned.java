package mezzo.util.version;

/**
 * <code>Versioned</code>
 * 
 * @author mezzodrinker
 * @since
 */
@FunctionalInterface
public interface Versioned {
    public Version getVersion();
}
