package mezzo.util.version;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.SerializeAs;

/**
 * 
 * @author mezzodrinker
 * @since 0.0.1
 */
@SerializeAs("Version")
public class Version implements Comparable<Version>, ConfigurationSerializable {
    private List<Integer>         version     = new ArrayList<Integer>();
    private String                prefix      = null;
    private String                suffix      = null;

    protected static final String KEY_PREFIX  = "prefix";
    protected static final String KEY_VERSION = "version number list";
    protected static final String KEY_SUFFIX  = "suffix";

    public static final Version   UNKNOWN     = new Version(null, Arrays.asList(-1), null);
    public static final Pattern   REGEX       = Pattern.compile("(.*?)(\\d+(?:\\.\\d+)*)(.*)");

    /**
     * Creates a new instance of <code>Version</code>
     * 
     * @param prefix
     *            the verson's prefix
     * @param version
     *            the version numbers
     * @param suffix
     *            the version's suffix
     */
    private Version(String prefix, List<Integer> version, String suffix) {
        this.prefix = prefix;
        this.version = Collections.unmodifiableList(version);
        this.suffix = suffix;
    }

    /**
     * Compares this object with the specified object for order. Returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(Version o) {
        int ret = 0;
        final List<Integer> other = o.version;
        final List<Integer> self = version;

        for (int i = 0; i < other.size(); i++) {
            try {
                final Integer curOther = other.get(i);
                final Integer curSelf = self.get(i);

                if (curOther > curSelf) {
                    ret = -1;
                    break;
                } else if (curOther < curSelf) {
                    ret = 1;
                    break;
                } else {
                    ret = 0;
                    continue;
                }
            } catch (final IndexOutOfBoundsException e) {
                break;
            }
        }

        return ret;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Version)) return false;

        return compareTo((Version) o) == 0;
    }

    /**
     * Returns the text prefix of this version.
     * 
     * <h2>Example</h2>
     * 
     * <code>
     * Version v = Version.valueOf(&quot;prefix1.0suffix&quot;);<br/>
     * System.out.println(v.getPrefix()); // prints &quot;prefix&quot;
     * </code>
     * 
     * @return the text prefix of this version
     * @see #getVersion()
     * @see #getSuffix()
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Returns this version's version number.
     * 
     * <h2>Example</h2>
     * 
     * <code>
     * Version v = Version.valueOf(&quot;prefix1.0suffix&quot;);<br/>
     * System.out.println(v.getVersion()); // prints &quot;1.0&quot;
     * </code>
     * 
     * @return this version's version number
     * @see #getPrefix()
     * @see #getSuffix()
     */
    public String getVersion() {
        final StringBuilder ret = new StringBuilder();

        for (int i = 0; i < version.size(); i++) {
            final Integer c = version.get(i);
            if (i > 0) {
                ret.append(".");
            }
            ret.append(c);
        }

        return ret.toString();
    }

    /**
     * Returns the text suffix of this version.
     * 
     * <h2>Example</h2>
     * 
     * <code>
     * Version v = Version.valueOf(&quot;prefix1.0suffix&quot;);<br/>
     * System.out.println(v.getSuffix()); // prints &quot;suffix&quot;
     * </code>
     * 
     * @return the text suffix of this version
     * @see #getPrefix()
     * @see #getVersion()
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Returns a string representation of this version.
     */
    @Override
    public String toString() {
        final StringBuilder ret = new StringBuilder();
        if (prefix != null) {
            ret.append(prefix);
        }
        ret.append(getVersion());
        if (suffix != null) {
            ret.append(suffix);
        }
        return ret.toString();
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(KEY_PREFIX, prefix);
        map.put(KEY_SUFFIX, suffix);
        map.put(KEY_VERSION, version);
        return map;
    }

    /**
     * Casts a value of a map to <code>{@link String}</code>.
     * 
     * @param key
     *            the key for the value
     * @param map
     *            the map containing the key
     * @return a <code>String</code> representation of the value
     * @throws IllegalArgumentException
     *             if {@link Map#get(Object) map.get(key)} returns <code>null</code>
     */
    private static String asString(String key, Map<String, Object> map) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException(key + " is not contained in " + map);
        return value.toString();
    }

    /**
     * Casts a value of a map to <code>{@link List}&lt;{@link Integer}&gt;</code>. The returned list will only contain values that are either {@link Number numbers} or {@link String strings} that can
     * be parsed into an {@link Integer} instance.
     * 
     * @param key
     *            the key for the value
     * @param map
     *            the map containing the key
     * @return a <code>List&lt;Integer&gt;</code> instance containing all (parseable) elements of the map's value
     * @throws IllegalArgumentException
     *             if {@link Map#get(Object) map.get(key)} returns <code>null</code> or the value is not a List
     */
    private static List<Integer> asIntegerList(String key, Map<String, Object> map) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException(key + " is not contained in " + map);
        if (!(value instanceof List)) throw new IllegalArgumentException(value + "(value of " + key + ") is not a list");
        List<Integer> list = new ArrayList<Integer>();
        for (Object o : (List<?>) value) {
            if (o instanceof Number) {
                list.add(((Number) o).intValue());
            }
            if (o instanceof String) {
                try {
                    list.add(Integer.valueOf(o.toString()));
                } catch (NumberFormatException e) {
                }
            }
        }
        return list;
    }

    /**
     * Creates a <code>Version</code> instance for the given string.
     * 
     * @param v
     *            The string representation of a version number. Has to match the regular expression <code>.*\d+(\.\d+)*.*</code>
     * @return the <code>Version</code> instance
     */
    public static Version valueOf(String v) {
        List<Integer> version = new ArrayList<>();
        String prefix = "";
        String suffix = "";
        Matcher matcher = REGEX.matcher(v);

        if (!matcher.matches()) throw new IllegalArgumentException("illegal version \'" + v + "\'");

        prefix = matcher.group(1);
        suffix = matcher.group(3);
        String versionString = matcher.group(2);
        String[] split = versionString.split("\\.");
        for (String s : split) {
            version.add(Integer.valueOf(s));
        }

        return new Version(prefix, version, suffix);
    }

    /**
     * Creates a new instance of <code>{@link Version}</code> represented by the values of a map.
     * 
     * @param args
     *            the map representing the <code>Version</code> instance
     * @return a new <code>Version</code> instance
     */
    public static Version deserialize(Map<String, Object> args) {
        String prefix = asString(KEY_PREFIX, args);
        String suffix = asString(KEY_SUFFIX, args);
        List<Integer> version = asIntegerList(KEY_VERSION, args);
        return new Version(prefix, version, suffix);
    }
}
