package mezzo.util.wrappers;

import java.awt.Color;
import java.awt.color.ColorSpace;
import java.awt.image.ColorModel;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.SerializeAs;

/**
 * <code>SerializableColor</code>
 * 
 * @author mezzodrinker
 * @since 0.0.3
 */
@SerializeAs("Color")
public class SerializableColor extends Color implements ConfigurationSerializable {
    private static final long     serialVersionUID = -2973979494850928817L;

    protected static final String KEY_RED          = "red";
    protected static final String KEY_GREEN        = "green";
    protected static final String KEY_BLUE         = "blue";
    protected static final String KEY_ALPHA        = "alpha";

    /**
     * Creates an opaque sRGB color with the specified red, green and blue values in the range (0 - 255).
     * The actual color used in rendering depends on finding the best match given the color space available for a given output device. Alpha is defaulted to 255.
     * 
     * @param red
     *            the red component
     * @param green
     *            the green component
     * @param blue
     *            the blue component
     * @throws IllegalArgumentException
     *             thrown if <code>red</code>, <code>green</code> or <code>blue</code> are outside of the range 0 to 255, inclusive
     * @see #getRed()
     * @see #getGreen()
     * @see #getBlue()
     * @see #getRGB()
     */
    public SerializableColor(int red, int green, int blue) {
        super(red, green, blue);
    }

    /**
     * Creates an sRGB color with the specified red, green, blue and alpha values in the range (0 - 255).
     * 
     * @param red
     *            the red component
     * @param green
     *            the green component
     * @param blue
     *            the blue component
     * @param alpha
     *            the alpha component
     * @throws IllegalArgumentException
     *             thrown if <code>red</code>, <code>green</code>, <code>blue</code> or <code>alpha</code> are outside of the range 0 to 255, inclusive
     * @see #getRed()
     * @see #getGreen()
     * @see #getBlue()
     * @see #getAlpha()
     * @see #getRGB()
     */
    public SerializableColor(int red, int green, int blue, int alpha) {
        super(red, green, blue, alpha);
    }

    /**
     * Creates an opaque sRGB color with the specified combined RGB value consisting of the red component in bits 16-23, the green component in bits 8-15, and the blue component in bits 0-7. The
     * actual color used in rendering depends on finding the best match given the color space available for a particular output device. Alpha is defaulted to 255.
     * 
     * @param rgb
     *            the combined RGB components
     * @see ColorModel#getRGBdefault()
     * @see #getRed()
     * @see #getGreen()
     * @see #getBlue()
     * @see #getRGB()
     */
    public SerializableColor(int rgb) {
        super(rgb);
    }

    /**
     * Creates an sRGB color with the specified combined RGBA value consisting of the alpha component in bits 24-31, the red component in bits 16-23, the green component in bits 8-15, and the blue
     * component in bits 0-7. If the <code>hasAlpha</code> argument is <code>false</code>, alpha is defaulted to 255.
     * 
     * @param rgba
     *            the combined RGBA components
     * @param hasAlpha
     *            determines if alpha bits will be considered or not
     * @see ColorModel#getRGBdefault()
     * @see #getRed()
     * @see #getGreen()
     * @see #getBlue()
     * @see #getAlpha()
     * @see #getRGB()
     */
    public SerializableColor(int rgba, boolean hasAlpha) {
        super(rgba, hasAlpha);
    }

    /**
     * Creates an opaque sRGB color with the specified red, green and blue values in the range (0.0 - 1.0).
     * The actual color used in rendering depends on finding the best match given the color space available for a given output device. Alpha is defaulted to 1.0.
     * 
     * @param red
     *            the red component
     * @param green
     *            the green component
     * @param blue
     *            the blue component
     * @throws IllegalArgumentException
     *             thrown if <code>red</code>, <code>green</code> or <code>blue</code> are outside of the range 0.0 to 1.0, inclusive
     * @see #getRed()
     * @see #getGreen()
     * @see #getBlue()
     * @see #getRGB()
     */
    public SerializableColor(float red, float green, float blue) {
        super(red, green, blue);
    }

    /**
     * Creates an sRGB color with the specified red, green, blue and alpha values in the range (0.0 - 1.0).
     * 
     * @param red
     *            the red component
     * @param green
     *            the green component
     * @param blue
     *            the blue component
     * @param alpha
     *            the alpha component
     * @throws IllegalArgumentException
     *             thrown if <code>red</code>, <code>green</code>, <code>blue</code> or <code>alpha</code> are outside of the range 0.0 to 1.0, inclusive
     * @see #getRed()
     * @see #getGreen()
     * @see #getBlue()
     * @see #getAlpha()
     * @see #getRGB()
     */
    public SerializableColor(float red, float green, float blue, float alpha) {
        super(red, green, blue, alpha);
    }

    /**
     * Creates a color in the specified {@link ColorSpace} with the color components specified in the <code>float</code> array and the specified alpha. The number of components is determined by
     * the type of the {@link ColorSpace}. For example, RGB requires 3 components, but CMYK requires 4 components.
     * 
     * @param colorSpace
     *            the <code>ColorSpace</code> to be used to interpret the components
     * @param components
     *            an arbitrary number of color components that is compatible with the <code>ColorSpace</code>
     * @param alpha
     *            alpha value
     * @throws IllegalArgumentException
     *             if any of the values in the <code>components</code> array or <code>alpha</code> is outside of the range 0.0 to 1.0
     * @see #getComponents
     * @see #getColorComponents
     */
    public SerializableColor(ColorSpace colorSpace, float[] components, float alpha) {
        super(colorSpace, components, alpha);
    }

    /**
     * Returns a string representation of this {@link SerializableColor}. This
     * method is intended to be used only for debugging purposes. The
     * content and format of the returned string might vary between
     * implementations. The returned string might be empty but cannot
     * be <code>null</code>.
     * 
     * @return a string representation of this <code>SerializableColor</code>.
     */
    @Override
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode()) + "[r=" + getRed() + ",g=" + getGreen() + ",b=" + getBlue() + ",a=" + getAlpha() + "]";
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(KEY_RED, getRed());
        map.put(KEY_GREEN, getGreen());
        map.put(KEY_BLUE, getBlue());
        map.put(KEY_ALPHA, getAlpha());
        return Collections.unmodifiableMap(map);
    }

    private static Number asNumber(String key, Map<String, Object> map) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException(key + " is not contained in map " + map);
        if (!(value instanceof Number)) throw new IllegalArgumentException(key + "(" + value + ") is not a number");
        return (Number) value;
    }

    public static SerializableColor deserialize(Map<String, Object> deserialize) {
        Number red = asNumber(KEY_RED, deserialize);
        Number green = asNumber(KEY_GREEN, deserialize);
        Number blue = asNumber(KEY_BLUE, deserialize);
        Number alpha = 255;
        if (deserialize.containsKey(KEY_ALPHA) && deserialize.get(KEY_ALPHA) != null) {
            alpha = asNumber(KEY_ALPHA, deserialize);
        }
        if (red instanceof Integer || green instanceof Integer || blue instanceof Integer || alpha instanceof Integer)
            return new SerializableColor(red.intValue(), green.intValue(), blue.intValue(), alpha.intValue());
        return new SerializableColor(red.floatValue(), green.floatValue(), blue.floatValue(), alpha.floatValue());
    }
}
