package mezzo.util;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * <code>BidirectionalMap</code>
 * 
 * @author mezzodrinker
 * @param <K>
 *            key type
 * @param <V>
 *            value type
 */
public class BidirectionalMap<K, V> extends AbstractMap<K, V> implements Iterable<Entry<K, V>> {
    protected HashMap<K, V> keyValueMap = new HashMap<>();
    protected HashMap<V, K> valueKeyMap = new HashMap<>();

    public BidirectionalMap() {}

    public BidirectionalMap(Map<K, V> map) {
        putAll(map);
    }

    public K getKey(Object value) {
        return valueKeyMap.get(value);
    }

    public K removeValue(Object value) {
        K oldKey = valueKeyMap.remove(value);
        keyValueMap.remove(oldKey);
        return oldKey;
    }

    @Override
    public boolean containsValue(Object value) {
        return valueKeyMap.containsKey(value);
    }

    @Override
    public boolean containsKey(Object key) {
        return keyValueMap.containsKey(key);
    }

    @Override
    public V get(Object key) {
        return keyValueMap.get(key);
    }

    @Override
    public V put(K key, V value) {
        V oldValue = keyValueMap.get(key);

        keyValueMap.remove(key);
        keyValueMap.put(key, value);

        valueKeyMap.remove(oldValue);
        valueKeyMap.put(value, key);

        return oldValue;
    }

    @Override
    public V remove(Object key) {
        V oldValue = keyValueMap.remove(key);
        valueKeyMap.remove(oldValue);
        return oldValue;
    }

    @Override
    public void clear() {
        keyValueMap.clear();
        valueKeyMap.clear();
    }

    @Override
    public Set<K> keySet() {
        return keyValueMap.keySet();
    }

    @Override
    public Set<V> values() {
        return valueKeyMap.keySet();
    }

    @Override
    public void forEach(Consumer<? super Entry<K, V>> consumer) {
        entrySet().forEach(consumer);
    }

    @Override
    public Spliterator<Entry<K, V>> spliterator() {
        return entrySet().spliterator();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return keyValueMap.entrySet();
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return entrySet().iterator();
    }
}
