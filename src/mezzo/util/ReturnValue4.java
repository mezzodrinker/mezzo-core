package mezzo.util;

/**
 * <code>ReturnValue4</code>
 * 
 * @author mezzodrinker
 * @param <T1>
 *            type of first value
 * @param <T2>
 *            type of second value
 * @param <T3>
 *            type of third value
 * @param <T4>
 *            type of fourth value
 */
public class ReturnValue4<T1, T2, T3, T4> {
    public final T1 value1;
    public final T2 value2;
    public final T3 value3;
    public final T4 value4;

    public ReturnValue4(T1 value1, T2 value2, T3 value3, T4 value4) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }
}
