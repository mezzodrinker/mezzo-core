package mezzo.util;

import java.util.LinkedHashMap;

/**
 * <code>LinkedBidirectionalMap</code>
 * 
 * @author mezzodrinker
 * @param <K>
 *            key type
 * @param <V>
 *            value type
 */
public class LinkedBidirectionalMap<K, V> extends BidirectionalMap<K, V> {
    public LinkedBidirectionalMap() {
        keyValueMap = new LinkedHashMap<>();
        valueKeyMap = new LinkedHashMap<>();
    }
}
