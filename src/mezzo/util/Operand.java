package mezzo.util;

/**
 * <code>Operand</code>
 * 
 * @param <T>
 *            type of values this operand accepts as second operand
 * @author mezzodrinker
 * @since 1.0.46
 */
public interface Operand<T> extends Comparable<T> {
    /**
     * @param t
     *            value to compare to
     * @return <tt>this &lt; t</tt>
     * @see #lessThan(Object)
     */
    default public boolean lt(T t) {
        return lessThan(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>(this &lt; t) or (this = t)</tt>
     * @see #lessThanOrEqualTo(Object)
     */
    default public boolean lte(T t) {
        return lessThanOrEqualTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>(this &lt; t) or (this = t)</tt>
     * @see #lessThan(Object)
     * @see #equalTo(Object)
     */
    default public boolean lessThanOrEqualTo(T t) {
        return lessThan(t) || equalTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return this &gt; t
     * @see #greaterThan(Object)
     */
    default public boolean gt(T t) {
        return greaterThan(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return this &gt; t
     * @see #greaterThanOrEqualTo(Object)
     */
    default public boolean gte(T t) {
        return greaterThanOrEqualTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>(this &gt; t) or (this = t)</tt>
     * @see #greaterThan(Object)
     * @see #equalTo(Object)
     */
    default public boolean greaterThanOrEqualTo(T t) {
        return greaterThan(t) || equalTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>&not;(this = t)</tt>
     * @see #notEqualTo(Object)
     */
    default public boolean neq(T t) {
        return notEqualTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>&not;(this = t)</tt>
     * @see #equalTo(Object)
     */
    default public boolean notEqualTo(T t) {
        return !equalTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>this = t</t>
     * @see #equalTo(Object)
     */
    default public boolean eq(T t) {
        return equalTo(t);
    }

    /**
     * @param t
     *            value to compare to
     * @return <tt>this &lt; t</tt>
     */
    public boolean lessThan(T t);

    /**
     * @param t
     *            value to compare to
     * @return <tt>this &gt; t</tt>
     */
    public boolean greaterThan(T t);

    /**
     * @param t
     *            value to compare to
     * @return <tt>this = t</tt>
     */
    public boolean equalTo(T t);

    /**
     * Compares this object with the specified object for order. Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * <p>
     * The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>. (This implies that <tt>x.compareTo(y)</tt> must throw an exception iff <tt>y.compareTo(x)</tt> throws an exception.)
     *
     * <p>
     * The implementor must also ensure that the relation is transitive: <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies <tt>x.compareTo(z)&gt;0</tt>.
     *
     * <p>
     * Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt> implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for all <tt>z</tt>.
     *
     * <p>
     * It is strongly recommended, but <i>not</i> strictly required that <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>. Generally speaking, any class that implements the <tt>Comparable</tt> interface
     * and violates this condition should clearly indicate this fact. The recommended language is "Note: this class has a natural ordering that is inconsistent with equals."
     *
     * <p>
     * In the foregoing description, the notation <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical <i>signum</i> function, which is defined to return one of <tt>-1</tt>, <tt>0</tt>
     * , or <tt>1</tt> according to whether the value of <i>expression</i> is negative, zero or positive.
     *
     * @param o
     *            the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     *         is less than, equal to, or greater than the specified object.
     *
     * @throws NullPointerException
     *             if the specified object is null
     * @throws ClassCastException
     *             if the specified object's type prevents it
     *             from being compared to this object.
     */
    @Override
    default public int compareTo(T o) {
        return lt(o) ? -1 : eq(o) ? 0 : 1;
    }
}
