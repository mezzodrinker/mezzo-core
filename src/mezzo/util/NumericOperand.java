package mezzo.util;

/**
 * <code>NumericOperand</code>
 * 
 * @param <T>
 *            type of values this operand accepts as second operand
 * 
 * @author mezzodrinker
 * @since
 */
public interface NumericOperand<T extends Number> extends Operand<T> {
    public Number add(T t);

    public Number sub(T t);

    public Number div(T t);

    public Number mul(T t);

    public Number exp(T t);

    public Number mod(T t);
}
