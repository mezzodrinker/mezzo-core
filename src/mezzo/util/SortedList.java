package mezzo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * <code>SortedList</code>
 * 
 * @author mezzodrinker
 * @param <T>
 *            type of elements this list will store
 */
public class SortedList<T extends Comparable<T>> extends ArrayList<T> {
    private static final long serialVersionUID = 998105306219025553L;

    public SortedList(int initialCapacity) {
        super(initialCapacity);
    }

    public SortedList() {
        super();
    }

    public SortedList(Collection<? extends T> c) {
        super(c);
    }

    @Override
    public boolean add(T element) {
        boolean ret = super.add(element);
        Collections.sort(this);
        return ret;
    }

    @Override
    public T set(int index, T element) {
        T ret = super.set(index, element);
        Collections.sort(this);
        return ret;
    }

    @Override
    public void add(int index, T element) {
        super.add(index, element);
        Collections.sort(this);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean ret = super.addAll(c);
        Collections.sort(this);
        return ret;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        boolean ret = super.addAll(index, c);
        Collections.sort(this);
        return ret;
    }
}
