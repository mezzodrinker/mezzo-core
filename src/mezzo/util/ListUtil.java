package mezzo.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author lirtistasya
 * @since 0.0.1
 */
public class ListUtil {
    // can't instantiate ListUtils
    private ListUtil() {}

    /**
     * Converts an array into an unmodifiable list.
     * 
     * @param array
     *            the array to convert
     * @return unmodifiable list representation of the input array
     * @see Arrays#asList(T...)
     * @see #unmodifiableList(List)
     */
    @SafeVarargs
    public static <T> List<T> unmodifiableList(T... array) {
        return unmodifiableList(Arrays.asList(array));
    }

    /**
     * Alias for <code>Collections.unmodifiableList(List<? extends T>)</code>.
     * 
     * @param list
     *            the list to convert
     * @return unmodifiable list representation of the input list
     * @see java.util.Collections#unmodifiableList(List)
     */
    public static <T> List<T> unmodifiableList(List<T> list) {
        return Collections.unmodifiableList(list);
    }

    public static <T> String listToHumanString(List<T> list) {
        if (list == null) return "[]";
        if (list.isEmpty()) return "[]";
        StringBuilder builder = new StringBuilder();
        int counter = 0;
        if (list.size() > 1) {
            for (T element : list) {
                builder.append(element);
                if (counter < list.size() - 1) {
                    builder.append(", ");
                } else {
                    builder.append(" and ");
                }
            }
        } else
            return builder.append(list.get(0)).toString();
        return builder.toString();
    }

    public static <T extends CharSequence> int maxLength(List<T> list) {
        int length = 0;
        for (CharSequence s : list) {
            length = Math.max(length, s.length());
        }
        return length;
    }
}
