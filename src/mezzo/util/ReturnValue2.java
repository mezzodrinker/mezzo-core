package mezzo.util;

/**
 * <code>ReturnValue2</code>
 * 
 * @author mezzodrinker
 * @param <T1>
 *            type of first value
 * @param <T2>
 *            type of second value
 */
public class ReturnValue2<T1, T2> {
    public final T1 value1;
    public final T2 value2;

    public ReturnValue2(T1 value1, T2 value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public String toString() {
        return getClass().getName() + "{" + value1 + ":" + (value1 != null ? value1.getClass().getSimpleName() : "null") + "," + value2 + ":"
                + (value2 != null ? value2.getClass().getSimpleName() : "null") + "}";
    }
}
