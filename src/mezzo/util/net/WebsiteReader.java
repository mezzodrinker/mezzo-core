package mezzo.util.net;

import static mezzo.util.ArrayUtil.array;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.CanolaExtractor;

/**
 * <code>WebsiteReader</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class WebsiteReader {
    private int                             httpResponseCode = 0;
    private String                          httpResponse     = null;
    private String                          title            = null;
    private String                          description      = null;
    private String                          content          = null;
    private String                          error            = null;
    private URL                             url              = null;
    private final String[]                  knownShorteners  = array("adf.ly", "goo.gl", "tinyurl.com", "t.co", "bit.ly", "ow.ly", "is.gd");
    private final Map<String, List<String>> headerFields;

    public final URLConnection              connection;
    public final InputStream                inputStream;

    public WebsiteReader(String url) throws MalformedURLException, IOException {
        this(new URL(url));
    }

    public WebsiteReader(URL url) throws IOException {
        this.url = url;
        connection = url.openConnection();
        inputStream = connection.getInputStream();
        headerFields = connection.getHeaderFields();

        unshorten();
        read();
        analyze();
        replace();
    }

    private void read() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            if (builder.length() > 0) {
                builder.append("\n");
            }
            builder.append(line);
        }

        content = builder.toString();
        httpResponse = headerFields.get(null).get(0);

        String[] s = httpResponse.split(" ", 3);
        Pattern pattern = Pattern.compile("\\d+");
        for (String element : s) {
            Matcher matcher = pattern.matcher(element);
            if (matcher.matches()) {
                httpResponseCode = Integer.valueOf(matcher.group(0));
                break;
            }
        }
    }

    private void analyze() throws IOException {
        Pattern _title = Pattern.compile("<.*?title.*?>(.+)<.*?/title.*?>", Pattern.CASE_INSENSITIVE);
        Matcher m;

        m = _title.matcher(content);
        if (m.find()) {
            title = m.group(1);
        } else {
            title = String.valueOf(url);
        }

        try {
            description = new CanolaExtractor().getText(url);
            int limit = 100;
            if (url.getHost().contains("wikipedia.org")) {
                description = description.replaceAll("(?i)jump to:.*?\n", "");
                description = description.replaceAll("\\[\\d+\\]", "");
                description = description.substring(0, description.indexOf("\n"));
                description = description.replaceAll("\\s\\s+", " ");
            } else if (description.length() > limit) {
                description = description.substring(0, limit) + "...";
            }
        } catch (BoilerpipeProcessingException e) {
            throw new IOException(e);
        }
    }

    private void unshorten() throws IOException {
        String host = url.getHost();
        String shortener = null;
        for (String knownShortener : knownShorteners) {
            if (host.contains(knownShortener)) {
                shortener = knownShortener;
                break;
            }
        }
        if (shortener == null) return;

        String protocol = url.getProtocol();
        if (!protocol.equalsIgnoreCase("http") && !protocol.equalsIgnoreCase("https")) return;
        protocol = protocol.toLowerCase();

        URL lookupURL = new URL("http://api.longurl.org/v2/expand?url=" + url.toString() + "&response-code=1&meta-description=1&format=xml");
        URLConnection connection = lookupURL.openConnection();
        InputStream inputStream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            if (builder.length() > 0) {
                builder.append("\n");
            }
            builder.append(line);
        }

        String content = builder.toString();
        Matcher longURL = Pattern.compile("<long-url>(.*)</long-url>").matcher(content);
        Matcher responseCode = Pattern.compile("<response-code>(.*)</response-code>").matcher(content);
        Matcher metaDescription = Pattern.compile("<meta-description>(.*)</meta-description>").matcher(content);
        Matcher error = Pattern.compile("<error>(.*)</error>").matcher(content);
        Pattern cdata = Pattern.compile("<!\\[CDATA\\[(.*)\\]\\]>");

        if (longURL.find()) {
            String response = longURL.group(1);
            Matcher m = cdata.matcher(response);
            if (m.find()) {
                url = new URL(m.group(1));
            }
        }

        if (responseCode.find()) {
            String response = responseCode.group(1);
            Matcher m = cdata.matcher(response);
            if (m.find()) {
                httpResponseCode = Integer.valueOf(m.group(1));
            }
        }

        if (metaDescription.find()) {
            String response = metaDescription.group(1);
            Matcher m = cdata.matcher(response);
            if (m.find()) {
                description = m.group(1);
            }
        }

        if (error.find()) {
            String response = error.group(1);
            Matcher m = cdata.matcher(response);
            if (m.find()) {
                this.error = m.group(1);
            }
        }
    }

    private void replace() {
        Map<String, String> replacements = new HashMap<String, String>();
        replacements.put("&raquo;", "\u00bb");
        replacements.put("&auml;", "\u00e4");
        replacements.put("&ouml;", "\u00f6");
        replacements.put("&uuml;", "\u00fc");
        replacements.put("&mildot;", "\u00b7");
        replacements.put("&bull;", "\u2022");
        replacements.put("&nbsp;", " ");
        replacements.put("&quot;", "\"");

        for (Entry<String, String> entry : replacements.entrySet()) {
            String target = entry.getKey();
            String replacement = entry.getValue();
            title = title.replace(target, replacement);
            description = description.replace(target, replacement);
        }
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public URL getURL() {
        return url;
    }

    public int getHTTPResponseCode() {
        return httpResponseCode;
    }

    public String getHTTPResponse() {
        return httpResponse;
    }

    public String getError() {
        return error;
    }
}
