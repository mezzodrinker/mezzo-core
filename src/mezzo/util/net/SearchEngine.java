package mezzo.util.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * <code>SearchEngine</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class SearchEngine {
    private static final String ABSTRACT_URL = "AbstractURL";
    private static final String RESULTS      = "Results";

    private SearchEngine() {}

    private static String getContent(URL url) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder builder = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            if (builder.length() > 0) {
                builder.append("\n");
            }
            builder.append(line);
        }

        return builder.toString();
    }

    public static URL[] instantAnswer(String term) throws MalformedURLException, IOException {
        URL ddg = new URL("https://api.duckduckgo.com/?q=" + term.replace(" ", "%20") + "&format=json&no_redirect=1&t=mezzo-core");
        JSONObject response = new JSONObject(getContent(ddg));
        JSONArray results = response.getJSONArray(RESULTS);
        String url = response.getString(ABSTRACT_URL);
        URL[] urls = new URL[results.length() + 1];
        int c = 0;

        if (url == null || url.isEmpty()) return new URL[0];

        urls[c++] = new URL(url);
        for (int i = 0; i < results.length(); i++) {
            Object element = results.get(i);
            if (element == null) {
                continue;
            }
            urls[c++] = new URL(String.valueOf(element));
        }

        return urls;
    }

    public static void main(String[] args) {
        try {
            instantAnswer("!yandex MobTalker2");
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
