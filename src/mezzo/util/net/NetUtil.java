package mezzo.util.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author lirtistasya
 * @since 0.0.1
 */
public class NetUtil {
    public static final String NEWLINE    = "%0D%0A";
    public static final String WHITESPACE = "%20";

    // can't instantiate WebUtils
    private NetUtil() {}

    public static final boolean isURL(String s) {
        try {
            new URL(s);
            return true;
        } catch (MalformedURLException e) {
        }
        return false;
    }

    public static final String replaceHTMLCharacters(String s) {
        s = s.replace("&ouml;", "\u00f6");
        s = s.replace("&auml;", "\u00e4");
        s = s.replace("&uuml;", "\u00fc");
        s = s.replace("&bull;", "\u2022");
        return s;
    }

    /**
     * @param url
     *            URL to a website
     * @return title of the given website
     * @throws MalformedURLException
     * @throws UnknownHostException
     * @throws IOException
     * @since mezzo-util 1.1.27
     */
    public static final String getTitle(String url) throws MalformedURLException, UnknownHostException, IOException {
        URL address = new URL(url);
        BufferedReader in = new BufferedReader(new InputStreamReader(address.openStream()));
        Pattern headStart = Pattern.compile("<\\s*?head.*?>", Pattern.CASE_INSENSITIVE);
        Pattern headEnd = Pattern.compile("<\\s*?/head.*?>", Pattern.CASE_INSENSITIVE);
        Pattern titleStart = Pattern.compile("<\\s*?title.*?>", Pattern.CASE_INSENSITIVE);
        Pattern titleEnd = Pattern.compile("<\\s*?/title.*?>", Pattern.CASE_INSENSITIVE);
        Pattern title = Pattern.compile(".*<\\s*?title.*?>(.*?)<\\s*?/title.*?>.*", Pattern.CASE_INSENSITIVE);
        String line;
        boolean inHead = false;
        boolean inTitle = false;
        final long start = System.currentTimeMillis();
        while ((line = in.readLine()) != null && System.currentTimeMillis() - start <= 5000) {
            Matcher _hs = headStart.matcher(line);
            Matcher _he = headEnd.matcher(line);
            Matcher _ts = titleStart.matcher(line);
            Matcher _te = titleEnd.matcher(line);

            boolean _hsFound = _hs.find();
            boolean _heFound = _he.find();
            boolean _tsFound = _ts.find();
            boolean _teFound = _te.find();

            if (_tsFound && _teFound) {
                Matcher _t = title.matcher(line);
                if (_t.matches()) return replaceHTMLCharacters(_t.group(1));
            }
            if (_hsFound && _heFound) {
                break;
            }

            if (_hs.find()) {
                inHead = true;
            }
            if (_ts.find()) {
                inTitle = true;
            }
            if (inHead && inTitle) return replaceHTMLCharacters(line.replaceFirst("\\s*", ""));
            if (_te.find()) {
                break;
            }
            if (_he.find()) {
                break;
            }
        }
        return "unknown title";
    }

    /**
     * Reads a website from a given URL and returns it as a list of strings. Each element of that list represents a single line.<br/>
     * <br/>
     * This method reads ALL the text in the specified page (including HTML tags etc). If you just want to get the text, please use {@link #getSiteRaw(String)}.
     * 
     * @param url
     *            the URL
     * @return the site in form of a string list
     * @throws MalformedURLException
     *             {@link java.net.URL#URL(String) URL(String)}
     * @throws UnknownHostException
     *             if the specified host could not be found
     * @throws IOException
     *             {@link java.net.URL#openStream() openStream()}
     */
    public static final List<String> getSite(String url) throws MalformedURLException, UnknownHostException, IOException {
        List<String> ret = new ArrayList<String>();

        URL address = new URL(url);
        String line = null;

        try (BufferedReader in = new BufferedReader(new InputStreamReader(address.openStream()));) {
            while ((line = in.readLine()) != null) {
                ret.add(line);
            }
        } catch (MalformedURLException e) {
            throw e;
        } catch (UnknownHostException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret;
    }

    /**
     * Reads a website from a given URL and returns it as a string list. Each element of that list represents a single line.<br/>
     * <br/>
     * This method does only read the raw text of the page (everything except HTML tags etc). If you want to get the whole page (including HTML tags etc), please refer to {@link #getSite(String)}
     * 
     * @param url
     *            the URL
     * @return the page in form of a list of strings
     * @throws MalformedURLException
     *             {@link java.net.URL#URL(String) URL(String)}
     * @throws UnknownHostException
     *             if the specified host could not be found
     * @throws IOException
     *             {@link java.net.URL#openStream() openStream()}
     */
    public static final List<String> getSiteRaw(String url) throws MalformedURLException, UnknownHostException, IOException {
        List<String> ret = new ArrayList<String>();
        URL address = new URL(url);
        String line = null;
        boolean tagOpen = false;
        Pattern bodyStart = Pattern.compile("<\\s*?body.*?>", Pattern.CASE_INSENSITIVE);
        Pattern bodyEnd = Pattern.compile("<\\s*?/body.*?>", Pattern.CASE_INSENSITIVE);

        try (BufferedReader in = new BufferedReader(new InputStreamReader(address.openStream()));) {
            boolean wasInBody = false;
            boolean inBody = false;
            while ((line = in.readLine()) != null) {
                if (bodyStart.matcher(line).find()) {
                    inBody = true;
                    wasInBody = false;
                }
                if (bodyEnd.matcher(line).find()) {
                    inBody = false;
                }
                if (!inBody) {
                    if (wasInBody) {
                        break;
                    }
                    continue;
                }
                if (line.contains("<") && !line.contains(">") && !tagOpen) {
                    tagOpen = true;
                    continue;
                }

                if (tagOpen && !line.contains(">")) {
                    continue;
                }

                if (tagOpen && line.contains(">")) {
                    tagOpen = false;
                    continue;
                }

                line = line.replaceAll("\\t", "");
                line = line.replaceAll("<.*>", "");

                if (line.matches("\\s*")) {
                    continue;
                }

                String toAdd = "";
                int pos = 0;

                for (; pos < line.toCharArray().length; pos++) {
                    char c = line.toCharArray()[pos];

                    if (c != ' ') {
                        break;
                    }
                }

                toAdd += line.substring(pos);

                if (!toAdd.isEmpty()) {
                    ret.add(toAdd);
                }
            }
        } catch (MalformedURLException e) {
            throw e;
        } catch (UnknownHostException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret;
    }
}
