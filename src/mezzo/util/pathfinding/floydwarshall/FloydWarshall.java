package mezzo.util.pathfinding.floydwarshall;

import static mezzo.util.ArrayUtil.concatenate;

import java.util.List;

import mezzo.util.pathfinding.DistanceMap;
import mezzo.util.pathfinding.GraphNode;
import mezzo.util.pathfinding.WeightedEdge;
import mezzo.util.pathfinding.WeightedGraph;

/**
 * <code>FloydWarshall</code>
 * 
 * @author mezzodrinker
 */
public class FloydWarshall {
    private FloydWarshall() {}

    public static DistanceMap calculate(WeightedGraph graph) {
        DistanceMap distances = new DistanceMap();
        List<GraphNode> nodes = graph.getNodes();
        List<? extends WeightedEdge> edges = graph.getEdges();

        for (GraphNode node : nodes) {
            distances.setDistance(node, node, 0d);
        }

        for (WeightedEdge edge : edges) {
            distances.setDistance(edge.getFrom(), edge.getTo(), edge.getDistance());
            distances.setPath(edge.getFrom(), edge.getTo(), new GraphNode[] {edge.getTo()});
        }

        for (GraphNode k : nodes) {
            for (GraphNode i : nodes) {
                for (GraphNode j : nodes) {
                    double oldDistance = distances.getDistance(i, j);
                    double newDistance = distances.getDistance(i, k) + distances.getDistance(k, j);
                    if (oldDistance > newDistance) {
                        distances.setPath(i, j, concatenate(distances.getPath(i, k), distances.getPath(k, j)));
                        distances.setDistance(i, j, newDistance);
                    }
                }
            }
        }

        return distances;
    }
}
