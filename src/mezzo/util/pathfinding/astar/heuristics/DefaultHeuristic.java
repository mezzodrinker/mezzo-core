package mezzo.util.pathfinding.astar.heuristics;

import mezzo.util.pathfinding.astar.LocationNode;

/**
 * <code>None</code>
 * 
 * @author mezzodrinker
 */
public final class DefaultHeuristic implements ILocationHeuristic {
    @Override
    public double estimateDistance(LocationNode from, LocationNode to) {
        return 1;
    }
}
