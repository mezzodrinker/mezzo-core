package mezzo.util.pathfinding.astar.heuristics;

import mezzo.util.pathfinding.astar.LocationNode;

/**
 * <code>ManhattanHeuristic</code>
 * 
 * @author mezzodrinker
 */
public class ManhattanHeuristic implements ILocationHeuristic {
    @Override
    public double estimateDistance(LocationNode from, LocationNode to) {
        return Math.abs(from.x - to.x) + Math.abs(from.y - to.y);
    }
}
