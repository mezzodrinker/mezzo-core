package mezzo.util.pathfinding.astar.heuristics;

import mezzo.util.pathfinding.astar.LocationNode;

/**
 * <code>ILocationHeuristic</code>
 * 
 * @author mezzodrinker
 */
@FunctionalInterface
public interface ILocationHeuristic {
    public double estimateDistance(LocationNode from, LocationNode to);
}
