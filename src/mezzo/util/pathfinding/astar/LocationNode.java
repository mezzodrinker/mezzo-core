package mezzo.util.pathfinding.astar;

import java.util.HashSet;
import java.util.Set;

import mezzo.util.pathfinding.Node;

/**
 * <code>LocationNode</code>
 * 
 * @author mezzodrinker
 */
public class LocationNode extends Node {
    protected Set<LocationNode> adjacentNodes = new HashSet<LocationNode>();

    public final double         x;
    public final double         y;

    public LocationNode(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public LocationNode(double x, double y, String name) {
        super(name);
        this.x = x;
        this.y = y;
    }

    @Override
    public Set<? extends LocationNode> getAdjacentNodes() {
        return adjacentNodes;
    }

    @Override
    public double getDistance(Node node) {
        if (!(node instanceof LocationNode)) throw new UnsupportedOperationException();
        if (!getAdjacentNodes().contains(node)) throw new IllegalArgumentException(node + " is not adjacent to " + this);
        LocationNode n = (LocationNode) node;
        return Math.sqrt(Math.pow(x - n.x, 2) + Math.pow(y - n.y, 2));
    }

    @Override
    public void setDistance(Node node, double distance) {
        if (!adjacentNodes.contains(node)) {
            if (!(node instanceof LocationNode)) throw new UnsupportedOperationException();
            LocationNode n = (LocationNode) node;
            adjacentNodes.add(n);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LocationNode)) return false;
        LocationNode n = (LocationNode) o;
        return n.x == x && n.y == y;
    }

    @Override
    public String toString() {
        return (name == null ? "LocationNode@" + Integer.toHexString(hashCode()) : name) + "(" + x + "|" + y + ")";
    }
}
