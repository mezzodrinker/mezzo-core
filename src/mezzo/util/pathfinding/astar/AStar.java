package mezzo.util.pathfinding.astar;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;

import mezzo.util.pathfinding.astar.heuristics.DefaultHeuristic;
import mezzo.util.pathfinding.astar.heuristics.ILocationHeuristic;

/**
 * <code>AStar</code>
 * 
 * @author mezzodrinker
 */
public class AStar {
    private AStar() {}

    private static Set<LocationNode>               closed;
    private static PriorityQueue<LocationNode>     open;
    private static Map<LocationNode, Double>       distanceFromStart;
    private static Map<LocationNode, Double>       estimatedTotalDistance;
    private static Map<LocationNode, LocationNode> paths;

    private static LocationNode[] reconstructPath(LocationNode current) {
        List<LocationNode> path = new ArrayList<>();
        path.add(0, current);
        while (paths.containsKey(current)) {
            current = paths.get(current);
            path.add(0, current);
        }
        return path.toArray(new LocationNode[0]);
    }

    private static void initialize() {
        closed = new HashSet<>();
        open = new PriorityQueue<>(new Comparator<LocationNode>() {
            @Override
            public int compare(LocationNode a, LocationNode b) {
                Double dA = estimatedTotalDistance.get(a);
                Double dB = estimatedTotalDistance.get(b);
                if (dA == null && dB != null) return 1;
                if (dA != null && dB == null) return -1;
                if (dA == null && dB == null) return 0;
                double ret = dB - dA;
                return (int) (ret == 0 ? 0 : ret > 0 ? Math.floor(ret) : Math.ceil(ret));
            }
        });
        distanceFromStart = new HashMap<>(1000, 0.9f);
        estimatedTotalDistance = new HashMap<>(1000, 0.9f);
        paths = new HashMap<>();
    }

    public static LocationNode[] calculate(LocationNode start, LocationNode target) {
        return calculate(start, target, new DefaultHeuristic());
    }

    public static LocationNode[] calculate(LocationNode start, LocationNode target, ILocationHeuristic heuristic) {
        initialize();

        distanceFromStart.put(start, 0d);
        estimatedTotalDistance.put(start, distanceFromStart.get(start) + heuristic.estimateDistance(start, target));
        open.add(start);

        while (!open.isEmpty()) {
            LocationNode current = null;

            double minTotalDistance = Double.POSITIVE_INFINITY;
            for (Entry<LocationNode, Double> entry : estimatedTotalDistance.entrySet()) {
                if (!closed.contains(entry.getKey()) && entry.getValue() < minTotalDistance) {
                    minTotalDistance = entry.getValue();
                    current = entry.getKey();
                }
            }

            if (current.equals(target)) return reconstructPath(target);

            open.remove(current);

            closed.add(current);

            for (LocationNode neighbor : current.getAdjacentNodes()) {
                if (neighbor == null) {
                    System.err.println("a neighbor of " + current + " is null!");
                }
                if (closed.contains(neighbor)) {
                    continue;
                }
                double neighborDistanceFromStart = distanceFromStart.get(current) + current.getDistance(neighbor);

                if (!open.contains(neighbor) || neighborDistanceFromStart < distanceFromStart.get(neighbor)) {
                    paths.put(neighbor, current);
                    distanceFromStart.put(neighbor, neighborDistanceFromStart);
                    estimatedTotalDistance.put(neighbor, distanceFromStart.get(neighbor) + heuristic.estimateDistance(neighbor, target));

                    if (!open.contains(neighbor)) {
                        open.add(neighbor);
                    }
                }
            }
        }
        throw new RuntimeException("no path between " + start + " and " + target);
    }
}
