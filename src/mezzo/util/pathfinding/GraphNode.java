package mezzo.util.pathfinding;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <code>GraphNode</code>
 * 
 * @author mezzodrinker
 */
public class GraphNode {
    private boolean                      visited = false;
    private Map<GraphNode, WeightedEdge> edges   = new HashMap<GraphNode, WeightedEdge>();

    public final String                  name;

    public GraphNode() {
        this(null);
    }

    public GraphNode(String name) {
        this.name = name;
    }

    public Set<GraphNode> getAdjacentNodes() {
        return edges.keySet();
    }

    public Collection<WeightedEdge> getEdges() {
        return edges.values();
    }

    public void addConnection(GraphNode node) {
        if (edges.containsKey(node)) return;
        edges.put(node, new Edge(this, node));
    }

    public void setDistance(GraphNode node, double distance) {
        if (edges.containsKey(node)) {
            edges.get(node).setDistance(distance);
            return;
        }
        edges.put(node, new WeightedEdge(this, node, distance));
    }

    public double getDistance(GraphNode node) {
        WeightedEdge edge = edges.get(node);
        if (edge == null) return Double.POSITIVE_INFINITY;
        return edge.getDistance();
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isVisited() {
        return visited;
    }

    @Override
    public String toString() {
        return name == null ? "GraphNode@" + Integer.toHexString(hashCode()) : name;
    }
}
