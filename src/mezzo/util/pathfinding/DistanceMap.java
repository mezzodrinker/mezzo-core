package mezzo.util.pathfinding;

import static mezzo.util.StringUtil.copyOf;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import mezzo.util.HashTable;
import mezzo.util.HashTable.Key2;

/**
 * <code>DistanceMap</code>
 * 
 * @author mezzodrinker
 */
public class DistanceMap {
    private HashTable<GraphNode, GraphNode, Double>      distances = new HashTable<>();
    private HashTable<GraphNode, GraphNode, GraphNode[]> paths     = new HashTable<>();

    public boolean contains(GraphNode from, GraphNode to) {
        return distances.contains(from, to);
    }

    public void setDistance(GraphNode from, GraphNode to, double distance) {
        distances.set(from, to, distance);
    }

    public void setPath(GraphNode from, GraphNode to, GraphNode[] path) {
        paths.set(from, to, path);
    }

    public double getDistance(GraphNode from, GraphNode to) {
        Double ret = distances.get(from, to);
        return ret == null ? Double.POSITIVE_INFINITY : ret.doubleValue();
    }

    public GraphNode[] getPath(GraphNode from, GraphNode to) {
        GraphNode[] ret = paths.get(from, to);
        return ret == null ? new GraphNode[] {from} : ret;
    }

    public void printTo(PrintStream out) {
        List<GraphNode> from = new ArrayList<GraphNode>();
        List<GraphNode> to = new ArrayList<GraphNode>();

        for (Entry<Key2<GraphNode, GraphNode>, Double> entry : distances.entrySet()) {
            GraphNode f = entry.getKey().part1;
            GraphNode t = entry.getKey().part2;
            if (!from.contains(f)) {
                from.add(f);
            }
            if (!to.contains(t)) {
                to.add(t);
            }
        }

        int length = 10;
        out.print(copyOf("*", length));
        for (GraphNode n : to) {
            out.print(" ");
            out.print(copyOf(n, length));
        }
        out.println();
        for (GraphNode f : from) {
            out.print(copyOf(f, length));
            for (GraphNode t : to) {
                out.print(" ");
                out.print(copyOf(getDistance(f, t), length));
            }
            out.println();
        }
    }
}
