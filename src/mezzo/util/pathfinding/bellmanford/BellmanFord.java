package mezzo.util.pathfinding.bellmanford;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mezzo.util.pathfinding.GraphNode;
import mezzo.util.pathfinding.WeightedEdge;
import mezzo.util.pathfinding.WeightedGraph;

/**
 * <code>BellmanFord</code>
 * 
 * @author mezzodrinker
 */
public class BellmanFord {
    private WeightedGraph graph = null;

    public BellmanFord(WeightedGraph graph) {
        this.graph = graph;
    }

    public Paths calculate(GraphNode start) {
        Map<GraphNode, Double> weight = new HashMap<GraphNode, Double>();
        Map<GraphNode, GraphNode> predecessor = new HashMap<GraphNode, GraphNode>();

        for (GraphNode node : graph.getNodes()) {
            if (node.equals(start)) {
                weight.put(node, 0d);
            } else {
                weight.put(node, Double.POSITIVE_INFINITY);
            }
            predecessor.put(node, null);
        }

        for (WeightedEdge edge : graph.getEdges()) {
            if (weight.get(edge.getFrom()) == Double.POSITIVE_INFINITY) {
                continue;
            }
            double newDistance = weight.get(edge.getFrom()) + edge.getDistance();
            double oldDistance = weight.get(edge.getTo());
            if (newDistance < oldDistance) {
                weight.put(edge.getTo(), newDistance);
                predecessor.put(edge.getTo(), edge.getFrom());
            }
        }

        for (WeightedEdge edge : graph.getEdges()) {
            double newDistance = weight.get(edge.getFrom()) + edge.getDistance();
            double oldDistance = weight.get(edge.getTo());
            if (newDistance < oldDistance) throw new NegativeWeightCycleException();
        }

        return new Paths(start, predecessor, weight);
    }

    public static final class Paths {
        private final GraphNode                 start;
        private final Map<GraphNode, Double>    distances;
        private final Map<GraphNode, GraphNode> predecessors;

        protected Paths(GraphNode start, Map<GraphNode, GraphNode> predecessors, Map<GraphNode, Double> distances) {
            this.start = start;
            this.predecessors = new HashMap<>(predecessors);
            this.distances = new HashMap<>(distances);
        }

        public GraphNode getStart() {
            return start;
        }

        public GraphNode[] getPath(GraphNode target) {
            List<GraphNode> path = new ArrayList<>();
            GraphNode current = target;
            GraphNode next;
            path.add(0, current);
            while ((next = predecessors.get(current)) != null && current != null) {
                current = next;
                path.add(0, current);
            }
            return path.toArray(new GraphNode[0]);
        }

        public double getDistance(GraphNode target) {
            if (!distances.containsKey(target)) return Double.POSITIVE_INFINITY;
            return distances.get(target);
        }
    }

    public static class NegativeWeightCycleException extends RuntimeException {
        private static final long serialVersionUID = -8050581506501984276L;
    }
}
