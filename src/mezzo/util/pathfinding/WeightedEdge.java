package mezzo.util.pathfinding;

/**
 * <code>WeightedEdge</code>
 * 
 * @author mezzodrinker
 */
public class WeightedEdge {
    protected final GraphNode from;
    protected final GraphNode to;
    protected double          distance;

    protected WeightedEdge(GraphNode from, GraphNode to) {
        this(from, to, 1d);
    }

    public WeightedEdge(GraphNode from, GraphNode to, double distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public GraphNode getFrom() {
        return from;
    }

    public GraphNode getTo() {
        return to;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
