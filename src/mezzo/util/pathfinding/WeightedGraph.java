package mezzo.util.pathfinding;

import java.util.ArrayList;
import java.util.List;

/**
 * <code>WeightedGraph</code>
 * 
 * @author mezzodrinker
 */
public class WeightedGraph {
    protected List<GraphNode> graph = new ArrayList<>();

    public List<GraphNode> getNodes() {
        return graph;
    }

    public void add(GraphNode node) {
        if (!graph.contains(node)) {
            graph.add(node);
        }
    }

    public void addEdge(GraphNode from, GraphNode to) {
        addEdge(from, to, 1d);
    }

    public void addEdge(GraphNode from, GraphNode to, double distance) {
        add(from);
        add(to);
        from.setDistance(to, distance);
    }

    public List<? extends WeightedEdge> getEdges() {
        List<WeightedEdge> l = new ArrayList<>();
        for (GraphNode node : graph) {
            l.addAll(node.getEdges());
        }
        return l;
    }
}