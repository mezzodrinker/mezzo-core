package mezzo.util.pathfinding;

import java.util.ArrayList;
import java.util.List;

/**
 * <code>Graph</code>
 * 
 * @author mezzodrinker
 */
public class Graph extends WeightedGraph {
    @Override
    public void addEdge(GraphNode from, GraphNode to) {
        add(from);
        add(to);
        from.addConnection(to);
    }

    @Override
    public void addEdge(GraphNode from, GraphNode to, double distance) {
        addEdge(from, to);
    }

    @Override
    public List<? extends Edge> getEdges() {
        List<Edge> l = new ArrayList<Edge>();
        List<? extends WeightedEdge> superList = super.getEdges();
        for (WeightedEdge edge : superList) {
            if (edge instanceof Edge) {
                l.add((Edge) edge);
            }
        }
        return l;
    }
}
