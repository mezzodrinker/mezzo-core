package mezzo.util.pathfinding.dijkstra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mezzo.util.PriorityQueue;
import mezzo.util.pathfinding.Node;

/**
 * <code>Dijkstra</code>
 * 
 * @author mezzodrinker
 */
public class Dijkstra {
    private Dijkstra() {}

    private static Node[] reconstructPath(Map<Node, Node> paths, Node current) {
        List<Node> path = new ArrayList<Node>();
        path.add(0, current);
        while (paths.containsKey(current)) {
            current = paths.get(current);
            path.add(0, current);
        }
        return path.toArray(new Node[0]);
    }

    public static Node[] calculate(Node start, Node target) {
        PriorityQueue<Node> queue = new PriorityQueue<Node>();
        Map<Node, Node> nodes = new HashMap<Node, Node>();
        Map<Node, Double> distances = new HashMap<Node, Double>();

        queue.push(start, 0d);
        distances.put(start, 0d);

        while (!queue.isEmpty()) {
            Node node = queue.pop();
            node.setVisited(true);
            for (Node n : node.getAdjacentNodes()) {
                if (!n.isVisited()) {
                    double alt = distances.get(node) + node.getDistance(n);
                    Double distance = distances.get(n);
                    if (distance == null || alt < distance) {
                        distances.put(n, alt);
                        nodes.put(n, node);
                        queue.set(n, alt);
                    }
                }
            }
        }
        // distance = distances.get(target);
        return reconstructPath(nodes, target);
    }
}
