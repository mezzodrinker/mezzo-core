package mezzo.util.pathfinding;

/**
 * <code>Edge</code>
 * 
 * @author mezzodrinker
 */
public class Edge extends WeightedEdge {
    public Edge(GraphNode from, GraphNode to) {
        super(from, to);
    }

    @Override
    public void setDistance(double distance) {}
}
