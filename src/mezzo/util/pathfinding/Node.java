package mezzo.util.pathfinding;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <code>Node</code>
 * 
 * @author mezzodrinker
 */
public class Node {
    private boolean           visited   = false;
    private Map<Node, Double> distances = new HashMap<Node, Double>();

    public final String       name;

    public Node() {
        this(null);
    }

    public Node(String name) {
        this.name = name;
    }

    public Set<? extends Node> getAdjacentNodes() {
        return Collections.unmodifiableSet(distances.keySet());
    }

    public double getDistance(Node node) {
        return distances.get(node);
    }

    public void setDistance(Node node, double distance) {
        distances.put(node, distance);
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isVisited() {
        return visited;
    }

    @Override
    public String toString() {
        return (name == null ? "Node@" + Integer.toHexString(hashCode()) : name);
    }
}
