package mezzo.util.swing;

import java.awt.Component;
import java.awt.Container;
import java.util.HashSet;
import java.util.Set;

/**
 * <code>SwingUtil</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class SwingUtil {
    private SwingUtil() {}

    @SuppressWarnings("unchecked")
    public static <T extends Component> T getComponent(Container container, Class<T> subComponentType) {
        for (Component c : container.getComponents()) {
            if (subComponentType.isInstance(c)) return (T) c;
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Component> Set<T> getComponents(Container container, Class<T> subComponentType) {
        Set<T> components = new HashSet<T>();
        for (Component c : container.getComponents()) {
            if (subComponentType.isInstance(c)) {
                components.add((T) c);
            }
        }
        return components;
    }
}
