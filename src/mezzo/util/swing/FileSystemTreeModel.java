/*
 * (C) 2004 - Geotechnical Software Services
 * 
 * This code is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
package mezzo.util.swing;

import java.io.File;
import java.io.FilenameFilter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * A TreeModel implementation for a disk directory structure.
 * 
 * Typical usage:
 * 
 * <pre>
 * FileSystemModel model = new FileSystemModel(new File(&quot;/&quot;));<br/>
 * JTree tree = new JTree(model);
 * </pre>
 * 
 * @author <a href="mailto:info@geosoft.no">GeoSoft</a>/**
 * @author mezzodrinker
 * @since 0.0.1
 */
public class FileSystemTreeModel implements TreeModel {
    protected Collection<TreeModelListener> listeners;
    protected FileTreeNode                  rootNode;
    protected HashMap<File, List<File>>     sortedChildren; // File -> List<File>
    protected HashMap<File, Long>           lastModified_;

    /**
     * Create a tree model using the specified file as root.
     * 
     * @param root
     *            Root file (directory typically).
     */
    public FileSystemTreeModel(File root) {
        rootNode = new FileTreeNode(root);

        listeners = new ArrayList<TreeModelListener>();
        sortedChildren = new HashMap<File, List<File>>();
        lastModified_ = new HashMap<File, Long>();
    }

    protected boolean acceptFile(File directory, String filename) {
        return true;
    }

    @Override
    public FileTreeNode getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        final List<File> children = sortedChildren.get(parent);
        return children == null ? null : children.get(index);
    }

    @Override
    public final int getChildCount(Object parent) {
        final File file = (File) parent;
        if (!file.isDirectory()) return 0;

        final File[] children = file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File directory, String filename) {
                return acceptFile(directory, filename);
            }
        });
        final int nChildren = children == null ? 0 : children.length;

        final long lastModified = file.lastModified();

        final boolean isFirstTime = lastModified_.get(file) == null;
        boolean isChanged = false;

        if (!isFirstTime) {
            final Long modified = lastModified_.get(file);
            final long diff = Math.abs(modified.longValue() - lastModified);
            isChanged = diff > 4000; // MS/Win or Samba HACK. Check this!
        }

        // Sort and register children info
        if (isFirstTime || isChanged) {
            lastModified_.put(file, new Long(lastModified));

            final TreeSet<FileTreeNode> sorted = new TreeSet<FileTreeNode>();
            for (int i = 0; i < nChildren; i++) {
                sorted.add(new FileTreeNode(children[i]));
            }

            sortedChildren.put(file, new ArrayList<File>(sorted));
        }

        // Notify listeners (visual tree typically) if changes
        if (isChanged) {
            final TreeModelEvent event = new TreeModelEvent(this, getTreePath(file));
            fireTreeStructureChanged(event);
        }

        return nChildren;
    }

    protected Object[] getTreePath(File file) {
        final List<File> path = new ArrayList<File>();
        while (!file.equals(rootNode)) {
            path.add(file);
            file = file.getParentFile();
        }
        path.add(rootNode);

        final int nElements = path.size();

        final Object[] treePath = new Object[nElements];
        for (int i = 0; i < nElements; i++) {
            treePath[i] = path.get(nElements - i - 1);
        }

        return treePath;
    }

    @Override
    public boolean isLeaf(Object node) {
        return ((File) node).isFile();
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {}

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        final List<?> children = sortedChildren.get(parent);
        return children.indexOf(child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener listener) {
        if (listener != null && !listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeTreeModelListener(TreeModelListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    public void fireTreeNodesChanged(TreeModelEvent event) {
        for (final TreeModelListener listener : listeners) {
            listener.treeNodesChanged(event);
        }
    }

    public void fireTreeNodesInserted(TreeModelEvent event) {
        for (final TreeModelListener listener : listeners) {
            listener.treeNodesInserted(event);
        }
    }

    public void fireTreeNodesRemoved(TreeModelEvent event) {
        for (final TreeModelListener listener : listeners) {
            listener.treeNodesRemoved(event);
        }
    }

    public void fireTreeStructureChanged(TreeModelEvent event) {
        for (final TreeModelListener listener : listeners) {
            listener.treeStructureChanged(event);
        }
    }

    /**
     * Extension to the java.io.File object but with more appropriate compare rules
     * 
     * @author <a href="mailto:info@geosoft.no">GeoSoft</a>
     */
    public static class FileTreeNode extends File implements Comparable<File> {
        private static final long serialVersionUID = 7078823348206725984L;

        public FileTreeNode(File file) {
            super(file, "");
        }

        /**
         * Compare two FileTreeNode objects so that directories are sorted first.
         * 
         * @param file
         *            Object to compare to.
         * @return Compare identifier.
         */
        @Override
        public int compareTo(File file) {
            final File file1 = this;
            final File file2 = file;

            final Collator collator = Collator.getInstance();

            if (file1.isDirectory() && file2.isFile())
                return -1;
            else if (file1.isFile() && file2.isDirectory())
                return +1;
            else
                return collator.compare(file1.getName(), file2.getName());
        }

        /**
         * Retur a string representation of this node. The inherited toString() method returns the entire path. For use in a tree structure, the name is more appropriate.
         * 
         * @return String representation of this node.
         */
        @Override
        public String toString() {
            return getName();
        }
    }
}
