package mezzo.util.logging.stream;

import java.io.PrintStream;

import mezzo.util.logging.Level;
import mezzo.util.time.Time;

/**
 * <code>DefaultPrintStreamLogger</code>
 * 
 * @author mezzodrinker
 */
public class DefaultPrintStreamLogger extends PrintStreamLogger {
    public DefaultPrintStreamLogger(PrintStream stream) {
        super(stream);
    }

    @Override
    public void log(Object o, Level level) {
        if (level == null) {
            level = Level.FINEST;
        }
        stream.println(Time.getCurrentDateTimeAsString() + " [" + level.toString() + "] " + String.valueOf(o));
    }

    @Override
    public void log(Object o, Throwable t) {
        if (t == null) throw new IllegalArgumentException("t can not be null");
        log(o, Level.SEVERE);

        for (Throwable cause = t; t.getCause() != null; t = t.getCause()) {
            log(cause.getClass().getName() + ": " + cause.getMessage());

            StackTraceElement[] stackTrace = cause.getStackTrace();
            for (StackTraceElement element : stackTrace) {
                log("\tat " + element.getClassName() + "." + element.getMethodName() + " (" + element.getFileName() + ":" + element.getLineNumber() + ")");
            }
        }
    }
}
