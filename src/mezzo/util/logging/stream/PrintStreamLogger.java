package mezzo.util.logging.stream;

import java.io.PrintStream;

import mezzo.util.logging.Logger;
import mezzo.util.logging.Level;

/**
 * <code>StreamLogger</code>
 * 
 * @author mezzodrinker
 */
public abstract class PrintStreamLogger implements Logger {
    protected boolean           debugEnabled = false;
    protected final PrintStream stream;

    protected PrintStreamLogger(PrintStream stream) {
        if (stream == null) throw new IllegalArgumentException("stream can not be null");
        this.stream = stream;
    }

    @Override
    public void setDebugEnabled(boolean debugEnabled) {
        this.debugEnabled = debugEnabled;
    }

    @Override
    public void error(Object o) {
        log(o, Level.SEVERE);
    }

    @Override
    public void warning(Object o) {
        log(o, Level.WARNING);
    }

    @Override
    public void info(Object o) {
        log(o, Level.INFO);
    }

    @Override
    public void log(Object o) {
        log(o, Level.FINEST);
    }

    @Override
    public void debug(Object o) {
        log(o, Level.DEBUG);
    }

    @Override
    public abstract void log(Object o, Level level);

    @Override
    public abstract void log(Object o, Throwable t);
}
