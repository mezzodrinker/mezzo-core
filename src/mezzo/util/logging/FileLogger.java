package mezzo.util.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.logging.Level;

/**
 * 
 * @author lirtistasya
 * @since 0.0.1
 */
@Deprecated
public class FileLogger {
    protected static StringBuilder log = new StringBuilder();
    protected static PrintStream   out = null;

    static {
        File log = new File("log-0-latest.log");
        File log0 = log;
        File log1 = new File("log-1.log");
        File log2 = new File("log-2.log");

        if (log2.exists()) {
            log2.delete();
        }
        if (log1.exists()) {
            log1.renameTo(log2);
        }
        if (log0.exists()) {
            log0.renameTo(log1);
        }

        try {
            out = new PrintStream(log);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void log(Object o) {
        log(o, Level.FINEST);
    }

    public static void log(Object o, Level l) {
        String[] messages = o.toString().split("\n");
        for (String message : messages) {
            log.append("[").append(l.getName()).append("]");
            log.append(message);
            log.append("\n");
        }
    }

    public static String getLog() {
        return log.toString();
    }

    public static void save() {
        if (!log.toString().isEmpty()) {
            out.println(log.toString());
        }
    }
}
