package mezzo.util.logging.file;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import mezzo.util.logging.Level;
import mezzo.util.time.Time;

/**
 * <code>DefaultFileLogger</code>
 * 
 * @author mezzodrinker
 */
public class DefaultFileLogger extends FileLogger {
    public DefaultFileLogger(String path) throws IOException {
        this(new File(path));
    }

    public DefaultFileLogger(File file) throws IOException {
        super(file);
        if (file.exists()) {
            file.delete();
            file.createNewFile();
        }
    }

    @Override
    public void log(Object o, Level level) {
        if (level == null) {
            level = Level.FINEST;
        }
        String[] lines = String.valueOf(o).split("(\n|\r\n|\n\r)");
        for (String s : lines) {
            stream.println(Time.getCurrentDateTimeAsString() + " [" + level.toString() + "] " + s);
        }
    }

    @Override
    public void log(Object o, Throwable t) {
        if (t == null) throw new IllegalArgumentException("t can not be null");
        if (o != null && !String.valueOf(o).isEmpty()) {
            log(o, Level.SEVERE);
        }

        for (Throwable cause = t; cause != null; cause = cause.getCause()) {
            if (cause == t) {
                log(cause.getClass().getName() + ": " + cause.getMessage(), Level.SEVERE);
            } else {
                log("caused by " + cause.getClass().getName() + ": " + cause.getMessage(), Level.SEVERE);
            }

            StackTraceElement[] stackTrace = cause.getStackTrace();
            for (StackTraceElement element : stackTrace) {
                log("\tat " + element.getClassName() + "." + element.getMethodName() + " (" + element.getFileName() + ":" + element.getLineNumber() + ")", Level.SEVERE);
            }
        }
    }

    @Override
    public PrintStream getStream() {
        return stream;
    }
}
