package mezzo.util.logging.file;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import mezzo.util.logging.Level;
import mezzo.util.logging.stream.PrintStreamLogger;

/**
 * <code>FileLogger</code>
 * 
 * @author mezzodrinker
 */
public abstract class FileLogger extends PrintStreamLogger {
    protected boolean    debugEnabled = false;
    protected final File file;

    protected FileLogger(File file) throws IOException {
        super(new PrintStream(file));
        if (file == null) throw new IllegalArgumentException("file can not be null");
        this.file = file;
    }

    @Override
    public void setDebugEnabled(boolean debugEnabled) {
        this.debugEnabled = debugEnabled;
    }

    @Override
    public void error(Object o) {
        log(o, Level.SEVERE);
    }

    @Override
    public void warning(Object o) {
        log(o, Level.WARNING);
    }

    @Override
    public void info(Object o) {
        log(o, Level.INFO);
    }

    @Override
    public void log(Object o) {
        log(o, Level.FINEST);
    }

    @Override
    public void debug(Object o) {
        if (debugEnabled) {
            log(o, Level.DEBUG);
        }
    }

    @Override
    public abstract void log(Object o, Level level);

    @Override
    public abstract void log(Object o, Throwable t);

    public abstract PrintStream getStream();
}
