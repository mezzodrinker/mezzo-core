package mezzo.util.logging;

/**
 * <code>ILogger</code>
 * 
 * @author mezzodrinker
 */
public interface Logger {
    public void error(Object o);

    public void warning(Object o);

    public void info(Object o);

    public void log(Object o);

    public void log(Object o, Level level);

    public void log(Object o, Throwable t);

    public void debug(Object o);

    public void setDebugEnabled(boolean debugEnabled);
}
