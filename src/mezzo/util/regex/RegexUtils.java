package mezzo.util.regex;

import java.util.regex.Pattern;

/**
 * 
 * @author lirtistasya
 * @since 0.0.1
 */
public class RegexUtils {
	// can't instantiate RegexUtils
	private RegexUtils() {
	}

	public static boolean matches( CharSequence input, String regex ) {
		return Pattern.compile(regex).matcher(input).matches();
	}

	public static boolean matchesDotAll( CharSequence input, String regex ) {
		return Pattern.compile(regex, Pattern.DOTALL).matcher(input).matches();
	}
}
